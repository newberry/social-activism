import json

f = open('itemTranscriptions_wogfn_221231.json')
data = json.load(f)

g = open('items.json')
titleFile = json.load(g)


for iidx, i in enumerate(data["transcriptions"]):
    # if iidx < 5:
    filename = 'testPres/static/data/item_' + str(i["id"]) + '.txt'
    with open(filename, "w") as txtfile:
        try:
            itemFields = next(item for item in titleFile["items"] if item["id"] == i["id"])
            title = itemFields["title"]
            txtfile.write('\n')
            txtfile.write("## " + str(i["id"]) + ": " + title)
            for pidx, p in enumerate(i["pages"]):
                # if pidx < 5:
                txtfile.write('\n')
                txtfile.write('### page ' + str(pidx + 1))
                if len(p["transcription"]) > 0:
                    txtfile.write('\n')
                    txtfile.write(p["transcription"])
                else:
                    txtfile.write("No transcription.")
        except:
            print('\n')
            print("failed to find title for " + i["id"])
            txtfile.write('\n')
            txtfile.write("failed to find title for " + i["id"])

# titleOutput = []
# for i in titleFile["items"]:
#     lilArr = [i["id"], i["title"]]
#     titleOutput.append(lilArr)

# json_object = json.dumps(titleOutput, indent=4)

# # Writing to sample.json
# with open("testPres/src/titles.json", "w") as outfile:
#     outfile.write(json_object)
