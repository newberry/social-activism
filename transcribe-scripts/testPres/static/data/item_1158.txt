
## 1158: Anna Sophie Raster diaries [in German], 1886, 1897-1898
### page 1
no text
### page 2
January, Friday 1. 1886.
### page 3
January, Monday 4. 1886
### page 4No transcription.
### page 5No transcription.
### page 6No transcription.
### page 7No transcription.
### page 8No transcription.
### page 9No transcription.
### page 10No transcription.
### page 11No transcription.
### page 12No transcription.
### page 13No transcription.
### page 14No transcription.
### page 15No transcription.
### page 16No transcription.
### page 17No transcription.
### page 18No transcription.
### page 19No transcription.
### page 20No transcription.
### page 21No transcription.
### page 22No transcription.
### page 23No transcription.
### page 24No transcription.
### page 25No transcription.
### page 26No transcription.
### page 27No transcription.
### page 28No transcription.
### page 29No transcription.
### page 30No transcription.
### page 31No transcription.
### page 32No transcription.
### page 33No transcription.
### page 34No transcription.
### page 35No transcription.
### page 36No transcription.
### page 37No transcription.
### page 38No transcription.
### page 39No transcription.
### page 40No transcription.
### page 41No transcription.
### page 42No transcription.
### page 43No transcription.
### page 44No transcription.
### page 45No transcription.
### page 46
New York
MAY, THURSDAY 13. 1886
Fulton Markt. 
Broadway u. die großen Brücken gesehen. 
---
FRIDAY 14.
---
SATURDAY 15. 
---
Im Hotel.
### page 47No transcription.
### page 48No transcription.
### page 49
MAY, SATURDAY 22. 1886

SUNDAY 23.

MONDAY 24.

In Southampton.
### page 50
MAY, TUESDAY 25. 1886.
Regenwetter.

WEDNESDAY 26.

THURSDAY 27.
### page 51No transcription.
### page 52No transcription.
### page 53No transcription.
### page 54No transcription.
### page 55No transcription.
### page 56No transcription.
### page 57No transcription.
### page 58No transcription.
### page 59No transcription.
### page 60No transcription.
### page 61No transcription.
### page 62No transcription.
### page 63No transcription.
### page 64No transcription.
### page 65No transcription.
### page 66No transcription.
### page 67No transcription.
### page 68No transcription.
### page 69No transcription.
### page 70No transcription.
### page 71No transcription.
### page 72No transcription.
### page 73No transcription.
### page 74No transcription.
### page 75No transcription.
### page 76No transcription.
### page 77No transcription.
### page 78No transcription.
### page 79No transcription.
### page 80No transcription.
### page 81No transcription.
### page 82
AUGUST, SUNDAY 29. 1886
Walther
MONDAY 30.
TUESDAY 31.
### page 83
MEMORANDA
Received 8 New Years cards.
### page 84
LETTER REGISTER
Name.                    Received.                    Answered.
Minnie Wetzler       20 Dec.                       29 Dec.
Großmama            1 Feb.                          2 Jan. 
Rachel Weis                                              6 Jan.
---                                                          7 Jan. 
Großmama                                                28 März [?]
### page 85
ADDRESSES.
Name.  Residence.
Ernestine Stuck 292 E. Indiana
Percy Wolff 735 Fulton
Emma Feldkamp 303 Van Buren
Paula Bush 429 Oak Str.
Clara Schmall 504 Dearborn
Stella Leicht 210 Seadgw.
Clara Stotz 301 Adams
Saly Sommer 404 Jackson
Pama Haentze 628 Adams
Frida Petersen 414 Jackson
Clara Rahm 418 Centrat
Ida Petersen 305 Van Buren
Clara Petersen 531 Adams
### page 86
VISITS.
Name.  Address.
Stella Leicht 710 Sedgw.
Oliver unclear 504 Dearborn
unclear 404 Jackson
unclear 623 Owosso
### page 87No transcription.
### page 88
Cash Account--January

                                             Received.     Paid.

[[Kaffe---}}                                 2.78
Tisch--                                    .45
unclear                                                        .05
unclear                                                        .15
unclear                                                        .03                                                                       .03
Blumen                                                           .25
Tasche--                                  .45
### page 89
Cover
### page 90
Agenda - cover
### page 91No transcription.
### page 92
JANUAR 2
Regenwetter. Früh aufgestanden

Abends zuhause geblieben.
### page 93No transcription.
### page 94No transcription.
### page 95No transcription.
### page 96No transcription.
### page 97No transcription.
### page 98No transcription.
### page 99No transcription.
### page 100No transcription.
### page 101No transcription.
### page 102No transcription.
### page 103No transcription.
### page 104No transcription.
### page 105No transcription.
### page 106No transcription.
### page 107No transcription.
### page 108No transcription.
### page 109No transcription.
### page 110No transcription.
### page 111No transcription.
### page 112No transcription.
### page 113No transcription.
### page 114No transcription.
### page 115No transcription.
### page 116No transcription.
### page 117No transcription.
### page 118No transcription.
### page 119No transcription.
### page 120No transcription.
### page 121No transcription.
### page 122No transcription.
### page 123No transcription.
### page 124No transcription.
### page 125No transcription.
### page 126No transcription.
### page 127No transcription.
### page 128No transcription.
### page 129No transcription.
### page 130No transcription.
### page 131No transcription.
### page 132No transcription.
### page 133No transcription.
### page 134No transcription.
### page 135No transcription.
### page 136No transcription.
### page 137No transcription.
### page 138No transcription.
### page 139No transcription.
### page 140No transcription.
### page 141No transcription.
### page 142No transcription.
### page 143No transcription.
### page 144No transcription.
### page 145No transcription.
### page 146No transcription.
### page 147No transcription.
### page 148No transcription.
### page 149No transcription.
### page 150No transcription.
### page 151No transcription.
### page 152No transcription.
### page 153No transcription.
### page 154No transcription.
### page 155No transcription.
### page 156No transcription.
### page 157No transcription.
### page 158No transcription.
### page 159No transcription.
### page 160No transcription.
### page 161No transcription.
### page 162No transcription.
### page 163No transcription.
### page 164No transcription.
### page 165No transcription.
### page 166No transcription.
### page 167No transcription.
### page 168No transcription.
### page 169No transcription.
### page 170No transcription.
### page 171No transcription.
### page 172No transcription.
### page 173No transcription.
### page 174No transcription.
### page 175No transcription.
### page 176No transcription.
### page 177No transcription.
### page 178No transcription.
### page 179No transcription.
### page 180No transcription.
### page 181No transcription.
### page 182No transcription.
### page 183No transcription.
### page 184No transcription.
### page 185No transcription.
### page 186No transcription.
### page 187No transcription.
### page 188No transcription.
### page 189No transcription.
### page 190No transcription.
### page 191No transcription.
### page 192No transcription.
### page 193No transcription.
### page 194No transcription.
### page 195No transcription.
### page 196No transcription.
### page 197No transcription.
### page 198No transcription.
### page 199No transcription.
### page 200No transcription.
### page 201No transcription.
### page 202No transcription.
### page 203No transcription.
### page 204No transcription.
### page 205No transcription.
### page 206No transcription.
### page 207No transcription.
### page 208No transcription.
### page 209No transcription.
### page 210No transcription.
### page 211No transcription.
### page 212No transcription.
### page 213No transcription.
### page 214No transcription.
### page 215No transcription.
### page 216No transcription.
### page 217No transcription.
### page 218No transcription.
### page 219No transcription.
### page 220No transcription.
### page 221No transcription.
### page 222No transcription.
### page 223No transcription.
### page 224No transcription.
### page 225No transcription.
### page 226No transcription.
### page 227No transcription.
### page 228No transcription.
### page 229No transcription.
### page 230No transcription.
### page 231No transcription.
### page 232No transcription.
### page 233No transcription.
### page 234No transcription.
### page 235No transcription.
### page 236No transcription.
### page 237No transcription.
### page 238No transcription.
### page 239No transcription.
### page 240No transcription.
### page 241No transcription.
### page 242No transcription.
### page 243No transcription.
### page 244No transcription.
### page 245No transcription.
### page 246No transcription.
### page 247No transcription.
### page 248No transcription.
### page 249No transcription.
### page 250No transcription.
### page 251No transcription.
### page 252No transcription.
### page 253No transcription.
### page 254No transcription.
### page 255No transcription.
### page 256No transcription.
### page 257No transcription.
### page 258No transcription.
### page 259No transcription.
### page 260No transcription.
### page 261No transcription.
### page 262No transcription.
### page 263No transcription.
### page 264
JUNI
22
Schönes, warmes Wetter. Vormittag Mama und ich gepackt, Besorgungen gemacht. Bella Vocke zwei Mal bei mir; ihre Verlobung mit Dr. Bröse mitgetheilt. Frl. Düllen aus Hannover gekommen. Walther Nachmittag bei uns. Mama und ich ausgegangen: in Cafe Bauer: gepackt. Ich Blumen von d. Eitel bekommen, Lord Parker Herr Grandt, etc. Erich Dulk und Walter auch da. Früh zu Bett. Mama wohl.
### page 265No transcription.
### page 266No transcription.
### page 267No transcription.
### page 268No transcription.
### page 269No transcription.
### page 270No transcription.
### page 271No transcription.
### page 272No transcription.
### page 273No transcription.
### page 274No transcription.
### page 275No transcription.
### page 276No transcription.
### page 277No transcription.
### page 278No transcription.
### page 279No transcription.
### page 280No transcription.
### page 281No transcription.
### page 282No transcription.
### page 283No transcription.
### page 284No transcription.
### page 285No transcription.
### page 286No transcription.
### page 287No transcription.
### page 288No transcription.
### page 289No transcription.
### page 290No transcription.
### page 291No transcription.
### page 292No transcription.
### page 293No transcription.
### page 294No transcription.
### page 295No transcription.
### page 296No transcription.
### page 297No transcription.
### page 298
AUGUST 1
unclear
### page 299
August 8
Sonntag.
### page 300
AUGUST 10
zü unclear?
### page 301
AUGUST 15
unclear
### page 302
AUGUST 17
unclear?
### page 303
AUGUST 22
unclear
### page 304
AUGUST 29
unclear
### page 305
SEPTEMBER 5
unclear
### page 306
Frl. Holldorf
Frau Peterson
Willi Honicke
Frl. Polzien
Beusters
Schwendys.
Dr. Mahler.
### page 307
blank cover
### page 308
Excelsior Diary
### page 309
Wea.                                                      Sat. Jan 1, 1898                                                        Ther.
Schoner, kalter Tag. Alle sehr lange geschlafen: Walther & Herz Katzenjammer von Sylvester gehabt. Mama der ganzen Tag krank imm Bitt gewesen: vom Fall auf der "elevated" Bahn ___________ blau gestossen. Dr. Bluthardt zum Mittagessen bei uns. Nachmittag Brucker gekommen Gegen Allen, Emil und Emma Eitel bei uns; zum Thugeblieben. Froh nach Haus.

Wea.                                                      Sunday 2                                                                    Ther.
Den gauzen Vormittag gefracht. Zum Mittag essen Eddi und Toni bei uns. Mama nach immer krank. Nachmittag Eddi, Toni, Herz und ich mit Walter zum Bahnhof gefahren. Weil auch dort. Walther _______ Uhr nach Troy Abgereist. Eddi und Toni den Nachmittag, zum Abend essen und abends ____ uns geblieben, Musicurt, Spiele gesfielt.
### page 310
Wea.                                                     Mon. Jan. 3, 1898                                                       Ther.
Schoner Tag. Ich morgens zu Fuss mit Herz in die Stadt gegangen; zu Fuss zurück.
### page 311
Wea. WED. JAN. 5, 1989 Ther.
Schönes, warmes Wetter. Ich zum lunch bei Emma Eitel (Mama in der Stadt gegessen), dann Emma und ich zu Lily Brand gegangen. Nellie Stern dort. Emma mit mir zu Frau Dr. Mels zur italienischen Stunde gegangen: Mattie, Arnold, Clotilde Schmidt, Marie Eitel dort. Ich spät nach Hause zum Abendbrot. Arthur 2 Contracte mit Tosetti abgeschlossen: 2 Lokale.
Wea. THURSDAY 6 Ther.
Schönes Wetter. Vormittag zuhause gearbeitet. Arthur zehr früh auf die office gegangen. Nachmittag Eddie bei uns. Dann Mama Kafeekränzchen gehabt: Frau Hotz, Frau Specht, Frau Dr Bluthardt, Frau Roesch, Frau Neubarth, Frau Jordan, Frau Dr. Hessert. Frau Bauer mitgekommen. Herze spät nachhause gekommen. Abends gelesen, früh zu Bett. Mein Zimmer zehr bewundert worden.
### page 312
Wea. FRI. JAN. 7, 1898 Ther.
Sehr schön und warm. Arthur früh auf die office. Mama und ich in die Stadt zu Herrn unclear Eddi, Arthur (unclear dort), unclear und Bank. Mit Eddi in seine Fabrik gegangen: Eddi zum lunch bei uns. Nachmittag Frau Dr. Matthei und Frau Dr. Doepp uns besucht. Ich Visiten bei Klärchen Hotz und Ida Wright gemacht. unclear
### page 313No transcription.
### page 314No transcription.
### page 315No transcription.
### page 316No transcription.
### page 317No transcription.
### page 318No transcription.
### page 319No transcription.
### page 320No transcription.
### page 321No transcription.
### page 322No transcription.
### page 323No transcription.
### page 324No transcription.
### page 325No transcription.
### page 326No transcription.
### page 327No transcription.
### page 328No transcription.
### page 329No transcription.
### page 330No transcription.
### page 331No transcription.
### page 332No transcription.
### page 333No transcription.
### page 334No transcription.
### page 335No transcription.
### page 336No transcription.
### page 337No transcription.
### page 338No transcription.
### page 339No transcription.
### page 340No transcription.
### page 341No transcription.
### page 342No transcription.
### page 343No transcription.
### page 344No transcription.
### page 345No transcription.
### page 346
Wea. WED. MARCH 16, 1898 Ther.
Wunderbar schönes, aber heisses Wetter. Mama krank im Bett mit Schmerzen. Ich zu Fuss mit Arthur in die Stadt. bei Dr. Bluthardt. Einkäufe gemacht. Mama im Bett. Dr. Bluthardt zum lunch bei mir. Ich Nachmittag zuhause, kleinen Spaziergang gemacht. Gegen Abend Mama wohler. Ich ital. Stunde geschwänzt. Abends gemüthlich wir drei zuhause. Sehr müde, früh zu Bett. Manuscript verschickt underlined.
Wea. THURSDAY 17 Ther.
Wunderbar schönes, warmes Wetter. Mama besser. Ich zu Fuss früh mit Arthur in die Stadt, zu Fuss zurück. Hausarbeit gethan. Abends Gesellschaft bei uns: Theo Pietsch, Dr. Lee, Arthur Woltersdorf, Olivia Bopp, Frida Vocke, Hertha Rubens, Tilly und Willi Rapp, Hertha Rubens struck through, Ed und Toni. Bowle und kaltes Essen. Ziemlich lustig. Theo Pietsch Frida Vocke den Hof gemacht. Um 1/2 1 Uhr Alle fortgegangen Arthur Kopfweh gehabt.
### page 347
Wea. FRI. MARCH 18, 1898 Ther.
Abscheuliches Regenwetter. Arthur und Mama beide wieder wohl. Vormittag zuhause geblieben. Frida bei mir. Nachmittag Mama und ich bei Toni zum Kaffee. Unterwegs Herrn Juergens, Frl. Lampe, Clara Bauer getroffen. Bei Toni Frau Brucker, Frau Leeb, Frl. Böttcher. Sehr gemütlich. Zum Abendessen Mama, Arthur und ich bei Bruckners. Ed und Toni ausgegangen. Wir abends bei Frau Brucker und Ralph. Furchtbarer Regen. Im Wagen nachhause. Starkes Gewitter.
Wea. SATURDAY 19 Ther.
Trübes, aber warmer Tag. Mama nicht wohl. Ich Schmerzen im Gegentheil gehabt. Zu Fuss morgens mit Arthur in die Stadt, zu Fuss zurück. Nachmittag zuhause gefaullenzt. Arthur zum Kaffee gekommen, mir seinen Vortrag für Columbia Club vorgelesen. Abends Mama, Arthur und ich in die Germania zur 48er Feier. Reden sehr schön. Papa vergessen. - Im Kneipstübchen mit sehr lustiger Gesellschaft bis 3 Uhr früh gesessen. unclear gehabt. Brentano, Bluthardt, Schmidts, Eitels, Webers. O.O.
### page 348
Wea. SUN. MARCH 20, 1898 Ther.
Trübes, trockenes Wetter. Lange geschlafen. Mama und ich zu Rapp auf die office unclear. Zu Mittag wir drei allein; sehr unclear. Musicirt, gelesen. Arthur und ich zum unclear zü Brands, unclear Clara Petersen, Frau Gehrken getroffen. Abends zuhause zu Dritt. Ich ein wenig nervös. Sehr müde Alle. Früh zu Bett. O. Sehr gut geschlafen Regen in der Nacht.
Wea. MONDAY 21 Ther.
Schönes, warmes Wetter, aber trübe. Vormittag zuhause geblieben, Hausarbeit gethan, gefaullenzt. Nach Tisch Dr. Bluthardt gekommen. Dann Mama und ich in die Stadt gegangen, Besorgungen gemacht. Bei Eddi. Arthur abgeholt, mit ihm bei Henrici Kaffee getrunken. Mama nach Hause. Ich auf Arthur's office geblieben mit Günzel geplaudert. Arthur und ich zu Fuss nach Hause: Regen. Abends musicirt, gelesen.
### page 349
Wea. TUES. MARCH 22 1898 Ther.
Sehr warm, aber trübe. Ich und Mama den ganzen Tag zuhause geblieben, Verse gemacht, Hausarbeit gethan. Nachmittag zu Hotz gehen wollen, aber furchtbarer Regenguss dazwischen gekommen. Wetter plötzlich sehr kalt geworden. Arthur früh schon nach Haus gekommen. Abends wir drei gemütlich in unserm Zimmer gesessen. Arthur seinen Vortrag für Columbia Club vorgelesen. Sehr schön.
Wea. WEDNESDAY 23 Ther.
Kalt und trübe. Ich früh zu Fuss mit Arthur in die Stadt, auf seiner office und bei Eddi. Besorgungen gemacht. Zu Fuss zurück. - Nachmittag Besuch mit Mama zu Hotz's. Nicht zu Hause. Zu Fuss in die Stadt. Zum Kränzchen bei Paula Andersen. Gut amüsirt: Stella, Toni, Laura Kranz, Frl. Blocki, Frida Petersen dort. - Abends wir drei gemütlich zusammen zuhause; musicirt, gelesen. Ich den ganzen Tag sehr gut Gefühlt, wie früher. O.
### page 350
Wea. THUR. MARCH 24, 1898 Ther.
Wunderbar schönes, aber kühles Wetter. Ich früh mit Arthur zu Fuss in die Stadt gebummelt. Tags über Hausarbeit gethan. Frau MacDonald uns besucht. Arthur früh nach Haus: bicycle unclear
Wea. FRIDAY 25 Ther.
unclear
### page 351
Wea. SAT. MARCH 26, 1898 Ther.
unclear
Wea. SUNDAY 27 Ther.
Sonntag.
Abscheuliches Regenwetter den ganzen Tag. Sehr spät aufgestanden, erst um 1/2 12 Uhr. Zum Mittagessen Günzel gekommen, bis abends geblieben. Ich den ganzen Tag entsetzlich krank mit Katzenjammer, Kopfweh. Nachmittag Alle geschlafen. unclear
### page 352No transcription.
### page 353No transcription.
### page 354No transcription.
### page 355No transcription.
### page 356No transcription.
### page 357No transcription.
### page 358No transcription.
### page 359No transcription.
### page 360No transcription.
### page 361No transcription.
### page 362No transcription.
### page 363No transcription.
### page 364No transcription.
### page 365No transcription.
### page 366No transcription.
### page 367No transcription.
### page 368No transcription.
### page 369No transcription.
### page 370No transcription.
### page 371No transcription.
### page 372No transcription.
### page 373No transcription.
### page 374No transcription.
### page 375No transcription.
### page 376No transcription.
### page 377No transcription.
### page 378No transcription.
### page 379No transcription.
### page 380No transcription.
### page 381No transcription.
### page 382No transcription.
### page 383No transcription.
### page 384No transcription.
### page 385No transcription.
### page 386No transcription.
### page 387No transcription.
### page 388No transcription.
### page 389No transcription.
### page 390No transcription.
### page 391No transcription.
### page 392No transcription.
### page 393No transcription.
### page 394No transcription.
### page 395No transcription.
### page 396No transcription.
### page 397No transcription.
### page 398No transcription.
### page 399No transcription.
### page 400No transcription.
### page 401No transcription.
### page 402No transcription.
### page 403No transcription.
### page 404No transcription.
### page 405No transcription.
### page 406No transcription.
### page 407No transcription.
### page 408No transcription.
### page 409No transcription.
### page 410No transcription.
### page 411No transcription.
### page 412No transcription.
### page 413No transcription.
### page 414No transcription.
### page 415No transcription.
### page 416No transcription.
### page 417No transcription.
### page 418No transcription.
### page 419No transcription.
### page 420No transcription.
### page 421No transcription.
### page 422No transcription.
### page 423No transcription.
### page 424No transcription.
### page 425No transcription.
### page 426No transcription.
### page 427No transcription.
### page 428No transcription.
### page 429No transcription.
### page 430No transcription.
### page 431No transcription.
### page 432No transcription.
### page 433No transcription.
### page 434
Wea. THUR. SEPT. 8, 1898 Ther.
unclear Columbia Damen Club, Bismarck - Garten. Concert, Kaffee. Frl. Lieb, Frl. Boettcher Frau Kirchhof, Toni, etc. Schwach besucht. Abends wir Alle zusammen gemüthlich zühause. Arthur krank.
Wea. FRIDAY 9 Ther.
Schönes, kühles Wetter. Arthur wohler, ich erkältet. Ohne Köchin. Ich früh in die Stadt mit Mama, Mattie Arnold getroffen, Besorgungen gemacht. Schmerzen gehabt. Nachmittag Mama und ich wieder in der Stadt, Einkäufe gemacht. Sehr kalt. Walter Nachmittag und abends bei Toni. Mama, Arthur und ich abends allein. Ich nicht wohl. Gelesen. Früh zu Bett.
### page 435
Wea. SAT. SEPT. 10, 1898 Ther.
Schönes, kaltes Wetter. Ich erkältet, nicht wohl. Ohne Mädchen, Hausarbeit gethan. Vormittag ich zu Brands nicht zuhause. Wegen der Köchin. Walter gepackt, nachmittag ausgegangen. Ich mit Mama zuhause, gearbeitet. Abends Alle zusammen zuhause. Walter's letzter Abend. Geplaudert. Arthur Offerte von Baumgarten für Brand's Stelle gehabt. Sehr müde. Früh zu Bett. - Kaiserin von Osterreich erinordet.
Wea. SUNDAY 11 Ther.
Wunderschönes, kühles Wetter. Vormittag Hausarbeit gethan. Walter gepackt. Zum Mittagessen wir vier allein. Graf Contin gekommen und mit Walter, Arthur und mir zum Bahnhof. Eddie und Toni dort. Walter um 3 Uhr nach Troy abgereist. Wir zum Abschiedskafee bei Brands. Sehr gemütlich und schön. Arthur Abschiedsrede gehalten: Wein, etc. Schlesiers, Frl. Becker, Günzel, Weber, Manch, wir vier. Um 1/2 8 Uhr zuhause. Mama den ganzen Tag allein gewesen. Abend zuhause geblieben.
### page 436
Wea. MON. SEPT. 12, 1898 Ther.
Wunderbar schönes Wetter. Noch immer ohne Mädchen Walter fort. Mama und ich wohl. Vormittag zuhause gearbeitet. Nachmittag Mama und ich in die Stadt gegangen, Einkäufe gemacht, bei Marshall Field Thee getrunken. Spaziergang an Michigan Avenue. Sehr heiss. Abends wir drei zusammen zuhause; vor der Thür gesessen, Gelesen. Ich unartig, müde. Früh zu Bett.
Wea. TUESDAY 13 Ther.
Kühles, nasser Regenwetter. Ohne Mädchen. Alle wohl. Erste Karte von Walter. Vormittag zuhause, gearbeitet.Genäht. Nachmittag unsere Ida nach Portland Oregon, unclear, zu unclear
### page 437No transcription.
### page 438No transcription.
### page 439No transcription.
### page 440No transcription.
### page 441No transcription.
### page 442No transcription.
### page 443No transcription.
### page 444
Wea. WED. SEPT. 28, 1898 Ther.
unclear
Wea. THURSDAY 29 Ther.
Schönes, aber furchtbar heisses Wetter. Ich sehr wohl. Frl. Gustchen bei Köchin eingeübt. Ich und Mama den ganzen Tag zuhause, genäht, geschrieben. Emma Calder Vormittag gekommen mit baby Sachen, ein Stündchen geblieben. Mama abends nicht wohl, Migräne. Ich mit ihr allein. Arthur nicht zum Essen und abends nicht zuhause, im Donnerstag-Abend Club, Germania.
### page 445No transcription.
### page 446No transcription.
### page 447No transcription.
### page 448No transcription.
### page 449No transcription.
### page 450No transcription.
### page 451No transcription.
### page 452No transcription.
### page 453No transcription.
### page 454No transcription.
### page 455No transcription.
### page 456No transcription.
### page 457No transcription.
### page 458No transcription.
### page 459No transcription.
### page 460No transcription.
### page 461No transcription.
### page 462No transcription.
### page 463No transcription.
### page 464
Wea. MON. NOV. 7, 1898 Ther.
unclear
Wea. TUESDAY 8 Ther.
Hässliches, stürmisches, schmutziges Regenwetter. Ich früh zu Fuss mit Arthur gegangen. Dann gestickt, genäht. Mama und ich beide wohl. 
Nachmittag Frau Petersen mit Ida Kochs, Frau Rapp mit Mathilde zum Kaffee bei uns. Baby-Wäsche gezeigt. Gemüthlich geplaudert. Arthur früh nach Haus gekommen. 
Abends in seinem Zimmer gesessen. Geplaudert vorgelesen, ich gestickt. Wenig Schmerzen gehabt.
### page 465
Wea. WED. NOV. 9, 1898 Ther.
Schreckliches Regenwetter, Sturm, Schmutz. Früh aufgestanden, gestickt. Alle wohl. Frl. Gustchen zum Nähen bei uns. Mama in die Stadt gefahren, ich zu Hause geblieben: genäht, geschrieben. Schneegestöber. Abends Alle gemüthlich in Arthur's Zimmer gesessen. Thee getrunken. Briefe aus Budapesth. Ich arge Schmerzen bekommen & Zittern. Bald vorübergegangen. Genäht, spät zu Bett. Ich wieder ganz wohl.
Wea. THURSDAY 10 Ther.
Wunderschönes, sonniges Wetter. Ich früh aufgestanden, gestickt. Alle wohl. Ich mit Arthur zu Fuss in die Stadt. Dann den ganzen Tag mit Mama zuhause. Geschrieben, gestickt, genäht, gebadet. Mein Zimmer eingeräumt. Wetter wieder schlecht geworden. Abends alle zuhause. Gemüthlich in Arthur's Zimmer gesessen, geplaudert, gelesen, Thee getrunken. Mama wegen ihrer Finanzverhältnisse sehr aufgeregt.
### page 466
Wea. FRI. NOV. 11, 1898 Ther.
Wunderschönes Wetter, aber sehr schmutzig. Alle wohl. Vormittag ich zu Hause genäht. Clothilde Hotz mich besucht. Baby-Ausstener besehen. 
Nachmittag Mama und ich erst bei Eddie, dann bei Juergens (Herrn Luerssen dort getroffen) und bei Pietsch. Grosse Geschäftsverhandlung. Judge Tuthill getroffen. Dr. Bluthardt während unserer Abwesenheit bei uns. - Abends zuhause. Mama sehr niedergeschlagen.
Wea. SATURDAY 12 Ther.
Wunderbar schönes, sonniges Wetter. Früh Mama, Arthur und ich zu Fuss zu Eddie. Dann Mama und ich Einkäufe gemacht, ich schlecht gefühlt, nach Hause. 
Mama mit Eddie zur Consultation bei Rubens. - Ich wieder ganz wohl. Nachmittag Frau Koelling uns besucht. Auch Arthur früh zum Kaffee gekommen. Frl. Gustchen fortgegangen. 
Abends Mama & Arthur in die Oper gegangen: "Traviata" underlined gehört: Auditorium. Ich zuhause. Bertha bei mir.
### page 467
Wea. SUN. NOV. 13, 1898 Ther.
Trübes, kaltes, unfreundliches Wetter. Alle wohl. Lange geschlafen. Arthur Vormittag zu Eddie & Tonie. Mittags wir drei allein. Nachmittag geschlafen. Eddie und Tonie zum Kaffee gekommen und zum Abendbrot geblieben. Mama wegen ihrer Angelegenheiten sehr aufgeregt. Ich Briefe geschrieben. Abends Arthur mit Eddie und Tonie ins Deutsche Theater gegangen: Lustspiel. Mama mit mir zuhause.
Wea. MONDAY 14 Ther.
Trübes, aber trockenes, gutes Wetter. Alle wohl. Ich früh zu Fuss mit Arthur in die Stadt. Dann zuhause gearbeitet. Nachmittag Mama und ich in die Stadt gegangen, viele Besorgungen gemacht. Bei Günther Chocolade getrunken. Arthur früh nach Haus gekommen. Abends in seinem Zimmer gesessen, gelesen, geplaudert. Mama wieder vernügt. Ich beklommen gefühlt, ein Bad genommen. Nachher wohler.
### page 468
Wea. TUES. NOV. 15, 1898 Ther.
Wunderschönes, kaltes Wetter. Ich früh zu Fuss auf Eddies office. Mama eilig (durch Boten) nachgekommen. Geschäftsverlegenheiten. Mama & Eddie bei Herrn Pietsch und Herrn Rubens. Letzterem die Vermögensverwaltung übergeben. Nachmittag zuhause, gearbeitet. Ich wohl, Mama nicht. - Abends Arthur und ich zu Ed & Tonie gefahren. Güntzel & Zeisler dort. Thee getrunken. Früh nach Haus.
Wea. WEDNESDAY 16 Ther.
Wunderschönes, kaltes Wetter. Früh aufgestanden. Alle wohl. struck through Ich zu Fuss mit Arthur in die Stadt. Arthur Schmerzen am Fuss gehabt, ich im Gegentheil. Den ganzen Tag zu Hause gearbeitet, gestickt, gekocht, gebadet. Abends sehr müde. Alle in Arthur's Zimmer gesessen. Geplaudert, gelesen. Ich geschlafen. Nachher mit Arthur grosse Auseinandersetzung gehabt. Sehr spät eingeschlafen.
### page 469
Wea. THUR. NOV. 17, 1898 Ther.
Trübes, nebliges Wetter. Ich sehr früh aufgestanden. Nachher mit Mama in die Stadt gefahren Einkäufe gemacht. Herrn Leicht getroffen. Ich zu Fuss nach Hause. Mama Nervenanfall gehabt, Nachmittag im Bett gelegen. Ich genäht, gearbeitet. Mama abends besser. Regen. Arthur und ich im Wagen zu Hüfners, Willi's Geburtstag gefeiert, Bowle. Sehr lustig. Nur Eddie & Tonie dort. Um 1/2 1 Uhr zuhause. Mama allein geblieben.
Wea. FRIDAY 18 Ther.
Regenwetter, Schmutz. Alle wohl. struck through Ich früh mit Arthur ein wenig zu Fuss gegangen. Nachmittag Damenkaffee bei uns: Frau Bluthardt, Emma Calder, Frau Ryckoff, Frau Zeggel & Janina, Tonie und Frau Brucker. 
Mama beim Kaffee Nervenanfall bekommen: schreckliche Aufregung. Mama ins Bett. Abends nochmals Nervenanfall gehabt. Zu Dr. Lee, junior, geschickt. Der ihr eine Morphiumeinspritzung gegeben. Nachts besser.
### page 470
Wea. SAT. NOV. 19, 1898 Ther.
Trübes, nasskaltes Wetter. Mama den ganzen Tag im Bett: sehr krank. Ich bei ihr zuhause geblieben, sie gepflegt. Selber Schnupfen gehabt. Viel Handarbeit gemacht. Nachmittag Dr. Bluthardt lange bei Mama. Arthur früh zum Kaffee gekommen. Abends ich abwechselnd bei Arthur in seinem Zimmer und Mama gepflegt.
Wea. SUNDAY 20 Ther.
Wunderschönes, sonniges Wetter. Mama noch immer krank. Ich ziemlich wohl. Vormittag mit Arthur einen Spaziergang gemacht. Mama den ganzen Tag in ihrem Zimmer geblieben. Nachmittag Frl. Lampring uns besucht. Sie auch Willi, Eddie und Toni zum Kaffee bei uns. Die drei letzten zum Abendbrot geblieben. Eddie und Tonie ins Theater gegangen. Willie bei uns geblieben, mit Arthur gespielt. Arthur, Willi & ich spät zu Fuss in die Stadt. Zu Fuss zurück.
### page 471
Wea.  MON. NOV. 21, 1898.  Ther.
Schönes, mildes Wetter. Mama noch immer krank, den ganzen Tag das Zimmer gehütet. Ich leidlich wohl, etwas Schmerzen gehabt. Früh zu Fuss mit Arthur in die Stadt, Besorgungen. Beim Zahnarzt, Boston Dental Parlors. Nachmittag zuhause geschrieben, gestickt, gebadet. Müde & abgearbeitet. Mama wohler. Abends bei Arthur in seinem Zimmer gelesen und Mama gepflegt. Früh zu Bett.
Wea.  TUESDAY 22   Ther.
Schönes, sehr kaltes Wetter. Schneegestöber. Ich früh mit Arthur zu Fuss gegangen. Mama den ganzen Tag krank in ihrem Zimmer. Ich zuhause, gestickt, gearbeitet. Eddie mittags gekommen. Dr. Bluthardt Nachmittag bei uns. Mama ein klein wenig besser. Furchtbare Kälte. Abends Arthur in seinem Zimmer, Mama oben. Ich todmüde: ein paar Stunden geschlafen.
### page 472
Wea. WED. NOV. 23, 1898 Ther.
Wunderschönes, sehr kaltes Wetter. Ich früh einen langen Spaziergang gemacht. Sehr wohl gefühlt. Wäsche geflickt. Mama noch immer krank. Den ganzen Tag in ihrem Zimmer. Nachmittags ich in die Stadt gefahren, Besorgungen gemacht. Dann gebadet. Schmerzen ihm Gegentheil. Abends zuhause: ich aber bei Mama gesessen, sie gepflegt.
Wea. THURSDAY 24 Ther.
Thanksgiving. Ich früh auf, herumgewirthschaftet, gekocht. Mama krank, den ganzen Tag in ihrem Zimmer, nicht zum Essen gekommen. Zum Thanksgiving dinner Herr & Frau Brucker, Ralph, Ed und Toni, Günzel bei uns. Sehr gutes Essen, ich präsidirt. Nachmittag alle gemütlich bei uns: Schach gespielt, geplaudert, oben bei Mama. Brucker früh fort. Ed & Toni, Günzel, Ralph, Frau Brucker zum Abendessen geblieben. Arthur mit Eddie, Günzel ins Bismarck. Früh zurück. Frau Brucker, Toni inzwischen bei uns.
### page 473
Wea. FRI. NOV. 25, 1898 Ther.
Schönes, kaltes Wetter. Ich wohl. Mama noch immer krank, aber aufgestanden. Ich früh zu Fuss mit Arthur in die Stadt, Besorgungen gemacht. Nachmittag zum Kaffee Frau Arnold, Mattie & Clothilde Schmidt bei uns. Sehr gemütlich. Baby-Sachen angesehen. Von Frau Arnold ein Paar Schuhchen erhalten. Abends Arthur sehr verstimmt: geschäftlich. Er spät ins Bismarck gegangen um Bitter zu treffen. Schneefall.
Wea. SATURDAY 26 Ther.
unclear
### page 474No transcription.
### page 475No transcription.
### page 476No transcription.
### page 477No transcription.
### page 478No transcription.
### page 479
Wea. Thur. DEC. 8, 1898 Ther.
Schönes, aber eisig kaltes Wetter.  Mama den ganzen Tag sehr krank im Bett mit Migräne. -Ich wohl, früh mit Arthur zu Fuss in die Stadt, Einkäufe gemacht. Bei Marshall Field Frl. Escheburg als Verkäuferin getroffen. Miss Lyons, die Wärterin, mittags bei mir; alles verabredet. - Mein Zimmer aufgeräumt, gestickt, Mama gepflegt. Arthur guter Laune: gute Geschäfte. Ich abends schmerzen gehabt. Mit Arthur Dame gespielt. Sehr früh zu Bett.
Wea. Wed. 9 Ther.
Schönes, unclear Wetter. Mama nicht wohl, den ganzen Tag zu Hause. Ich gut gefühlt. Früh mit Arthur zu Fuss in die Stadt. Zuhause sehr fleissig gewesen: Wäsche geflickt, gestickt, aufgeräumt. Abends ich wieder Schmerzen in Gelenk gehabt. In Arthur's Zimmer gelegen. Dame gespielt, gelesen, früh zu Bett.  Mama besser.
### page 480
Wea. Fri. DEC. 9, 1898 Ther.
Sehr schönes, kaltes Wetter. Mama wieder wohl. Ich früh mit Arthur in die Stadt, Besorgungen gemacht. Dann zuhause gearbeitet. Nachmittag Mama geschäftlich bei Rubens: ihm alle ihre Angelegenheiten übergeben. Emma Eitel den ganzen Nachmittag bei uns, zum Kaffee. Arthur Contract für Lincoln-Theater bekommen Champagner züm unclear
### page 481No transcription.
### page 482No transcription.
### page 483No transcription.
### page 484No transcription.
### page 485No transcription.
### page 486No transcription.
### page 487No transcription.
### page 488No transcription.
### page 489
Wea.  Tues. Dec. 27, 1898 Ther.
Schanns, kaltes, unclear Wetter. Ich sehr wohl. Früh zu Fuss mit Arthur in dieStadt gegangen. Mama Walther in der uncleart getroffen. Die Wärterin auf bei uns. Emma Calder mit Nellie & Marjorie uns befrust. Zum lunch Walter bei uns auf Nachmittag und zum Abendessen geblieben. Dr. Bluthardt bei uns. unclear abends mit Milton? im Theater: Cyrano de Bergerac gesehen. Mama & ich zuhause. Frü zu Bett.
Wednesday 28
Wunderschönes, klares, aber kaltes Wetter. unclear uns mit Mamaunclear krank im Bett. Ich sehr wohl. Früh zu Fuss mit Arthur in Dir Stadt gegangen. Gebadet, Mama ]]unclear]]. Die unclear unclder unclear bei mir unclear. Walter zum lunch gekommen, unclear unclear. Arthur mit uns unclear {[uncear]] nach Hause. uncleargekommen,  schlecht gefühlt. Mama besser. Abends unclear [unclear]] gelaufen.
### page 490
Wea. Thurs. Dec. 29, 1898 Ther.
Wunderbar schäns? war uns, aber schmutziges Thü- Wetter. Lange geschlafen. Arthur arger Sch-uhsen? gehabt, Mama besser, ich sehr wohl. Früh zu Fuß mit Arthur in die Park gegangen. Walther zum lunch gekommen,  nacjmittag mit Mama bis ? (die krank im Bett), bei Jürgend und Rubens geschüftlich. Auf zum Abendessen und = Walther bei uns geblieben. Arthur krank, erkältet. Ich in der Nacht = und = gehabt.
Wea. FRIDAY 30 Ther.
Trübes, kaltes Wetter. Ich ziemlich - gefühlt.
### page 491
Sat. Dec. 31, 1898
Morgens frúh um 4 Uhr unclearunclear unclear. unclear unclear im Luft geblieben. unlcear, den ganzen Tag im Haus uncldar, zum lunch unten. Arthur zu Hause geblieben, sehr erkältet, nicht wohl. Zum lunch Dr. Bluthardt bei uns. M--lther? zum lunch, bis spät abends bei uns. Dr. Bluthhardt und Eddie auf zum Abendessen hier. Uhm 6 Uhr unlcear unclear ärger? genorden? um 10 Uhr sehr arg?, um 11 Uhr zu Luft gebracht unclear. Arthur, Dr. Bluthardt, Miss Lyons immer bei rmir. Mama nicht im Zimmer.
Notes for 1899
### page 492
back of book