
## 962: Seventh U.S. Cavalry meeting notes, 1868
### page 1
A meeting of the Officers of the Seventh U.S. Cavalry, was held in the Camp of the Regiment on the North Fork of Canadian River, Indian Territory, the fourth day of December 1868, to take into Consideration the untimely death of Captain Louis M. Hamilton of the Regiment, who was killed in the battle of the Washita November 27, 1868, and to testify by resolution the respect and estimation in which the deceased was held by his Comrades in Arms.

     Brevet Major General Geo. A. Custer was chosen to preside over the Meeting, and a Committee was appointed to draft resolutions.
     The following was reported by the Committee:

Resolved-

    That the death in battle of our late Comrade Captain Louis M. Hamilton has bereft us of a dear and valued friend who, while living, we cherished as a rare and gifted gentleman of unsullied honor and spotless fame,
    That we miss the genial face, the sparkling wit, the well tried warm and trusty heart of him whose loss we mourn more deeply than words can tell. 

Resolved-

    That by the death of the heroic Hamilton, the Army has lost one of its brightest ornaments: that



[in pencil]  * boy Ayer N. A. MS 208
### page 2
Death of Captain Hamilton ---
Original Resolution
### page 3
he was a thorough, gallant Soldier, with heart and hand in his work: whose highest aim was to be perfect "without fear, and without reproach" in all things pertaining to his profession.

    That among the brilliant soldiers, who were selected after the closest scrutiny from the Armies of the East, and of the West for the new Army which was organized at the close of the late war, our lamented Hamilton stood in the foremost rank: that the genius of his mind, and the qualities of his heart stamped him as one of the purest and brightest soldiers of his Year and time: that his blameless life and glorious death, entitle him to a place among the departed heroes of his race.

Resolved-

    That the patriotic Ardor, and devotion to Country and duty, which rendered the Grandsire Alexander Hamilton illustrious, were truthfully perpetuated in the Grandson, tho best efforts of whose life were directed towards the re-establishment of the Government, which his progenitor had aided to build: whose life's blood was shed in visiting just retribution upon those who had savagely outraged every principle of humanity, and who had persistently refused to recognize the Authority of that government

2


[in pencil]  *box Ayer N. A. MS 208
### page 4
which he had learned from infancy to Venerate, and for the Supremacy of which he had fought on many famous fields.
Resolved

    That the Officers and Soldiers of the Seventh Cavalry, do hereby express their heartfelt sympathy with all who mourn the loss of the deceased: Especially do they tender the same to his relatives and family friends.

Resolved

     That the Secretary of this meeting be directed to transmit a Copy these proceedings to the relations of the deceased and that he also be directed to transmit a copy of the same for publication to the Army and Navy Journal, and to the Daily Eagle, a paper published in Poughkeepsie N.Y. where the deceased resided. 

The report of the Committee was approved, and the meeting adjourned "sine die".

                                                 G A Custer
                                                  B Maj Genl
                                                      U.S.A.
                                                            President

Rob M West
Bvt Colones USA
Secretary


3 end

Ayer N. A. MS. 208