
## 834: Richard M. Johnson letter to Florida Governor William. P. Duvall, circa 1829
### page 1
? Wm. P. Duvall

                                                 City of Washington

My dear Friend,
I arrived here this morning, & have learnt from the Sec of War that he had on yesterday protested a draft drawn on him by you for the outfit & expences of the Boyes which he authorised to be sent on last winter to the Choctaw academy
I was astonished at this information & immediately run over to the Bank hoping, that the draft had not been sent back but was mistaken
### page 2
But rely upon it that I shall immediately attend to this matter & have the measure sanctioned & your draft paid -- Majr Eaton had forgotten the circumstance of your conversation & mine with him several times on the subject & had supposed it had been done by McKinney alone.  It is probable that he may require the chiefs to sanction sending the Boys to Kentucky school or rather to get them to express a wish of that kind, to justify their education among the whites as the treaty contemplated that the school should be at the agency.  Rest assured my friend that I shall not neglect this unpleasant business -- Can I serve you?
as ever truly yours
(In haste)
Rh. M. Johnson
Gov. Wm. P. Duvall
Tallahasee
Florida
No 47