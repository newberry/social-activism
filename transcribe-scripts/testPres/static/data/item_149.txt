
## 149: Jane Everett letters, 1850-1894
### page 1
Chicago. Nov. 15th 1881.
Miss Jennie Everett.
Dear Friend: 
Yours received and should have been answered sooner.  Is that the Bourgeois bought for "Uncle Tom's Cabin?" If it is, the type is over 25 years old; but type used on hand presses, and used carefully as material on the "Cenhadwr" were used, will last many years longer than on power presses. New Bourgeois is worth here about 40 cents per lb. - yours I think, ought to bring about 1/2. Old metal is worth only 10 to 12 1/2 cents per lb. 
I was glad to hear that Mr. Davies had purchased the Cenhadwr. It ought
### page 2
to be kept in Oneida Co. Association

  We returned from our visit Sept. 8th jut about 6 weeks from home. Had a very pleasant journey. Have been quite busy all the fall. Had more rain the past two months, than we have for years, the same length of time.  Today is beautiful. 
  Mrs Mary Ann Lloyd visited us on her way East. We were glad to see an old friend from Steuben. 
  We hope Sarah is better - tell her, we expect to see her out here, next summer, - we can make up in large city and prairie, where we lack in hills. 
  Give our love to her and Annie, also Mrs. L. Everett and family..

Your Friend
T. T. Jones
801 W Madison St.
### page 3
Confidential 
Some time ago Rev. Mr. Davie's wrote me in regard to the purchase of the "Cenhadwr." Some, he said, advised him to have it published in an office, same as the "Cyfaill", &c.   I advised him by all means to buy the whole office and control the workmen, as the cheapest in the long run. By adding a few modern Job Fonts he could get considerable work out-side, and so work into a good business. I thought of the Bourgeois, and that you had no use for it, suggested that he print the first form in that type, and make as few changes in its appearance as possible. The material is old and would bring but little in any other way. I think Mr. D. is the one to take hold of the "Cenhadwr."
### page 4
Remsen Nov. 8, 1887
Dear Friend Jennie
Your letter was received a long time ago, we were so glad to hear from you and that you were feeling better. I did not intend waiting so long before answering your letter but have not felt like writing to anyone so kept putting it off from day to day. Well Girls I little thought when you were bidding us good by, that it was forever to fear from, and it is a good thing we do not know or I dont know what would become of us, we knew it is
### page 5
all for the best, still we do miss her so, she was always here and so willing to do for everyone. It don't seem as though we could live without here, but we must. She was very sick for two days suffered very much she looked so peaceful in her casket- I try to think of her as resting as we miss her more and more all the time. It is good it will wear off in time or we coudnt have stood it as well as we have if it had not been for the prayers of the people so many said we will remember you in our prayers. the people have been so very kind we havent been left alone but a very little since her death. Thear is nothing left for us but to prepare to go after her. Your sister Anna
### page 6
was down in the village yesterday Ma saw her but not to speak with her she is feeling better Ma has been up to see her twice once with Aunt Irma and once alone she thought Irma was looking much better  I wish you could all get strong so you could come to church and fill that seat once more Jennie we havent had sabbath school for two sundays, the roads were getting bad and so few stayed that we thought it was useless to continue it any longer. they did talk of having a sunday school meeting but guess that has been given up. I hope you will get strong so when the school opens next summer you will be ready to like your class we missed you
### page 7
Old Mrs. Rees spent part of the day with us. She is feeling well with the exception of the rheumatism. She is such a nice older lady. Mother and I went up to see Mrs. Griffiths (Geven) for a short time this evening. She is very busy quilting. The prayer meeting at John D. Griffiths' last night. There were about twenty nine there. It's near very good. Mrs. Griffiths is feeling considerably better and so glad for the girls and her. It would be so hard for them to part with her. Ma wishes to be remembered to you both. She is feeling as well as can be expected. She misses Nan so much. We would be pleased to hear from you whenever you can write. This with lots of love from your little friend. Rose
### page 8
this page is a duplicate of one from a couple of pages earlier
Remsen  Nov. 8. 1887
Dear Friend Jennie
You letter was received a long time ago, we were so glad to hear from you and that you were feeling better. I did not intend on waiting so long before answering you letter but have not felt like writing to anyone so kept putting it off from day to day. Well Girls I little thought when you were bidding us good by that it was forever to fear from, and it is a good thing we do not know or I dont know what would become of us. We know it is
### page 9
all for the best. Still we do miss her so, she was always here and so willing to do for everyone. It don't seem as though we could live without her, but we must. She was very sick, for two days suffered very much, she looked so peaceful in her casket - I try to think of her as resting we miss her more and more all the time. It is good it will wear off in time or we couldnt have we couldn't have stood it as well as we have if it had not been for the prayers of the people so many said we will remember you in our prayers. The people have been so very kind we haven't been left alone but a very little since her death there is nothing left for us but to prepare to go after her. Your sister Anna.
### page 10
was down to the village of yesterday Ma saw her but not to speak with her.  she is feeling better  Ma has been up to see her twice  once with Aunt Irm and once alone she thought Anna was looking much better Is wish you would get strong so you could come to church and fill that seat once more  Jennie we havent had sabbath school for two sundays, the roads were getting bad and so few stayed that they is thought I was useless to continue it any longer.  they did talk of having a sunday school meeting but guess that has been given up  I hope you will get strong so when the school opens next summer you will be ready to take your class we missed you -
### page 11
and told them they would soon follow told Maria to tell her Father, (he was to weak to make him hear) that he had experienced a change of heart, and was dying happy. he thought it would be a great meeting between him and his Mother, & Nell Anna feels very bad, but says of course it is right, but it is hard for her to say “Thy will be done” that makes three new ones in our grave-yard, since you have been gone: Nan, William Hughes and George we do miss Nan so much but we trust she is better off, but it was so bad for us to spare her, but she knew best. 
in margins
I havent seen William Price or Mrs. Price in quite a while. she hasent been to church for a number of sundays, they were as well as usual. I was over to Mr. Edward Davis on saturday evening. Mate was home for a week they were well as usual. Ma sends love to you both and wants to see you  It makes it nice to have you
### page 12
Remsen Jan.5.1888
Dear Friend Jennie,
I guess you have come to the conclusion that I am not going to answer your letter. If you have, I will disappoint you. You know the old saying is "Better late than never." but I imagine I hear you say better never late so I think and will try to do better next time. It is quite cold here tonight. Ma and I have been out to prayer meeting, the children have all united, which makes it much pleasanter, we had
Brother Eddie so near, he has quite a family now, I will be obliged to draw this to a close for want of place. Give my love to Mary and be sure and come home in time to teach our class next summer and we will try and study more on our lessons this nite love from your friend Rina I Thomas
### page 13
prayer-meeting here at our house monday night, there were quite a lot here and had a very good meeting, they decided that night to unite with the others we have not had english preaching at our church this winter, it is impossible to get up there in the evening now, we will be obliged to wait until summer. I suppose Anna has written you, that's our Rev. Mr. Davis, is married and has a boy six years old, you see how smart he has been since you have been gone and to think, he went to the methadist church to get her, don't you feel sorry for us, but I guess we will live through it. I haven't been up to see Irma yet. I expect you will scold
### page 14
me when you get near enough to me but it isn't because I haven't wanted to, but I could not. I haven't been outside of the village since Nan died except to church, but I intend going to see Anna as soon as I can. Mother hasn't been feeling very well, she took a heavy cold, didn't go out scarcely any for over two weeks, but is as well as usual now. Dear George Pugh was buried last Saturday, he was only confined to his bed for about two weeks, he was so patient. Maria and her father feel very badly, still they have a great deal to be thankful for he died so happy, was perfectly reconciled, talked very nicely to each one
### page 15
Business Logo
Best Advertising Medium in the Country.
Y Drych, Established 1851.
Thomas J. Griffiths Proprietor.
Utica, N.Y.
Book and Job Printing
It circulates in every state in the Union. /logo
Utica, N. Y. Oct 5 1894
Miss J.E. Everett

  Dear friend - there is absolutely no market for old papers. They tell me at the bookstore they are glad to have the Jews take their old papers away for nothing. If there are complete sets of Cenhadwr perhaps a little might be got for them at some second hand book store. Some time ago I tried to get a set for myself, but do not feel able to buy one now. Have too many doctor's bills to pay. -- the cuts in Uncle Tom's Cabin, I think, are not saleable except as old metal, and I presume they are mounted on wood, or perhaps are woodcuts. Of course some amateur printer might buy them for a song.

With kind regards, Benj. F. Lewis
Y Drych = The Mirror
### page 16
Postcard
postmark  OSAWATOMIE KAN. OCT 4  /postmark
Jennie Everett
Remsen
Oneida Co
N.Y
### page 17
Oct 4, 81
Dear Jennifer,
Your card rec'd recent have written a letter but send this instead.  Very glad to hear Sarah is some better.  Am suffering from a cold of a few days duration.  Today and yesterday had little fever in the afternoon which makes me feel weak while it is on.  What do you take for a cold?  Am troubled with nausea, and malarial symptoms.  Am taking ipecac and nux vomica alternately.  Think perhaps of trying Bryonice tomorrow.  I have mourned for Garfield as a near relative, as so many others have done.  Where is John?
Shall probably be all right by night. 
John
### page 18
Monday Afternoon
Dear Jennie,
I was so disappointed in not seeing you down here on the 4th, as Anna had begun to write to you and didn't have enough time to finish and though that I would write.  We were very sorry to hear that you could not come down to see us, but as you had an engagement to go to tho North Pond a visiting we did not think that you would come.  I hope that you had a pleasant time.  It is now vacation we had a very plesent tirm.  I have made up
### page 19
my mind to come out to see you next weak but can not state what day, for our folks is very sorry now.  We are all well at present but it time for mail and I must close goodby
David Ellis
### page 20
Chicago July 8th
Friend Jennie
All my friends having left me & feeling a little lonesome I now of no way in which I could spend a few leisure moments which I now have from the Busy cares of Business Better than in writing to some of my absent friends & so I thought it be you that would be the one
As for news. I have but very little to which I could call your kind attention to at present. This morning as I returned from Breakfast I found
### page 21
Miss Sweetland and Miss Northrup waiting at the Store to see me. not knowing but what you had Left some word concerning her trunk (Miss Northrup) she has not got it yet I went to the depot with them & saw Mr Staring the Baggage Superintendent & he said that he had sent several letters to the Dif Station on the Road to see if it was left -- but had Recd no answer from any of them do you recolect--what the numbers of the Check was & who you spoke to concerning it the trunk if you do will you pleas write me immediately on Receiving this as Miss
### page 22
N- is anxious to go home but will not go untill she hears from you or gets her trunk but I think that she will not get her trunk for very probably some one found the Check & claimed the trunk. 

 Well Jennie how are you feeling nice as a pin I hope but I fear that you & Anna seem quite fatigued before you got home how is Anna I have lots of compliments for which I will tell when I go to that part of Remsen What kind of a companion did you find Miss Prince agreeable I hope did she
### page 23
find a sweet new you in the cars and how was she feeling when she left Utica and how did find the folk at home
Enclosed you will find that beautiful moss which grows in the ocean and to use the language of the Poetaster Doctor Kilkany {Oh Lord, thou art fare} {But all thy beauty is not so rare
Jenny excus me this time and I will say no more concerning the M.D.
Now I will close with my love to all of yours and a kind good night to you from your ever true friend Owen
Write soon and direct to Box 3096 Chicago
### page 24
Jan 10, 1866
Dear Jennie 
I can't tell you how very happy I was to get your letter I feared I should not hear from my Jennie again, when she was so sick. But she is better now and must write often to let me know and love her better. How your letter caused me to bound over the yesterdays, and in memory live again those happy days we spent together, again enjoying all over pleasant chats walks and drives halting on the summit of that
### page 25
high hill thinking only of enjoyment and the hours spent at the sewing circle careing very little for the comfort of the Soldier or anyone I fear but pleasing myself. Well the war is over Slavery dead, and all are free, our beautiful banner spotless of all stain floats in the air on this new year, and we have almost forgoten the dark days of a year ago. the only item of conversation here now is the cold and wishing for snow. perhaps you have plenty of both and are contend, have you been on the hill this winter? How is Mrs. Moris I wonder if any
### page 26
body carried away of her grapes last summer and the Ames's how are they as pleasant as ever and the Williams and everybody else I suppose you saw them all. have you been to Bethel since I was there with you do you remember how much I laulght by telling you that wonderful story how foolish you must have thought me but I could not help it. I laugh now every time I think of it. Does Anna remmer wearing those waxen bales. Jennie my dear don't you want something in Utica next week that you may hear Anna Dickinson I think you would
### page 27
enjoy hearing her and could you not come to see me for a few days. I wish you would accept love from all including. Annie write soon. Wishing you a happy new year. I close. Annie
Miss Jennie Everett
Present
### page 28
annotation    [Jane]     /annotation
Miss Everett & Associate Faculty
of
Knox Female Seminary
Present.
### page 29
Remsen Oct 23rd
     ( Aunt? )

My Dear Jane

   I have waited patiently for this letter but I see no signs of it yet but I am in hopes you will not forget it

I am here at the old school house again this is the third week of the winter term my school is much smaller now than it was in the summer and I am very glad of it for I can have a better school

  Elizabeth Roberts commenced a select school last week in Pres Roberts old tea shop that building that is nearly opposite P.W. Roberts lawn she has over twenty scholars and they were most all my scholars but I have over forty scholars yet
  Laura is quite sick the Doctor says that she has the congestive fever she is very much as she was before the time she had bleeding at the lungs only not as bad but the Doctor says she is not dangerous although it will be some time before she gets well.
  The rest of us are quite well. Richard was home he went off this morning the first he has been home since the big meeting Kate is going to continue her school till January if not longer Your Brother Louis has been at New York he came home last week and looks better than I have seen him before this summer he works at the office now. 
  Sarah's health is improving too she sat up about ten minutes Sunday and she has walked as far as the door of the study Dr. Guiteau says that she has a long life before her yet and not only tells her so but others and I hope he is a good prophet in that case
### page 30
Mary is going Whiteboro and your mother is coming home this week and Mary says you should hear from her soon.

 I hope you will enjoy yourself and improve in knowledge and goodness I sorry for you if you are not satisfied but never mined it better days are yet to dawn. Remember this good friend and guide that is always near. 
  I hope as engaged as you are that you will not forget me I shall be very happy to hear from you it unclear  ?ile quite lonesome for me now after you and Mary have gone and Laura so unwell 
  We have very good meetings since the big meeting and we have society after meeting and who do you think stayed last Sunday evening at Capel Ucha? John Griffith Timothy and I think it a great start dont you? I hope the rest will follow.
  Erasmus and Maria had a very good visit when they were here and Erasmus had given him in money more than enough to bear their expenses. 
  They are going to start a singing school here in the Village this winter they are going to continue next Thursday night a gentlemen from Utica is going to teach. The last school that I attended before was the time Erasmus taught and I hardly know how it will seem to attend one now but I shall try.
   Well Jane I guess you think by this time that my mind has wandered all over while I have been writing but I must quit for it snows hard and it is getting late and so Dear Jane I must bid you good bye for the present.  

Signature illegible
in lower left corner carry
### page 31
File:A torn piece of grey and navy-striped fabric
alternative: I suspect the grey parts were white back in the day
### page 32
light pencil writing, not legible without tools
unclear Electricity
fluid that pervades all space
first manifestations 6000 yrs B.C.
unclear – Zink – Lead – Mercury
unclear projected during test cen.
Electricity pervades all space
unclear is more conductive Comes?
unclear to get quantity
several more lines not legible
### page 33
Wednesday Evening
Dear Friends: I did not get the address of Mrs Biglow until just now,-which is Miss Nellie Hollnan - No. 318. West 25. Street
Bessie was very much pleased with the cards. My teeth ached very hard till midnight and then I slept of course 9'o clk this morning it commenced again and I went to have it out But it continued aching until it has made me down sick. How is the fingers and how is Sarah & please come and see me, with love from Katie
### page 34
US POSTAL CARD One cent stamp
postmark
NEW YORK APR 21 930 PM
D 80
/postmark
Jennie Everett
433? W 34th St
New York City