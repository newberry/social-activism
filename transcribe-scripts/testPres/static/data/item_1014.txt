
## 1014: Indians in Virginia, 1689
### page 1
An Account of the Indians In Virginia and of some Remarkable Things in that Country Collected out of some Letters from A minister in Virginia Some few things are inserted concerning the English there, or the Bucaniers in some places of America An Dom 1689
### page 2
An Index page
their stature - 3
their age & capacity - 3
their complexion & colour - 3
their habit & ornaments - 4
their Food - 4
their Houses - 5
their Beds - 5
their riches & monney - 6
their drinking - 6
their towns - 7
their nations - 7
their Kings - 8
the mens employment or Hunting - 9
their games - 10
the womens Employment - 11
the Conjurers - 12
their diseases & Cures - 13
their marriages & divorces - 14
their way of educateing their Children - 14
their Burials - 15
their great men or Nobles - 16
the way of makeing their men -  16
their way of clearing their ground -  17
the canows - 17
their musick - 17
their Religion & Worship - 18
their priesthood - 19
their opinion about the transmigration of souls - 19
their opinions about a future state - 20
About the Delings? - 21
### page 3
the English government of Virginia 
The weather
The Hail
The Thunder
The quakes
The productions of the Earth, party growing tobacco as other's things unclear they made? traffock
Their Rivers & Fishes - 2
Their Beasts - 2
Their Fowls - 3
Their Heraldry - 3
Their way of Trimming themselves - 3
Their Characters - 3
Their way of managing Treaties - 3
Their mourning for ye Dead - 3
Their way of reckoning of their time - 3
Their way of entertaining strangers - 3
Their custome after their journeys or hunting - 3
The state of the English Churches in Virginia - 3
The Bucaniers - 3
### page 4
They are tall, well built strong men.  They come sooner to their full growth than here in Europe } The Stature of the Indians
They seldome live longer than 40 or 50 years.  Neither do ye Inglish who are born in Virginia live beyond that age ordinarily.  They are sooner capable of learning any thing than they that are born here } Their Age & Capacity
They are of a tawny colour and they make themselves more so by anointing their bodies with bear's grease which they do to preserve themselves from ye heat of ye sun, which they endure much better than the Inglish } Their Complexion & Colour
### page 5
Their Habit & Ornaments } They goe bare headed both winter & summer. the King and his Nobles wear something like a ring made of feathers or a coronet made of yr money unclear - gih yy caopook about their head, the crown of their head being bare. Their cloaths are Deer skins, wrapt about them; or some coarse stuff which they buy from ye Inglish. They goe sometimes all naked, except something to cover their privities. They take no care for ye future; They will not buy any more cloaths than they have present use for, tho they might have them never so cheap: they will not provide new ones, till ye old be worn out. They have holes in their ears where they hang some of their money & beads & such other things, as they esteem precious & beautifull. Ye women have their cloaths laced about with their money.
Their Food } They plant corn for bread. All their drink is water? They Love Rum mightily, when they can have it, for when they are drunk, they say that they are brave? men, they have no fear, and can fight any man. They live on Deer, & other wild beasts & fowls. Tho the Inglish have good store of Hogs & Cattle, and good flocks of sheep, yet they eat generally salt meat except in ye beginning if winter, when they kill the Hogs & their beeves: ffor all ye rest of the year, they are so bad Husbands, that they have no meadows? and so want hay for maintaining their Cattle in the winter; and then, they not only have no benefit by but many of them dye for want of provender. Gentlemen, who are more provident have fresh provisions all ye years; and because they cannot keep beef fresh in the summer, above a few dayes, & cannot dispose of a whole? beeve themselves, half a dozen or eigth Gentlemen joyn in?
### page 6
killing one, every week, by Turns, and send every man portion of it.  some of ye remoter Indians eat their enemies, whom they take prisoners in war. They eat snakes.
Their Houses are built of small branches of Trees wreathed together in form of an Arbour, which are very artificially covered with the bark of Trees so that no rain can enter } Their Houses
They lye on their Deer Skins, or on Their Beds couches covered with mats with deer skins over? ym or English cloath, when ye weather is cold.  All other times ye lye naked.
### page 7
6
Their Riches & Money. } 
All their Riches are skins with some Beads.
They make their money of a shell they call Wampum: 
Their smallest money is called Roanok wch
is their silver; & ye bigger is as their gold which they
call peek, and it is double ye value of their Roanok. 
The black peek is double ye value of that
which is white. The Indians buy & sell their money 
by Cubits : They have no standing measure, but
every man that is to pay it measures it by his own
arm, be it short or long.
### page 8
7
Their Towns }
They live in Towns. The Inglish in Virginia 
have but one Town, which is James Town.
and that deserves not the name of a Town, for it
consists but of twenty or thirty Houses, and most
of them are ordinaries, for the accomodation of
such, as have business at ye generall Courts & Assemblies; 
In the rest of ye plantations they have
Towns except in Carolina.
Their Nations }
Every nation has one or more towns according
to the number of people, whereof it consists. In
some of their nations, there are not above 12 or 20
men.
### page 9
8
Their Kings
Every Nation has a King. There are some Emperours who have severall Kings under them. When their Kings dye, their bodies are laid on Scafolds, which are erected on Trees : They set before them? Tobacco & pipes, Turky & Deer & other victuals and Pocoon, which is a root they paint themselves with all : There they remain till their flesh be all consumed, and then the bones are wrapped up in Skins or matts and laid up in their Gods house.
### page 10
All their Imployment is hunting. The Inglish
furnish them with guns, powder & shot for their
hunting } The mens Imployment, or Hunting
### page 11
10
Their Games.
One of their most Common games is with small? reeds, whereof there must be 81, and of those they? take up some by hazard, & they that have seven or elevens winn. They sit sometimes whole days & nights at this game, and have an Indian by them to light their pipes, or to fetch them water to drink. They play away sometimes all they have: only what they lose in money, if it be payed in skins or money? or Inglish goods the winner allows double the value more for it than if he were to buy it. Sometimes they play away their wives & children who become slaves to the Winner, sometimes they play away themselves to be slaves, which they do thus. First they play away their feet, then their Legs, so forward till they come to their head: Before they Lose that, they may redeeme the other parts, with less than when they have wholly lost themselves, & playd away their whole body; for then they must be slaves unless they can procure from their freinds some more considerable summe for their Ransom; They use to pawn their Arms or legs &c for some certain Summe. They use divers other Games besides this, as Runing, then ye prize is hung upon a tree & it is his who comes first & takes it down. They use wrestling also, which they do, All naked, except a flap before their privities. Anoyr Game is with a crooked stick & a Ball made of Leather stufft with haire: He winns that drives it from ye other between two trees appointed for ye Goal.
### page 12
The Womens Imployment
The Women build their Houses, plant their
corn, & do all their work. They call the Inglish 
men fools, in working themselves & keeping
their wives idle. The Women also keep all the
money.
### page 13
12
Their Conjurers
The Indians esteem them the wisest men among
them, whom they call Conjurers; Who consult with?
God, about the Success of their wars & other affairs.
They say that tho their God can tell them when their
neighbouring Indians have any design upon them,
yet he cannot acquaint them with ye designs of
ye Inglish. Their Conjurers do sometimes raise?
storms, or divert clouds from one place to another
and make them fall where they will. This they do?
by drawing Circles, & muttering words, by making
a dreadfull howling, & using strange gestures and
various Rites, upon which the wind ariseth &c.
Their Conjurers can find out any thing that is hid, or try?
and goe to it blind folded, except it be a Bible, which, has
been often tryed, & they have been forced to acknowledge
that our God is stronger than theirs.
### page 14
13
Their Diseases & Cures
Their priests sometimes pray for their sick, and then
they make many repetitions with a terrible howling 
noise, & violent gestures of their body, till they
are all over in a sweat. They put Myrtle & Holly,
and some other things into a dish full of water, & sprinkle
it all over ye house; They take of the sick persons
cloaths and sprinkle his naked body; and sometimes
they stretch themselves on the sick persons body, hands
to hands, face to face &c. Sometimes after this ceremony, 
the sick person riseth up on a sudden & walks
about ye house, as if nothing had ailed him.
When they are sick they hang over their Cabins
where they lye some Cubits of their money, and the
Doctor that cures them, has them for his reward: If
there be two or three Doctors, they share it among ym.
They never open a vein to bleed. They admire our knowing 
ones distemper by feeling the pulse. When they are
wounded, or bruised, they cut the flesh to make it bleed, &
suck out the bruised blood. In severall distempers and
also when they are overwearied, after a journey, or Hunting,
they shut themselves up in a hot house to sweat & from
thence, run into the cold water, which, they say, perfectly 
cures them. When they are in health, but dull,
the doing this, makes them brisk & Lively.
The Indians have great knowledge of ye virtues of their medicinall 
herbs, & perform extraordinary cures with them.
There are some sad distempers, very common among them. Many are taken
by? ye belly acke, wch is a dreadfull torment: many times, it takes away
use of yr limbs: after recovery, they seldome come to have ye use of their
hands, and Legs. There is anoyr Disease, they call ye Distemper, wch
consumes their throat & nose & other parts, as the French pox does, and
they use the same remedies. Few or none are cured of it there. The Inglish
here, have, few Doctors that understand anything, there being little
money among them. Most new comers have a severe fever &
unclear, wch they call the seasoning & most part have it the first
year. It continues a month or two.
### page 15
14
Their Mariages & Divorces
They have in some nations as many Wives as they can maintain, but
common sort being poor have generally but one. Some two.?
They use to lye with them before mariage, to see if they
can like them. They have no ceremonies in mariage
that I can learn; All that is required is the mutual con=
sent of the parties. The Bridegroom makes a present
of their money to the Brides father, who bestowes ye
present on his daughter. Then they feast & dance &
make merry.
They put away their wives at their pleasure, and take
others. The women keep all their money, & it is theirs as?
their husband put them away, but then they must main=
tain all the children:  And if they marry again the?
money belongs to the children; and, if there be no children, 
it is to be restored to their former husband.
The men marry comonly at 16 or 18, and the women
about 14 or 16.
marginal note: Sometimes ye man keeps ye Boys & ye woman ye Girls
Their way of educating their children.
They bring up their children to nothing but to use
a Bow & a Gun. They put them into cold water as soon
as they are born, & often afterwards in the cold of winter
they make a hole in ye ice & put them in, thereby to harden them.
The Young people during most part of winter goe into ye River
if they be near one, every morning, & wash themselves for a
considerable time
### page 16
15
Their Burials.
Formerly their Dead were buried standing upright,
with their face towards the sun setting, where they believe
they goe after they dye. Now they commonly lay their
dead all along in ye ground as wee do. (vide page 8)
### page 17
16
The way of yr making of their men.
There is a great deal a do, in making their Men.
That is in receiving their young people into unclear
number of their men, before which they cannot
be of ye Kings Counsell, or capable of any pre?
ferment, & they cannot know their God (as they
say) when he appears to them. The manner is
this; They give them a drink made of a Root wc?
growes amongst them, which makes them run wild?
Then they put them all naked into a House where
they beat them soundly, & after that they turn them out
into ye Woods, & put bowes & Arrows into their hands
where they continue perfectly wild & mad, for several
weeks: marginal note And when they come to themselves, they re=
member nothing of their former life; They forget their
Father & Mother, their wife, & every thing they knew
before.
It is certain, that this drink does mightily stupefy them
for a long time, and if they take a large dose of it, they forget
their very language & everything else. If the dose be very
moderat, after sometime, they remember what they had unclear
But they say they must not Let the Indians know it when it
is so, for then they would be made to undergo the whole cere=
mony of it over again.
marginal note 1 yt? yr? to make several men at a time yo yn there is always one of ym Lost.
marginal note 2 dureing ye whole time of this ceremony? yy have a keep to look after ym.
Their great men of nobles }
They have but 7 great men in a Nation, & no more?
besides the King, because (according to their Tradi=
tion), there were but seven saved in the great deluge.
### page 18
17
Their way of clearing their ground. }
The Indians before they had Iron Instruments from
the? Inglish, used for clearing their ground of wood
so they might? plant their corn, to pull off ye bark of the Trees, that they
might rot & fall; or to cut them with sharp stones

to burn them down: And so they made their Canows

little boats for going on ye Rivers & for fishing.
} Canows
### page 19
18
Their Religion & Worship.
They have all of them some sort of Religion. They
offer the first fruits of everything to their God, before 
they eat of it. This they do, as to their corn, their
fruits, their vines, their Deer, their Turkeys &c as they
are in season. After they have made their offerings to their
God, in ye next place, they make a present to their King.
Their offerings are burnt without doors, on ye ground. They
have generally no Altars struck through: of stone, or of any other sort They burn
ye fat & head & neck & some other particular parts of the
Deer. They make offerings also at ye full moon, and
when they go to war, or undertake any great hunting &c. when
they lay up in their Gods house, & after some time, burn this?
But it is thought that ye old men doe herein cheat ye young &?
do really make use of a great part of those offerings.
In the morning, whatever they are to eat or drink, before
they taste of it, they throw some of it, into ye fire, or on ye
ground. They pray to their God, & praise
him after their manner. Their priests sometimes pray
for their sick (see page 13 concerning Diseases & cures).
In their Gods house, they have not only ye Images of a man?
for their God, but also the skins of Wolves, foxes, Ravens &
other creatures stufft with straw, and glass or beads in the
eye holes; These they worship as Gods, when they go to war, go?
a Hunting, and they think thereby to have ye swiftness, cunning, 
strength, & all the other qualities, that they observe in
those creatures communicated to them.
Their god does appear frequently to them, in the shape of a
handsome young Indian, but their boyes do not know him, unclear
they see him, till they be made men. vide page 16
They think we have one God, & they another, & that ours is better?
& stronger than theirs, because wee are better provided for, with?
### page 20
unclear, drink, & cloaths &c. But that they dare not own, nor 
worship? our Gods, for fear their own God should destroy them.
Some of them worship the sun or moon, as the negro's doe.
Their priesthood }
The Priesthood belongs to a family, and they have
it? by Inheritance.
Their opinion about ye Transmigration of souls.
They say when they dye, their last breath becomes
a Living Indian.
### page 21
20
Their opinion about A future State.
They believe, that Good men after death goe in?
a far countrey, where there is no extreme heat, no
cold, nor storms, but the air alwayes clear & unclear
where they live on ye choisest meats & drinks & the
women never grow old. But as to ye Wicked they
believe that they wander up & down about their
marishes & feed on coarse herbs. They say that ye?
horrible wild place to which the wicked are condemned
is within sight of ye other, but they can never come to?
it, for ye bushes & briars, & swamps & marshes, that are
between them and it. They say yt ye wicked after this
life are alwayes hungry & thirsty, but have nothing to eat?
& drink, but what is raw, As Frogs, flies &c, there being
nothing there to dress their Victuals withall.
### page 22
21
{ Their Tradition about the Deluge
They have a Tradition of the floud that all the
world was once drowned, except a few that were
saved?, to wit about seven or eigth in a great
canow.
### page 23
22
The English Government in Virginia }
In Virginia there are about 20 Counties; And in
every County there is a Court, which consists of about 8
or 10 Justices of the peace, who are ordinarily the
Gentlemen of the best parts and Estates: Twice in the
year, there is a generall Court, as they call it, held at
James Town for greater matters & for Criminall cases, by
the Governour & Council. Some of the Chief Gentlemen

of the Country are recommended by the Governour

to the King to be of his Council, who are ordinarily
commissioned by the King. The Countrey is governed 
by the Laws of Ingland mostly; tho there are other
Lawes, which ye condition of ye Countrey requires, wch
are made by the assembly, which the Governour calls
at pleasure. This Assembly consists of the House of
Burgesses, as they call it, that is, of two Gentlemen from
every County chosen by the ffreeholders; Those they call
their Burgesses, and they make, as it were, ye Lower House
of parliament: The upper House is the Governour and
Council. When an Act is passt in ye House of Burgesses, it
must be approved by the Governour & Council, and then
sent to Ingland, to be Confirmed by ye King. insert marginal note
At the first planting of this Country, people were not
restrained to a certain number of Acres of Land, as, in
New England & Pensylvania; And so every body endeavoured

to take up great Tracts of Land, seing they cost

them nothing but the charges of surveighing, & taking
out their patent, & the quit Rents payed to ye King, wch
was formerly a shilling for a 100 acres, but is now two
shillings or 24 pound weight of Tobacco; and so their having such great Tracts of Land
(as some have 10 or 20000 Acres, & most part three or
four hundred) makes the Countrey to be thin seated, and
ye people very remote from one another.
marginal note: what is granted by ym is still in unclear till it be repealed by ye K. & yt is ones unclear by ye K. cannot be repealed by H? wtout their own unclear
### page 24
Both South & North Carolina was to have been joyned to the
Government of Virginia in ye reign of K . Ja . 2 .
but now it is as it was
### page 25
24
The Weather. }
It is extreme hot from May to the end of August. September

& October, March & April is very pleasant, &

moderate weather. From November to March, they have
much cold weather, & hard frosts, especially in January
& February. Their weather is wholly governed
by the wind: When ye wind blowes from ye north west
where there Lyes a vast Tract of mountains, in the winter 
its extreme cold, & freezeth; And it flows cool
even in the middle of summer. When ye wind blowes
from ye south it is so hot many times, even in December
& January, that people go abroad in their shirts and
Drawers, as they do in the summer.
The Hail. }
Sometimes, they have extraordinary hail, which destroyes 
the corn & fruit, where ever it comes; sometimes 
kills the young Cattle, & breaks all the glass windows
in peices. The Hail stones there, are sometimes as big
as pullets eggs, & some of them nine or 10 Inches
about.
### page 26
25
The Thunder }
It thunders there very dreadfully all the summer : And
there is scarce ever any rain in the summer but thundershowers.
The thunder does not so much injury there, as it
might doe, if the Countrey were thicker seated. It falls sometimes
on a plantation : And everywhere almost one may
see in the woods, trees split with it.
Their Snakes
There are many sorts of Snakes in Virginia, but 
ye worst are ye Rattlesnake (whose bite is present death)
and ye horn snake, whose thrust with his horn in his tail
is also certain death, and there is no remedy for it. The
Indians know a remedy for ye biting of ye Rattlesnake.
They have a root insert from margin which they carry along with them when
they go a hunting in the woods, which is a certain Antidote
against it. These Snakes are not so very comon; &
they never trouble a man, except unawares he tread on
them, or on any bush, or stick that may touch them, which
if they do, they will be sure, if they can, to bite him, or
to thrust at Him.
(Margin):
reduced in powder?
### page 27
26
Their Tobacco & other productions of ye earth with such other things wherewith they traffick. }
The principal Commodity of the Countrey is Tobacco & there are two sorts of it : Sweet Scented & Arenoko; The
Sweet Scented, which is the best for smoking is all spent in Ingland; The Arenoko is transported into Holland
& other places, where they like it better than ye Sweet Scented.
There are 2 sorts of the Aronoko, one is of a dark colour, 
with a thick leaf & stong substance which is really 
best for ye pipe : The other is of a bright colour, and of
a thin light leaf; This is fittest for ye Dutch market, for
they sell it again in Retail by measure, & not by weight.
Some make great advantage by their trade with the Indians
whom they furnish with cloaths, guns, powder, & sho?
for their skins, which they send to Ingland. They send
flower & bisket, & pork & pipe staves & Tobacco into Barbados?,

& fetch from thence Sugar, Molasses Rumm, Ginger

& other spices. The Inglish likewise send wheat to Madera,
for which they have Madera wine.
There are an abundance of grapes that grow naturally. Some
have made small quantities of good wine: But this is
neglected, as many other improvements that this Country
is capable of. There is abundance of extraordinary
good Cyder made. They make beer not only of
Barley, & wheat, & Indian Corn, but likewise & most commonly
of Molasses, corn stalks, & of potatoes &
pumpkins, & divers orr? things but it is very windy, And
yet it is the Common drink of the poorer sort of the
Inglish, who drink also a great deal of water almost all the
year. They drink Rumm in Drams?
The Contrey produceth many sorts of good fruits, which
run upon ye ground, & where of almost all people plant
some, as Simnels, mecocks? cashaws, pumpkins ~
gourds, muskmelons, & watermelons, which last
is a great refreshment in a heat of summer, and so have
unknown that they never do hurt; They are very cooling in fevers.
This fruit grows not in Ingland, nor for ought I know anywhere in Europe.
### page 28
27
There are many sorts of good Herbs for salad; purslam?
groweth wild & strawberries everywhere in cleared ground.
There is a mighty variety of medicinal herbs. There is
plenty of fruits, Cherries, plums Apricocks, Walnuts,
Chestnuts, figs, excellent peaches, very good Apples
& good quinces &c All Inglish grains: as wheat, Barley
oats, grow well. Their worst ground produces Indian
Corn, or Maiz, whereof most people make their bread.
& Hominee & Mush which is ye comon food of servants
& the meaner sort. The Increase of their Indian Corn,
is three or 4 hundred for one it is planted on a
Hill at some foot distance the increase may be sometimes
5 or 6000 for one. Of one grain there are many
stalks; on every stalk there is one or 2 ears or more. On one ear,
one or two hundred sometimes six or seven hundred grains.
A grain of wheat produces the same increase if it be unkown at
a convenient distance to send out many stalks : But the
ordinary increase of wheat as it is sown there in the
Inglish manner is 15 20 or 30, & sometimes more; And
of Barley 30 or 40, sometimes more.
Mulberry Trees both black & white grow very well
there, and there are many silk worms, that 
are naturall to ye Countrey. There are diverse sorts of
plants & Woods, used by dyers. There is abundance of
pitch Tar, Rosin, & Turpentine. There is good stove
of Timber for Ships & Building. It produces Flax
Hemp Hopps &c Silk-grass growes there naturally,
& is not much inferiour to flax, & maybe
made better.
(Margin): 
plumbs are not good in Virginia

 unknown boyled corn.
+ like unknown pottage or meal & water
### page 29
28
Their Rivers & Fishes. }
Virginia has 4 great Rivers. James River, York River, 
Rapohanok, & Potomack. They come from the mountains 
that lye on ye west side of Virginia & run Eastward
into the Sea. They are navigable a hundred, or a hundred
& fifty Miles: & upwards. Potomok divides Virginia
from Mary Land on ye North. A small Tract of Land, on
ye Southside of James River divides it from Carolina on 
ye South. There is but one road from Virginia to North 
Carolina : And from ye most Southerly part of Virginia
it is not above 40 or 50 miles to Carolina.
Besides those 4 great Rivers, wch are in some places 2
or 6 or a dozen miles broad, or more, there are a vast
number of Creeks as they call them, which run into the [?]
These are greater than our Rivers & navigable for Sloops
& Boats, & are of great use for ye transporting of their Tobacco.
The Rivers & Creeks stored with great
plenty of fish, as Sturgeon Drummes, perch, Eels, 
Trout, Cat, Sheepshead &c.
(Margin): 
There are also a great number of lesser Rivers as Elizabeth R. Nansimond R. Hampton R. ..
### page 30
29
Their Beasts }
The land is well provided with great plenty of Beasts
as Deer, Bare, Elks Racoons Hares, Squirrels. Possams, ~
Beavers and many others good for food, & their skins for merchandice.
The possam is a rare creature
for its unknown After ye young are brought forth they illegible run into illegible
### page 31
30
Their Fowls }
They are plentifully with Fowl
in the winter; as with swans, wild
goose, ducks, unknown brants,
widgeons &c. There are likewise plenty of
partridges, Turtle Doves, Blackbirds, pidgeons, wild
Turkeys with many birds of curious colours; some
all red, some blew, some of various colurs; there are
some noted for their singing, as the mockingbird, which
is called So, because it imitates ye notes & tunes of all
other birds. There is also ye Humming bird, the least &
unknown of all others. It sucks flowers & herbs as the bees?
do, : It sleeps all the winter. The way to catch this
bird is either by a trap, or to put water or sand in 
a gun, & therewith to shoot them out of a gun, 
so to drown them with water, or to fell them with
ye sand.
### page 32
31
Their Heraldry
The Indians have a sort of Heraldry among them.
Every great family has some particular bird or
beast that belongs to the family in their Nation,
the skin whereof they have usually stufft & hung
up in their houses, or before their doors, which is
as it were their coat of Arms.
Their way of Trimming them selves. }
They know of no way of shaving themselves or cutting
their hair, as we do, and therefore they pluck 
up the hair of their beards by the roots.
### page 33
32
Their characters 
One of ye greatest of ye nations of ye Indians who
are neighbors to the Inglish, who are called the
Duskeruries? have an art, by drawing of a 
strange sort of lines to express their
thoughts in somethings, Their way is to make such
figures with sharp bones or shells on ye Inner side
of ye bark of Trees : Some doe it with a kind of black?
lead which they buy from ye Inglish. By these figures
they understand one another without words. But they
have not any such Characters as express single 
words.
### page 34
33
Their way of managing Treaties.
When ye Inglish are to make peace with
them & propose divers Articles to be observed on both
sides, the Indians use this method to supply
their want of Letters : That they may distinctly remember
what is proposed to them; They make one
take one Article, and another a Second, and another
a third. & so divide them amongst them; Every man
is careful to remember his own; Then they confer
about them; And by this means they are able to
resume? a large discourse, consisting of many parts
& divers Articles & to make a distinct answer
to every one of them.
### page 35
34
Their mourning for the Dead }
The women only mourn for their Dead, which they
do by blackening their faces & howling grievously
every morning. Sometimes for half a year or a year.
Some do it both morning & evening.
Their way of reckoning of their Time }
They reckon their time by Dayes & moons & years. The
year begins when the Geese & other fowls comes thither
which they do every year from Northern Countries about
the beginning of winter. They call their years Copahunks, 
from ye sound, the geese seem to make.
### page 36
35
Their way of Intertaining strangers }
When a stranger comes to their House, the Chief man in it
desires ye stranger to sit down : Within a little while, he rises
& toucheth ye stranger with his hand, saying you are
come : After him all ye rest of house doe the same.
None speaketh to him or asketh him any questions, till
he think fit to speak first.
Their custome after their Journeys & Hunting }
It is their custome when they have been on a Journey or
a Hunting, wherever they come tho, it be their own House.
They sit down first & compose themselves; Or they take
some refreshment of meat & drink if it be needfull,
before they say one word : Thereafter when they think
good they begin to talk, & enquire of all matters, or 
give an account of their own progress, of the good, or
bad success of the affairs they have been about.
### page 37
36
The State of ye English Churches in Virginia }
There are about 50 parishes in Virginia. There are
many little poor parishes not able to give a Ministeray?
competent maintenance; so that two of them must joyn
to have one minister to preach to them every other Sunday.
The ordinary Stipend of the Minister is 26000 pounds 
weight of Tobacco, wch some years does not amount to
50 lib Sterling, and other years it may amount to 150
lib St. Some give their Ministers 20000 pound weight 
of Tobacco. The Ministers Stipend (as all other
publick charges of ye Countrey) is raised by an equal 
imposition on all ye Inhabitants, whether ffreeholders,
or tenants, according to the number of their servants
that work in the ground, which they call Tythables.
In some parishes there are 300 Tythables; In
others 400, & more.
There are abundance of Churches empty in all places
of that Colony; and in most places, where they
have Ministers, they had better have none, there
being more hurt done by the Scandalous example
of their Lives, than good by their Doctrine.
There is no manner of Church Government among 
them, but every man does what seems good in his
own eyes. Prophaness reigns in most places to
an excessive degree, And where there is a man
more serious than ye rest, the Quakers do presently
catch him.
No great matter has been done there, as yet
towards the Conversion of the Indians. Some few
have taken pains to Instruct the Negro Slaves.
In most parishes, The Substantiall planters have
of these Negro Slaves, which the Merchands
purchase
### page 38
either immediately from ye Affrican Company in Guiney?, 
or at Second hand from Barbadoes. The children
of those Slaves that are born there, are generally Christianized :
But till of late, the Men & Women were neglected.
Some Scotish Ministers that are there have imployed
their endeavours in preparing the aged Negro's for
Baptism. They have found considerable difficulties
in this; Their Masters keeping them constantly at
work, except on Saturday afternoon, & ye Lords Day.
Since ye writing of ye former unknown, ye hath been some
care taken of ye plantations by ye Society at London for propagating

ye Gospel in forreign Parts
### page 39
38
The Bucaniers }
The Bucaniers were never entertained in Virginia. 
They were called first by that name, by the Spaniards
because they Lived by stealing of their cattle that run
wild in Hispaniola & other places, and sold the Hides
& Tallow. At the first discovery of America. The
Spaniards let Beeves & Hogs ashore on every
Island, they espied; which increased mightily in
those places where they have a constant Summer
and the grass alwayes plentiful. They rob
now at Sea, & have left off most of them their killing
of Beeves; and instead of that, they spoil the Spaniards
Towns, & Ships that they can surprise,
of their money : and when they have got of that
they never rest till they have spent it all somewhere,
or other, in drinking, gaming, & whoring
and then they set out upon some new adventure
And being desperate men, a few of them in a Canoe
surprise great Ships, and get aboard of them before
### page 40
they? are aware. It was the intertaining of them
in Barbadoes & Jamaica that did first enrich those
Islands, & now since there are strict orders from ye
King against their being intertained there they have
not such plenty of money, as formerly. They are
intertained now in Saltotudoes & in South Carolina
& some other places.
Their Whirlwinds or Gusts }
They have seldom Hurricanes
which shift about ; But they have 
frequent Gusts, which blow in
a strait line & blow down unknown
unknown is in their way. They
move for some scores of miles.
at their first appearance they
are of ye breadth of a unknown
unknown, but spread as ye go on,
above a mile.
### page 41
40
Their Hunting.
### page 42
Their Salt
They used up roots of trees
burned to ashes, for salt.
Their Punishments
They don't punish any crimes 
wt dth, only ye pay so much 
money. But ordinarily when? a
murther? is caught? ye friends take
revenge.
### page 43
42
Their Hunting }
Wn ye go a hunting, ye go 50 or 60
or more, unknown of ye English unknown
unknown many miles of
their ground, making a ring of the
dry leaves all about ye
unknown of ground, ye placed themselves?
unknown to shoot the deer ye, unknown
ye leaves on fire ? catch all ye and
unknown ye fiery circles. perhaps? 50 or 100
for ye deer will not pass through this fire.
### page 44
fire.
Their Fishing
They throw upon ye water ye
lines? of an Herb, wch brings up
fishes to ye surface of ye water
so ye unknown ye catch unknown wt ye Hands.
or taken sturgeon? by ye tail wt a nooss, for wch run or 1st dive
fish into water.  They shoot
fish alewifs wt arrows.
Their Wax
They make wax of a unknown
of Bay Leaves, wch is as
good as Bees? wax
### page 45
blank cover