
## 67: Sarah Everett Pritchard compositions, 1850s
### page 1
Charleston So C Jan the
My Dear Adelaide, 

 I arrived here after a long but pleasant voyage I suppose I need not tell you that I was sea sick terribly so. So much so that I had to keep my berth the greater portion of the time but I think you can better imagine than I can describe I am now staying at a place called Rio Janeiro on the coast of the Atlantic Ocean. All the large cities are along the coast Brazil include the entire table land of Brazil and nearly all of the Forest of the Amazon it is therefore richer in vegetation than any other part of South America the Mountains of the interior contain mines of gold and diamonds with iron an other kinds of metal the larger part of the coffee and and diamonds produced in the world comes from the above named place Rio Janeiro the capital of Brazil is largest city of South America it is nearly o[f?] half the size of the city of New York in North A[m]erica Rio Janeiro has a lovely harbour So large and beautiful and is the principal place of export for coffee and diamo[nds] Oh Adelaide I wish you were here it pays for all the tediousness of the voyage it so beautiful tell you[r] Mama houdie for me I wish I were an Artist so t[hat]
### page 2
in my next I hope to give you much better account I am your loving and affectionate friend
Sarah
PS
you have my address already
To
Miss Adelaide Gillies
Charleston
So Ca
### page 3
"Sarah my dear" 

     I had thought ere this to have seen you face to face but have not been able to bring it about. We have had company almost all the while for the last two or three weeks; and of course I have been very much engaged have not even seen the time when I could pen a few lines to my much loved Sarah. Now I have gone up to my room for the night but I shall not press my pillow till I have conversed with you a little. Oh how I wish I knew how you are this cold wintry evening I imagine you about closing your eyes
### page 4
for the night. May your slumbers be peaceful while angels watchful vigils keep. Mr Kellogg staid a night with us week before last. He brought me good news from you dear Sarah, I have imagined you many a time since walking from the bed to the fire but it dont seem hardly like you, you and the bed seem almost inseparable though perhaps the rocking chair lays the strongest claim now but I will not be to sanguine life is full of ups and downs, may you and I both so live that however we are situated we may feel each cloud has a silver lining and that He doeth all things well. Do you want to see me? Well I sat down to tell you that if nothing happens to prevent I shall set my feet towards Remsen next week, but I wish I knew if
### page 5
you are well enough to have company if you are not as if it would be more convenient to have me come some other time do not hesitate to let me know, for I should take no pleasure in my visit if I felt it was an injury to you - you will be perfectly candid will you not? I hope you will get this tomorrow and then I shall have time to get any word as I should not go before Wednesday. I shall not write much this time for my eyelids are heavy and I hope soon to see you. 
ever yours
Sarah
Friday evening
### page 6
Dear Lodge? - To redeem my promise I must burden you with a shower of words to night and if a few ideas can be sifted from the fragments, I shall be glad gratified satisfied.
I have been wanting an opportunity to say unclear rejoices my heart to find that our honored brethren are really not afraid to speak of temperance in connection with the ballot box. Act up to your convictions, good brethren, that is all we ask of you, but that we do ask -  with earnest entreaty.  unclear You will not be too near right if we follow our trust? noblest impulses and cleansed convictions lead us on a truer path path -  but they will certainly be nearer than if we consult policy? alone (or and if by patient sifting you can glean? a few ideas I shall be highly gratified
Strange how fearful we are to
You will then be none too near the right but will find yourself treading a truer path than the one where policy and self interest lead.  I would like to take a peek? at you all to night to see how the working harness fits.  I have been thinking that perhaps some would rather work between our sessions than in them and that such might find abundance to do.  There are numbers of poor inebriates around us whose forlorn cause some friend of humanity ought to espouse,  Might we next have a standing [com.?]] whose duty it should be to report evening intemperate person within the reach of our influence?  This we might resolve ourselves into companies of 2 and 3 brothers and sisters of Charity whose mission of love it should be to labor week after week with one of these unfortunates.  How better could we work out the principles in the sublime degree of Charity
I do not like to think that within shadow almost of our walls should live a family whose members none of our efforts can reach.
Let some of our sisters who we know are weary with longing and wishing for good results go with earnest sisterly love to Mrs. Williams and proffering encouragement and sympathy
### page 7
beseech her to join our order.  One visit might have an effect perhaps except to rouse opposition, but few can resist long continued effort for their good.  Others, whose out gushing charity is large enough to take in all the brotherhood of man might try their influence with Mr. Williams and see if there is - crossed out are not some dormant elements of humanity ^still slumbering in his bosom.  Then those two little boys, they ought, I think they can be brought into the children's lodge.
Those brothers too who to the solemn Do you thus promise [[but who have once unclear ]] have responded "I do," should not, must not be given up.  Cannot a company - crossed out company of our members go over - crossed out in a load to their parents houses and let them feel that they have not forfeited all sympathy and respect.
Lest a few words fitly? chosen be dropped in their ears, and God may ^yet save them.
Men tell us that woman's [[^and man's hearts? ]] power is great 
If that tongue? show more nobly can it be used then to win back the fallen.  We have been surprised that so few ladies have been appointed on our committee to labor with those who violate their obligations.  There is ^also another power, greater far than woman's pledged ever on the side of right and if with earnest christian?? faith we look for the aid God can give we can hardly fail of success
### page 8
I'll always remember that you have been here
written around other text that has been crossed out
God bless you dear cousins
Wherever you go
And though we may never
Again meet below
If we are but faithful
As time passes by
A glorious reunion
Awaits us on high
For if we love Jesus
Whatever may come
We'll soon meet in Heaven
Our beautiful home.
S.A.E.
August 27, 1866.
### page 9
To Mr. and Mrs. Sherwood.
I went to the parlor
The morning you left
And found it so lonely
So wholly bereft
No trifle forgotten
No token did stay
To be a reminder
Of those gone away
I looked all around me
From bureau to chair
And could not find even
As much as a hair.
But though you left neither
A ray nor a crumb
To say that you ever 
Had been in that room
You left in my bosom
A friendship so dear
### page 10
Why, Oh why should I so oft have wandered from the fold of our blessed redeemer? Why, long-ago, did I not give myself entirely to the Lord, "Lay all upon the alter and say here I am" what wilt thou have me to do? But Oh, instead of this how may years have I spent without God, and without prayer: searving sin and death and living only for myself? and even now, my efforts to glorify my saviors are so feeble, so faint, and as it seems, for aught -- I can see, so unsuccessful, that I am sometimes led to doubt whether I realy am Gods or not. Oh Satin thou wouldst if it were within thy power, ruin forever ruin my poor trembling
### page 11
soul by these gloomy thoughts; but God would not suffer thee. And often, when I am led astray, and am almost ready to murmur at the ways of providence, because I cannot see the great design of God in sending them, have I been checked by His long-suffering and gracious mercy, teaching me that I am wrong, and gently drawing me back to him; and I have been forced to exclaim, how wonderful, Oh God, is they forbearing love, and how great are all thy tender mercies which thou art daily manifesting toward us. Still, through all this, my hard heart has remained so cold and thankless. Oh, we never, never
### page 12
can repay him for what he has, and still is doing for us. All we can do is, to return to him, and say, with the prodigal son "Father, I have sinned against heaven, and before thee, and am no more worthy to be called thy son; make as one of thy hired servants." Father in heaven, being one more thy [smudged], sinful child, into thy arms of love, and wilt, over her, and guard her from all evil; enable her to resist all temptation; and may she never wander from thee but ever live a faithful follower of the Lord Jesus, humbly trusting that when this life is past, she may
### page 13
forever praise thee in a bright and happy land, where sin and sorrow shall never enter, but "God will wipe away all tears from our eyes." S. H.
### page 14
Nature.
Come with me and I will lead you forth amid the green fields to gaze on the beauties of nature and to meditate on the wisdom and goodness of natures God.
### page 15
a bitter aspect it does wear, When other old sling note of mine

Your accents all are in thire unclear mile? miliars wears a seiming graer transcription unfinished
### page 16
Or is it I forgotten am 
By dear neglectful Uncle Sam. 
Ask Anna how it is that she 
can't possibly me come to see 
And ask her if she cannot now 
Put forth rest of line undecipherable because of fold in page 
And not look back until she's done 
writing me a great long run 
of things all sensible and true 
Or if she likes foolish will do. 
You and I must be very  mum 
While under Mr. Forman's thumb. 
Oh sister dear how do you do 
this cold, and stormy morning too
I've read your letter sister dear
### page 17
Sarah's journal for Jennie

Friday, April 9. - It is quite late in the evening now, almost nine o'clock.  I am expecting Anne in every minute to put me to bed.  I have been out again today, and have been wheeled around in the halls so that I have seen a good part of the house that I never saw before.  Everything now that I see, looks so strange, so wonderful.  It seems almost as if a new life was opening before me, and I am about to enter into a new world.  i dread to think of the temptations to which I should be exposed if i am again permitted to enter into society.  I have so long nestled under the protecting wing of sickness that I fear to have it withdrawn lest the bright rays of worldly pleasures shall prove too strong for my weak mind, and all my good resolutions should be withered.  I must leave off abruptly, for Anne has come.
Sat. 10. - Cynthia's birthday.  Thirteen years ago, all spotless and pure, fresh from her Maker's hand this sweet creature was placed in our happy circle.  Time has borne her from the sweet garden of Infancy and he is now gently leading her up the wild but beautiful mountain of Girlhood.  Oh, may the angel of innocence accompany her, and may the fountains of love and peace flow ever at her feet; I had a very pleasant time taking my ride today, they took me
Cynthia Everett was born in 1839, making this date 1852. RMR 1/19/2021
### page 18
out on to the plank walk that passes under my window, and as I need when they let me stop and look in through the dining room windows. One of the girls handed me a piece of pie through the window and Kate Burnham and I ate it out doors! and then the gave me some bread and butter.  Don't you think it was almost the same as if I had eaten at the table? It is nearly bed time, so good night.
Monday 12. - It is damp and dreary today, the sweet smiles of yesterday have gone, and a look of sober sadness overspreads the face of Nature.  I do not remember as anything of much interest has occurred today, excepting my going out, which, of course, was interesting to me.  This makes the fifth time that I have been out.  Oh, what mouth? I must have given four months ago to have been so well as I am now.  But how strange the effort of habit! how soon we become accustomed to any thing that at first seemed so wonderful, so almost impossible.  When I was first taken out, everything seemed so new, so strange, that is was almost painful; but now, although I have been out so few times, I am beginning to get used to it, and instead of being filled with a feeling of such wild delight and wonder, I feel that there is a sort of sweet reality about it, that give me a dear and earnest pleasure.  Oh, my sisters, if I never am any better than I am now, aught I not to be thankful?
Friday 16. - Forgive me, Jennie, for letting three days pass
### page 19
without writing in my journal.  I did not forget you or my journal either, indeed i think if anything i thought of you both oftener than I should if I had written; but it did you no good though, as you knew none of my thoughts.  One reason that I did not write is that I did not have time; (that sounds rather strange for me, does it not?) in the morning at ten o'clock, instead of a sponge bath, I take a pack; I get through about eleven, and then i only have about time enough to get rested before it is time to take my ride, that is, between one and two usualy and it takes me most all afternoon to get rested after that.  But I do not intend to give up keeping this journal though, by any means, for that would be depriving myself of one of my sweetest pleasures. - Dear Jennie, how can I thank you enough for the very unexpected letter that I received from you last evening?  I had given up the hope of having a letter from you this week, and had made up my mind to get along as well as i could without it, when to my joy and surprise your very welcome letter came.  It came too at a time when it was greatly needed, for I had felt down spirited and almost discouraged, but that to you I feel quite cheerful again this morning.  I have been out every day since a week of yesterday, until yesterday it was so wet and rainy that i did not go; it looks as if it was going to be pleasant again
### page 20
today, and perhaps I shall be able to go out; if I do go. I guess i will go to the Gymnasium, I have not been there yet.  I was pleased when I read in your letter that you did not write because you feel like it, or because you had anything to say, but because you wanted to get another letter from me.  I meant to have told you when I wrote last that although I excused you from writing I did not intend to deny myself the pleasure of going on with mine.  I shall be glad when you get all through with your reviews; when you write next please tell me how you have succeeded, for I feel interested, even if I am not with you.  What is Mary's domestic work now, has she got through picking over dried apples?  I infered from a remark in your journal that you has something to do with the milk mann.  But nowhere is the reason that you have so much face ache, have you any diseased teeth?  as what is the matter; I feel uneasy about it; it must be a great drawback on all your duties as well as pleasures, to have the face ache so much.  Give my very best love to Mary and tell her when vacation comes I want her to be sure and write me a good long letter. - Mrs. Hill is intending to leave tomorrow; he health is a good deal better.  She says, give my love to Jennie and tell her that when i get to utica I will write her a letter.  She is to busy to write today.  I guess I shant go out today as it is quite cold.
With much love, S.
### page 21
Sarah's journal for Jennie

Sat. April 17 -- The lingering rays of a setting sun are shining through my window. A beautiful time for thought and for writing to absent friends; I love to think of you, my sisters, at this calm twilight hour, just as day begins to shade into evening. In this sacred hour, when day and night mingle together in silent beauty, shall not our spirits also direct and spend this prescious time in sweet thoughts of one another: Oh, I love to think that it may be so, it makes me feel happy to think that my dear, but absent sisters are thinking and perhaps even talking of me at this very moment!  -- Monday Apr. 19 -- Jennie dear, pardon me if I do not write this very plain for I am a little exited as the most that I have written with ink in a long, time. Oh, sister, ought I not to be very happy? I am not sitting up by a table or stand, as you do when you write, but I am sitting up in bed supported by pillows. Oh, girls I do wish you could be with me for one little hour this afternoon. I have so many things I want to say, to you that I cannot write, so many questions to ask you and so much I want to tell you. But I must do the best I can by writing, and wait frantically a few months and then, I hope, we shall all be together once more, Dr. said today that he hopes I will be able to go home? about? the first of June, which will be in about six weeks.
### page 22
Can it be possible that I shall be well enough to go home safely as soon as that? I will try to hope so although I can't believe it yet. Perhaps you noticed that I closed my journal on Sat. rather abruptly: I was quite tired, and the Dr. came in and told me not to write any more that evening, so of course, like an obedient patient, I left it until today. I meant to have told you some of the many things I had been doing that day; but I suppose it will do to tell you now. Well, in the morning I took a sits bath, don't you think I am getting to be quite like other patients? it made me very tired indeed, but today I took another and stood it much better, I think it will be a great benefit to me in strengthening my back if I am able to take it; as I now hope I shall be. In the afternoon I went out to the Gymnasium. Oh, I had a grand good time. I saw so many new things that I was almost bewildered. I staid out there quite a good while, and watched the girls rolling balls and swinging and climbing till I got quite excited and tired. I have not been out yet today, but I expect to before night. It has been rather cloudy today so far, and it has rained a little, but it seems to be growing pleasanter now. Haven't I done pretty well to write so much with ink? I guess I had better stop now and rest. -- 
Tuesday 20. -- It is dull and cloudy without but it seems pleasant and happy in my little room; I suppose one reason is that I feel so well myself. How strong it is that one's thoughts and feelings should have such an effect; at least to our minds, in every thing around us: If we are happy everything around us seems pleasant and
### page 23
cheerful, but if we feel discontented and unhappy, how changed the scene: although we may be in the same place that before seemed so changed, although the very same comforts surround us, all is now glowing and unclear; everything seems to add to already sad feelings. Then, I guess I had better stop this lingo? -- as I said before, I feel pretty well today. I think I shall be able to home before long, and that makes me so happy.  I shall feel bad, very bad to leave the good folks here, they have been so good so very kind to me. I know that I never can repay them. Oh, my sisters, I cannot tell you, you never can know, in this world, how very much they have done for me. You can join with me in feeling grateful to them, and I unclear that you be, and that you love them for my sake: but still you cannot know, as I do, how very deeply I am indebted to these dear friends for all they have done for me. -- I did not go out yesterday afternoon it was so unpleasant, but I went up to the parlour instead. The girls played on the piano for me; I had a very pleasant time. Today I have been out again; it was rather cold but I had a thick shawl on and a hood, so I guess it did not hurt me. Thursday 22.-- You need not think that I dispise my pencil just because I have commenced writing with ink, I do not give up old friends quite so easily; no, I believe I shall always love to write with a pencil, just because I have used one so much since I have been sick. -- You would not be surprised to hear I was tired if you knew all that I have done this afternoon
### page 24
I have been up in the nursery, or in Mrs. Kelloggs room, as they sometimes call it, and I staid up there I guess about two hours and a half; that is not all either, for I was threading needles most half of the time, don't you think I was smart. They were sewing on a carpet and I threaded needles for them, it was the next thing to sewing it myself, was it not? I did not go out today it rained so, indeed, it has been so unpleasant all this week that I have only been out once. When the weather becomes pleasant and settled I hope to gain faster. -- Oh, say Jennie, do you remember how you laughed at me for asking you to get me that silk braid to tie up my hair. Well I sent for another yard today, that that you got is so warm? and that it will hardly keep my hair up. I know it looked like looking ahead a good way to send for it then, my hair was so short, but it has grown so, it is quite decently long now. I can hardly see it is so dark, as you may think from these crooked lines. ______  Friday morning. -- Good morning, Jennie, how do you do this cold morning, and how is our dear Mary? Both feeling as well and as happy as I do, I hope. John left early this morning, about five o"clock. He came into my room to bid me good bye before I was hardly awake. It is quite early yet, about half past six. I wonder what you are doing at this hour mourning? because you cannot sleep longer perhaps?
### page 25
Sarah's journal.
June 14 Tuesday afternoon. -- This is indeed a summer day; bright, beautiful but very warm. Although the others seem to suffer from the heat I have so far kept pretty comfortable. It was delightfully cool and pleasant in my little room all morning, for the sun does not beat in until afternoon: it is some warmer now, but by keeping the blinds closed, and the window and door open I keep comfortably cool. But what are you doing, dear sisters? I wish I could just have a peek at you. I wish you could bring your books into my quiet little centrum? for an hour or two, so I could look at you while you study; but I rather think you wouldn't study a great deal if I had my way. Oh, have you had any strawberries yet: I had a few last Saturday: They were not very ripe or large but still they were strawberries and of course good. It seems so like home here; if there is anything new or interesting, it is brought, as it used to be there. to Sarah, as she is both of it (but you afraid they will spoil her?) The other day little Sophy brought me the first lily that blossomed in her bed. Dear creature: how happy she looked when she told me her
### page 26
lily was unclear and when one should blossom she was going to give it to me! Oh, how my heart thriled with emotion at this sweet token of childish affection. She did not forget it, but brought it to me early one morning, her face beaming with delight as she placed her simple, but to me precious, gift in my little bouquet. Oh, how many times my own dear ones have made me so happy by some such trifling but touching gifts, of pure love! Wed. 16. -- Both yesterday and today have been exceedingly warm, and but for the kind, refreshing wind condescending to fan us, I hardly now what we should do. -- There, Sammy has just brought me a letter from South Hadley; who can it be from? I guess I will open it and see, don't you think I had better. Dear sisters, I wish you were here with me. It is so pleasant, so delightful here! I am sitting out doors, on the plank walk that runs along by the living room windows; do you remember about it? It is almost sunset. The beauty of this hour is past description. Not a cloud can I see; all is calm, clear and beautiful. -- The sun is down, and I am once more on my bed in the little room often where you last saw me; and where, my dear sisters, you must think of me as being, I suppose. -- Dear Jennie the letter was indeed from you. Perhaps you think from what I have said
### page 27
that I received it carelessly; but Oh, Jennie! I wish you knew how much good it did me. I am so glad we have commenced keeping these journals, it gives me so much pleasure, both in reading yours and in writing my own. I hope it does you one half as much good as it does me. It is almost too dark for me to see. Good night. 
Thurs. 17. -- I received a dear, good letter from Anna today. I suppose it was much more welcome on account of my having been long and anxiously expecting a letter from home. I had been so foolish as to be quite uneasy about them as it had been about two weeks since I had had a letter from them before. They usually write at least once a week, and I hardly knew what to think of their long delay. But Anna's letter quieted my sad surmisings as it told the glad tiddings that they were all well and happy. I do not think she said so, in so many words, but the tone of her letter implied as much, as I guess you would think if you read it. -- It has been quite cool today, to what it was yesterday and in consequence, I suppose, I feel better. We have some patients here now, five of which I believe I have never mentioned to you. But now while I think of it, Miss Roger (who is one of the other two and I am the other,) the other day when I was
### page 28
up in her room told me to give her love to you, Jennie, when I write again. It seems as if she said something else but I do not know what. I took a foot-bath last night; the first one I have taken since I have been here! An important era in my history, don't you think so? It is about time now to take them, 7 1/2, and I guess I will leave you. 
Friday, June 18. -- I feel quite smart today. Julia has been reading to me this afternoon in a book entitled, Philip Randolph; an Indian story. It is very interesting; in some places painfully so, as it brings before the mind the dreadful suffering and trials of the early settlers. -- I changed my strain rather abruptly last evening when writing about the fine new patients. I was going to tell you who are. Mr. Whaley, who is a minister, and his wife, rather a young couple, are from Penn. I like Mrs. W. very much, the little I know of her. I have not become acquainted with him yet. Then there is a Mrs. Carpenter and Mrs. Wright and Jenny, or Jennie as we sometimes call her, Mrs. W's little daughter, quite an interesting child, about six years old; they are from Mich. Jennie has already become quite a little pet of mine. How glad I was to hear that you were so much better Mary; keep up good courage, sister, vacation is near; and then I hope you will have a good long time to recruit. Make Jennie take care of herself. I hope she will make herself sick by unclear exertions now? at the close of her school class? Good bye & be good girls -- Sarah. --
### page 29
A Mothers Longing. 
Where is my Charles, O, where is he; 
Child of my pride and my joy? 
Do new temptations and trials 
Darken the path of my boy? 
Often at twilight I listen 
Almost expecting to hear 
Sounding once more on the threshold 
Footsteps to memory so dear 
Where is my Thomas, my Lizzie 
Children so precious to me, 
Through the long days of their absence
O, how I yearn for the three! 
They have all crossed the wild ocean 
Left the old home for a new, 
Sadly I let them embrace me 
Whispering a tender adieu.
Now they are far from their mother,
Far from their dear father's grave, 
And there is dashing between us 
Many a pitiless wave.
### page 30
But though a thousand such oceans 
Ruthlessly hold us apart
Love with his strong arm shall hold them 
Close ever close to my heart. 
And I will follow my children 
On the swift pinions of prayer, 
Placing them, trusting them ever 
Under the dear Savior's care. 
He, the great Shepherd of Israel, 
Tenderly guardeth the sheep, 
And though all others forget us 
He will not slumber nor sleep.  
When I am weary with longing 
Thinking of them all the day 
When in the night I am sleeping
O, how it rests me to pray! 
Then I remember the promise
Of the bright mansions of love 
And I rest sweetly while pleading 
Oh, may we all meet above. 
S. A. E.