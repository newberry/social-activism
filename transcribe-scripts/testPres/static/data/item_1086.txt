
## 1086: Edwin Hatch diary [part 3], November 1855-January 1856
### page 1
Saturday, November 3d, 1855

A fine morning, unclearthe wind blowing down stream, is so strong that the boats will probably be obliged tounclearlay by

The day passes without any thing of interest occurring .  I was visited by Maj. Hamilton who is unclear of the fort, called "Fort Campbell"situated about 3/4 mile above Fort unclear it is the trading post of Campbell & Co. of St. Louis-
Some unclear unclear were at the fort today report their camp unclear from the mouth of the unclear (fifteen miles from here) to the unclearsecond? point below the fort-about 250 lodges in all
### page 2
Took a walk up to the "Butte" to look for the boats, they were not in sight but will probably arrive to day-Indians plenty about the Fort-mostly good. unclear another express arrived from Gov. Stevens, for medicine.  The Gov has left one unclear sock about forty miles from here-The expressunclear says he was robbed by the unclearBlack-?-The boats did not get here-we heard from them, will be here in the morning-
### page 3
Monday, November 5th, 1855
Beautiful morning. The boats came in sight about nine O'clock-were received by Mr. Dawson with a salute-the large boat was not able to get to the fort on account on the low water, she unloaded about one mile below here-We were quite busy all day receiving goods & unclear-The wind blew quite hard from the North West as usual.
### page 4
Tuesday, November 6th, 1855
Another fine morning. A man arrived from the unclear Yellow Stone?-fort Union last night with a letter dated Oct. unclear.  Sioux threaten to besiege Fort Clark-so we unclear are between two unclearfires?, the Sioux below and the unclearConbine? tribes west of us-And we are unclearquick? in the center  of the most warlike and heretofore the most hostile tribe on the continent, vis, the Blackfeet.  Busy all day uncleararranging? some presently which I shall give, to unclear 4 words who unclear 4 words the council-
### page 5
Wednesday, November 7th, 1855.
Pleasant morning-the slough below the Fort, was partly covered with ice, about the thickness of a dollar.  I made a present to two chiefs-"Two Elk" and "Generals Son"-I also received a present from a chief called Little White Calf, consisting of one horse and twenty one buffaloe unclearrobes? which I tried to explain to him that I did not want, I finally gave them to the trader (Mr. Dawson) and told the Indian to get his pay for them and that I also unclear unclear him a present in the morning-Two Indians from the Blackfeet arrived to-day, came to inform me that some chiefs were coming to see me.
### page 6
Thursday, November 8th, 1955.
Cloudy morning, somewhat cooler than yesterday- I made the chief, mentioned yesterday, a present of some goods and provisions-Fort full of unclear 2 words A train of carts arrived at the other fort, loaded with goods from the mouth of the Yellow Stone- The Blackfeet who sent the messenger yesterday arrived today, I gave them some provisions to cook and told them I would talk to them to-morrow-they are fine looking men-unclear the largest of the tribes, I think.
### page 7
Friday, November 9, 1855
A very pleasant morning.
I held a council with the Blackfeet, explained to them what had been done at the late treaty - They said they were well pleased, and would abide by the agreement.
I made them a present part of which they gave to the Gros Ventres with whom they had had some trouble - 
A man died at the other Fort today - he was cook for Gen? Stevens - was take sick after the Gen? left here and was sent back - I walked? over to see Mr. Piquoter? in the afternoon - 
Blackfeet moved off about sun set, probably fearing some fresh trouble with the Gros-Ventres -
### page 8
Saturday, November 10th, 1855
Cloudy morning, and appearance of snow - 
Commenced snowing about ten O'clock - fell to a? depth of three inches. - 
I had my room finished today and moved in - very comfortable - The Gros Ventres are finishing their trade and moving off - "Lame Bull?" visited the Fort - brought some fresh meat which was very acceptable. -
### page 9
Sunday, November 11th, 1855
Quite cold, ground white with snow, --
The day passes off quickly--
the view all 
about--A child died belonging to Ms. , the of the fort
### page 10
Monday, November 12th, 1855
Cold morning. Sun did not show much during the day although it was quite pleasant.
I made a present to the Indian women who have white husbands.
Geo. unclear child was buried to-day.
Everything quiet no indians about except a few loafing.
### page 11
Tuesday, November 13th, 1855
Cool morning. Snow still on the ground.
I sent to the other Fort to-day for the women, they came over a.m. I gave them some provisions, shall also give them some goods in a few days, it being their proportion of the presents distributed this fall.
The men are very busy to-day making preparations for a ball to come off tonight.
### page 12
Wednesday, November 14th, 1855
Cool morning, thermometer 17° above zero. The ball came off last night, was attended by all the beauty and fashion from both forts.
River full of morning ice.
### page 13
Thursday, November 15th, 1855
Very pleasant but quite cool. Thermometer 10° above zero.
Mr. Clark came to me for license to trade, had filed his application and bond at St. Louis and bought a certified to that effect. I granted him permission.
He brought me a letter from St Paul dated July 16th -- quite late news.
Large cakes of ice floating by a.m. river frozen out several feet from the other shore
### page 14
Friday, November 16th, 1855
Warm morning but very windy.
A ball came off at Fort Campbell? last night and was fully attended. We danced all night till broad daylight and came home this morning.
No ice morning in the river.
### page 15
last night about ? and this morning the ? is now two inches deep fine morning. cloudy,
This fort sent down ? to fort Nion?
They started this morning expect to be about fifty days making ?
### page 16
A very pleasant morning, 
[ships] fog --
[Two] visiter by "Slav [Nolu]"
a Gros Veutres chief, he brought some fresh meet,
I made him a present of some provisions
---
During the evening I hav another of those peculiar offers, [viv] A Pirgan Indian came into my room with his [nify] after remaining a while, I asked his business, he said he was ashamed to tell it, but nevertheless he did tell it, --- needless to say, his offer was declined
### page 17
The night, not very cold,
It commenced snowing again about 8 o'clock this morning but snow cleared up and we hav a very pleasant day -- Our life is very monotonous here at present. Mr. [Clasll] visited us, he is building on the Teton River about three miles from here -----
### page 18
A clear, pleasant morning, It was the coldest but the air very still consequently we did not feels it -- thermometer at 8 1/2 above 0.
The day was clear and pleasant.
### page 19
Beautiful morning, thermometer at 36 degrees above zero.
Dawson, Nillon and myself rode over to the Teton River and visited Mr. Clark, found him on the river about three miles from here, had a very pleasant ride - saw 3 deer - 
The day was warm and snow fast disappearing. We returned about four o'clock.
Mr. Dawson [stated] two indians to-day, to hunt for fresh meat.
### page 20
Clear and warm, ther[mometer] 26 degrees above 0 at sunrise.
Froze some last night,
river clear of ice.
I met with a great misfortune this morning,
dropped my water on the floor and broke something so that it is useless for the balance of the time remaining here.
[Lame] Bull was here again, traded a horse with Mr. Dawson.
### page 21
Mr. Clark visited us again today - he brought another letter for me, from St. Paul dated July 14th, While he had mislaid.
Gros Veutres Chief, Little White Calf, visited me today also.
Spent the day reading and conversing with Mr. Clark
### page 22
Unclear is certainly
not too be surpassed in
any climate
### page 23
SUNDAY, November 25th, 1855.
Pleasant as usual -
### page 24
[??] Westerly.---
Mr. Wilson went up to the horseguard, intends to remain one or two days and hunt chickens and rabbits.
### page 25
Tuesday, November 27th, 1855. Very pleasant indeed, [??} Westerly, it blew quite strong yesterday. Nothing doing, all quiet -- I employed myself upon a history of the Blackfeet nation.
### page 26
Wednesday, November 28th, 1855. 
Most beautiful weather, It reminds me of Minnesota, but I think is more pleasant than this even, considering the time of the year. Mr. Wilson came in today with some rabbits.
### page 27
Thursday, November 29th, 1855.  
Very pleasant. The wind blows continually from the west. Mr. Wilson again for the horseguard.[horsegrain]
### page 28
FRIDAY, November 30th, 1855.
Warm and pleasant, 
wind still blowing from the west. -- 
I listened this morning to an account of the ravages of the small pox among the North Indians from Mr {Munasae?}} , who was there at the time..--  
"Template:Spotted? Calf"  G. Ventres Chief visits Fort today,


                                                                    X
### page 29
SATURDAY, December 1st, 1855.
Very little cooler, Wind still strong from West.---- 
No news- nothing going on - very dull. Heard from Mr. Wilson - doing well.-
### page 30
SUNDAY, December 2nd, 1855.
No change in Weather. Was visited by Gros Ventres "Chief Daijle". --
Maj. Hamilton called on us.-- says he expects news from Fort Union in four or five days.
### page 31
A little cooler. Temperature 28 above at day light. 
Mr. Dawson sent some teams and men to the mountains to get out timber they will be absent about two weeks.
### page 32
Weather still the same - some ice Template:Forming in the river but no trouble in crossing.
Mr. Dawson and myself accompanied by Mr. Wilson and two men think of taking a trip up to High Wood River and the Mountains, starts Thursday.-
### page 33
Very firm morning ,from
shit from the rest

 Malting preperations 

for on voyage to morons
### page 34
THURSDAY, December 6th, 1855.
Appearance of bad weather, but soon cleared up --
Mr. Nilson, Dawson, & myself with two men (? and Shucette) left the fort, crossed the river and traveled over a smooth prairie, 25 miles, to The Little Mountain, We arrived early, found Mr. Dawson's men, getting out pine timber -- snow about four inches in the mountains, none on the Prairie, camped with the men. ---
### page 35
FRIDAY, December 7th, 1855.
Fine morning, We started early, taking Mr. Nevais and another man (a dutchman) with us -- struck over to the "High Noon" river, traveled down the river, (passing Mr. Ficotte cutting cotton wood to finish his fort), about five miles and stopped to dine - While the men were cooking, I caught 19 fine trout through a hole in the ice. The least [?] probably weigh 12 oz. --
After dinner we crossed the prairie to Belt Mountain Creek and camped -- travelled about fifteen miles
Passed several beaver dams on the "High Noon"
### page 36
SATURDAY, December 8th, 1855.
Beautiful morning - 
Left camp early, Mr. Revais shot our Deer (Blacktail) - We crossed "Box Elder" creek and camped early at the Falls of the Missouri --
There are several falls two within half mile of each other, highest of the two probably 45 feet. --
We narrowly escaped having a sad accident at night -
Shucetti went to the river and saw a flock of geese and was calling them, The dutch took Mr. Shucetti for a goose and run to us to shoot at it, as it was near dusk, and in our haste made the same mistake and fired two shots at him and missed him. --
### page 37
SUNDAY, December 9th, 1855
Another delightful morning quite a summer [?] day. -- started for home.
Camped on the "High Noon" low down.--
I lost my hook and could catch no trout --
### page 38
MONDAY, December 10th, 1855.
Fine morning but mild corn [?] blowing early, blew a gale the whole day but it was on our backs and rather assisted than retarded our progress -- We reached the Fort about one o'clock
Found some Gros Ventres and Blackfeet had arrived the former to trade and the latter to see the Agent. --
I held a Council with the Blackfeet and they said they were pleased with the Treaty --
### page 39
TUESDAY, December 11th, 1855.
Beautiful morning -- no wind -- I made the Blackfeet a small present and they left -- A war party. The first that had visited the fort arrives to-day --
Saw they had come to ask their Agent, what tribe to go to war upon --
I talked with them some time and think they will return home --
### page 40
WEDNESDAY, December 12th, 1855
Very pleasant, mild and no winds, -- "Little Dog" visited Fort today, a Pieplur Chief. --
The war party mentioned yesterday, divided and part went home, the balance to war,
### page 41
THURSDAY, December 13th, 1855.
No change in the weather -- Mr. Revais came in from the mountains -- no news. --
Maj. Hamilton called over -- says he expects to hear from four waggons that he sent to Fort Union for goods, soon. --
### page 42
FRIDAY, December 14th, 1855.
Very cloudy, strong north wind, weather mild --
A War Party of four blood indians passed to-day on their return with fifteen stolen horses. --
Big Snake a Piepan visited me to-day made a present of 35 buffalo tongues -- Mr Clark called over --
### page 43
SATURDAY, December 15th, 1855.
Weather unchanged, --
very pleasant --
### page 44
Pleasant morning. snowed about one inch last night
Little Robe, band of Piepan came in and camped between the two forts --
Sitting Squaw, visited the fort, reports much sickness in the Gros Ventres camp --
### page 45
Very pleasant but a little cooler -- Thermometer at 12 above 0 at nine O'clock.
Clouded over in the afternoon and commenced snowing, snow fell three inches during the evening. --
### page 46
TUESDAY, December 18th, 1855.
Pleasant but cold --
Our thermometer is a small one and is closed. -- It is a real bitter day -- expecting a band of Blackfeet in but they did not come. --
### page 47
WEDNESDAY, December 19th, 1855.
River closed last night, cold morning --
The North Blackfeet (13 lodges) arrived and camped inside the Fort. 
I talked to them and made them a present. --
### page 48
THURSDAY, December 20th, 1855.
Very cold. -- Men returned from the mountain early this morning, crossed the river on the ice.
I just froze in my room. --
### page 49
FRIDAY, December 21st, 1855.
Very cold
"Little Joey [?] Head and "Mountain Chief" two Piepan chiefs arrived to-day.--
Nothing new from below, -- Picotte & co. getting anxious about their waggons
### page 50
Saturday, December 22d, 1855
beingcoldmorning. = Mind blowing and snowy flying - terrible -
Wood very scarce, men gone to Teton for form loads - 
Made a present to some indians who were not at the treaty
### page 51
Clear cold morning, River is passable for teams.
### page 52
Monday, December 24th, 1855.
Very cold - 
"Bearded", [unclear] as the fort -
### page 53
Tuesday, December 25th, 1855
being cold - Clear air still - 
Christmas passed quickly, no unusual bustle - The men all had a holiday and a feast - Flour and Sugar. -
### page 54
still and very cold, Band of north ... ... to-day on a ... expedition to ... apart. I made them a present of goods and provisions.
### page 55
unclear the "oldest inhabitant" says I've never unclear equal. -- Dull and monotonous today for the first in a long time, no indians in the unclear -- Blackfeet lift? this morning --
### page 56
Very cold -- snowed a very little last evening but is clean this morning --
### page 57
Cold and windy -- Nothing doing wept? commened? baking for new? jeans? when I give a feast and ball to the whole country -- I have unclear at work.
### page 58
Very windy and snowing some unclear hardly distinguish the other fort on account of the snow flying "unclear and "Heavy unclear", a unclear visited unclear today. -- I went out and on my return found three squaw, What men's unclear in unclear, come to kiss me -- unclear
### page 59
This is the last day of the year and consequently the dancing and feasting commenced to-night -- They danced all night --
### page 60
Dancing and feasting, continued after unclear some presents to the indians and Whites. Mr. Dawson, Mr. Milson and myself went over to Mr. Clarks, we got some B - y and got lost did not reach the fort until near morning. -- Mr. Clark presented me a very fine horse -- Snowed some during the day --
### page 61
Wednesday, January 2d 1865
Very pleasant morning still cold. -- Nothing transpired  Nothing of note
### page 62
Thursday, January 3d, 1856
Commenced snowing again this morning -- An indian arrived at the fort with the news that he had seen Assinboin signs when out hunting -- Snowed all day and evening -- Mr. Clark and family came out to spend the night -- See first page
### page 63
Caché No. 1 -- on left banks -- 100 yds below willow + 100 yds back from river, at end of point -- The willows have some timber about the center, a little back from the river, and timber above and below --1 pr Elk horns  Aug. 20th 1855   
Caché No 2 -- on left bank 30 yds from River, in willows toward first cottonwood tree left - high willows near foot of point, say 1 mile - Aug 21st Elk Horns -- on next point below is caché No. 1 -- next point below [?Haneys?] point is No. 2. One long double point and one other from unclear -- unclear is on upper and of unclear point