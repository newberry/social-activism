
## 1385: Le vrai caractere de Marie-Antoinette
### page 1
LE VRAI 
CARACTERE 
DE
MARIE -ANTOINETTE- : ' 

Mortel, qui juges tout, quelle est ton imprudence, 
lorsque ton jugement s’arréte à l'apparence!
Ce qui, partout ailleurs, n'est qu’une erreur 
légère: est , à la cour un vice dangereux , & 
meurtrier: par les diverses interprétations dont 
est susceptible, la moindre démarche, l’acton la 
plus indifférente: parceque la cour est le théatre 
perpétuel d’une guerre cruelle, sur lequel les 
grands professeurs d'amitié se distinguent surtout 
dans l’art de vous nuire, & de vous perdre. 
Auguste nom de ma souveraine: nom cher & 
sacré, objet de la venération & des tendres sollicitudes
de ceux à qui la réflexion & la connaissance
du cœur humain ont appris à ne pas
### page 2
se laisser entraîner par le torrent, à ne jamais 
juger sans approfondir : quelle déplorable preuve 
ne nous offre-tu pas de cette triste vérité ? Je 
voudrois pouvoir te le diffimuler, je voudrois 
pouvoir l'oublier moi-meme : je désirerois de 
tout mon cœur, que la vérité me dictât tout 
autre langage : mais il n'est que trop vrai, ton 
nom , chère & malheureuse Antoinette, inspire 
à bien du monde, un sentiment bien éloigné du 
respect que l'on doit à une grande Reine. 
Illustre rejetton d’une illustre famille, trop 
malheureuse fille de la plus grande des Reines: 
toi dont la brillante Jeunesse t'offroit à nos 
desirs, comme l'héritiere de toutes ses vertus, 
pourquoi des Jours d'orage, des Jours de ténébres 
font-ils venus obscurcir cette brillante
aurore. La Joyeuse renommée, cette déesse souvent 
bavarde, mais toujours crue, qui avec le 
nom chéri d’Antoinette, porta Jadis l'allegresse
& le bonheur dans le cœur de tous les François, 
auroit-elle pu penser alors, qu’elle feroit un 
Jour, forcée d’y déposer des sensations & des 
sentîments tout dilférents. Ici, chere Antoinette, 
déposons pour un moment, la majesté
royale: rapprochons-nous de l'humanité: convenons
de ses faiblesses, & voyons, sans rougir
### page 3
que nul n’en efst exempt: mais, surout, 
que la triste vue du passé, que le pénible sentiment 
du préfent nous dispose à sagement user
de l’avenir. 
La belle nature touJours amante du parfait, 
t’avoit abondamment pourvue de tout ce qu’il
faut pour l’être, grâces, vertus, talents, esprits, 
généreux sentiments: tout étoit ton partage: 
tout ce que ses enfants chéris peuvent obtenir 
d’elle, elle le fit pour toi: mais la nature ne peut 
pas tout faire: fes plus grandes faveurs, ses 
dons les plus précieux, ne font pas touJours 
suivis du don d’en bien user: d’ailleurs les circonstances 
ne sont aucunement soumises à sa 
benigne influence. Souvent les plus heureuses 
dispositions sont déprimées par le vice envieux 
qui les avoisine, souvent les plus salutaires 
rayons du soleil sont interceptés par des nuages
absorbants. Telles ont toujours été les belles, 
qualités de ton ame, vertueuse Antoinette: le ciel 
te donna un cœur excellent, une ame généreuse 
& sensible, incapable de résister à la moindre 
demande touJours : prête à tendre une main secourable
à l’infortune: mais., pour ton malheur,
& pour le plus grand malheur de tes sujets ta 
trop grande bonté a toujours été entourée,
### page 4
obscurcie par les nuages absorbants, des vapeurs 
méphitiques, qui , dans l'ordre moral cornme 
dans l'ordre physique, pervertissent, empoisonnent tout ce qui ôse en approcher. 
Trop senfible princesse, qui peut se faire une 
Juste idée de la bonté de ton coeur, & connoître 
les maux réels dont elle est cause, sans verser 
des larmes de sang sur ton malheureux sort, 
sans frémir de rage contre le trop indigne objet 
de ton aveugle tendresse : cette infâme Duchesse,
dont le rang élevé & la vaste fortune, 
feroient encore avec elle ensevelies dans la poussiere, 
sans les sentlments généreux de sa souveraine: 
vraie sang-sue alfamée, qui pour reconnaissance,
n'en pouvant plus rien obtenir, vomit les plus sales horreurs contre sa bienfaitrice. 
Et cette vile créature, cent fois plus méprisable encore, 
impudique effrontée, à qui le besoin, 
l’habitude, ont rendu le crime nécessaire. 
Infâme débauchée qui, sans honte, sans pudeur, 
ose se faire gloire des vices les plus honteux, 
mais qu’a~t-on a ménager, lorsqu’on s’est fait 
rayer du tableau de la société? aussi, sans la 
moindre difficulté , a-t-elle l'audace, a la honte 
de toute probabilité, je devrois dire, de toute 
possibilité, de reJetter sur toi ô malheureuse
### page 5
Antoinette, l'accusation d’un crime dont elle à 
été Jugée & reconnue coupable, personne n’ignore 
que ce ne fut pas là, la première opération 
de ce genre, il est à délsrer pour elle que ce 
soit la derniere. Lorsque sortant de sa fange 
primitive, & s’élevant, par dégrés sur l’aile du 
vice, elle osa porter son impudence Jusqu’au pied 
de ton trône : si ta pénétration avoit pu démêler 
sous la figure d’une femme, l’infidieux caradtère 
d’un serpent tortueux qui, par ses souplesses & 
ses contortions devoit un Jour t’infecter de son 
venin, tu aurois sans-doute étouffé dans ton 
cœur la douce voie de la commisération: tu l’aurois 
sans doute fermé aux prodigues sentiments
d’intérêt, qu'elle a depuis métamorphosés d’une 
maniere si infâme, que la plus sale proftituée 
rcugiroit de les expliquer, fussent-ils même 
dans l’empire de la possibilité. 
Mais cesse enfin de craindre, dans ton cœur 
agité qu’un instant de calme remplace un long 
et pénible orage. Les vains prestiiges d’une imagination 
exaltée par les derniers dégrés du 
désespoir, n’en imposent jamais à la saine 
raison. Elle ne s’attend pas â voir un accusé se 
condamner lui-même ; elle sait, au contraire, 
combien il est naturel de chercher à se laver de
### page 6
ses crîmes, elle- sait aussi, que le dernier dégré 
d'infamie, n’a rien de sacré dans ses moyens 
de défense; aussi ne s’arrête-t-elle jamais à une 
futile apparence, toujours trompeuse, comme 
font les esprits légers et irréfléchis, par malheur 
trop communs; mais, profonde ferutatrice des 
replis du cœur humain, elle sait démêler la vérité, 
dans le cahos d obfcurité où elle cherche à 
s’envelopper, et déduisant toujours les actions 
de l’homme de la notion des principes et de l'essence
du caractère, elle ne s’égare jamais dans 
son jugement. C’est toi, saine raison, profonde . 
réflexion, toi que n'aveugle jamais l’esprit de 
prévention; c’est toi que M. Ant. [Marie Antoinette] réclame 
aujourd’hui pour Juge. Trop embrouillés pour 
être clairement saisis, les faits ne peuveat t’offrir 
qu’un moyen incertain de décider; mais à
défaut de faits constants, rassemble les probabilités;
calcule les possibilités; étudie les caracteres, 
et prononce entre une grande reine & 
l’enfant , l’éleve et la déesse de l’adultere. Vois 
d’un côté cette puissante fille des Rois, soeur 
d’un Roi, femme d’un Roi puissant, née, nourrie,
élevée dans le vaste sein de l’abondance, 
ayant toujours eu à sa dispofition tout ce que 
l'imagination peut offrir aux désirs les plus
### page 7
étendus, les plus capricieux; n’ayant jamais fait
preuve de mauvais sentiments  n’ayant jamais 
péché que par excès d’une trop grande bonté : 
& toujours dupe de trop de générofsté et d’une 
trop grande confiance. D'un autre coté je vois 
une reconnue Valois, fruit adultérin d’une erreur 
douteuse, née dans la bassesse & le besoin, 
nourrie dans la crapule, au milieu du crime,
par les mains de la débauche. De son propre 
aveu, l’infàme qui devroit mourir de honte, 
mais dans son état, on ne rougit plus : à la faveur
du libertinage, et de maints tours d’escroquerie,
qu'elle passe sous silence, parvenue par 
degrés de la plus basse indigence au faîte des 
grandeurs et d'une naissante opulence, d’où ses 
manéges criminels enfin dévoilés l’ont précipité,
pour la réduire à son état primitif. Saine 
raison, bon sens le plus commun, examinez, 
conlidérez les deux portraits que la vérité présente 
å vos yeux. Lequel des deux originaux 
mêrite le soupçon d’un vol. Tranquillise-toi 
CHERE ANTOINETTE : ce que la justice a 
prononcé, l'équité le confirme : le crime a fait 
de vains efforts; il retombe accablé sous son 
propre poids. 
Vertueuse princesse, dont les torts innom- 
brables, pour être étrangers aux dispositions de 
ton coeur, n'en sont pas moins réels: reçois 
de notre tendresse un conseil salutaire. La 
pesonne des Rois, la majesté du trône, ne 
devroient jamais être souillées par rien d’impur.
Une trop funeste expérience, ne prouve
### page 8
que trop, combien dangereuse est la négligence, 
dans le choix de tout ce qui l’approche. 
Mais combien plus dangereux encore,
combien condamnable est l'aveuglement qui
accompagne le choix de nos conseillers, de
nos amis, des dépositaires de nos pensées,
de nos secrets. Il est bien peu d'êtres dignes
d'une confiance royale, & point, absolument
point qui doivent être les confidents d'une foiblesse.
Personne n'a le droit d'être vicieux:
mais le malheur de l'être, doit être toujours
suivi de la sage précaution de le cacher. Ainsi,
chere & auguste princesse, beaucoup de prudence, à l'avenir, le plus scrupuleuse attention, 
dans le choix des heureuses compagnes
de tes loisirs: et les rares vertus, découlant 
de leur source, dans toute leur pureté: penétreront
les ennemis mêmes de la plus grande
admiration, de n'avoir pas plutôt dispersé les
nuages qui, jusqu'à présent, en ont dénaturé
l'éclat.
Signé [following in italics] deditissimus Patriae Filius
De l'Imprimerie de [following in caps] MOMORO, premier 
Imprimeur de la liberté nationale, rue de
la Harpe. Numéro 160.