
## 891: Extracts from the journal of the Commissioners of the United States appointed to hold a treaty at Sandusky, circa 1790s
### page 1
Extracts from the Journal of the Commissioners of the United States appointed to hold a Treaty of Sandusky for the purpose of making peace with the Western Indians. 
[in pencil]  as to accepting the Ohio River as the Boundary
____________________________________________________
Extract from the answer of the Commissioners to a speech delivered by Cap. Brandt on the 7th of July 1793. _ 
“Niagara 8th,, July 1793.”
“Brothers—you told us that the people whom we saw here, were sent to represent the Indian Nations who own the Lands on the northern side of the Ohio, as their common property [2 words underline]; & who are all of one mind—one heart.” 
“Brothers—we now come to the second point: whether we are properly authorized to have & establish a new boundary line between your land & ours?” 
“Brothers. We answer explicitly that we have that authority. Where this line should run will be the great subject of discussion at the treaty between you & us: & if we sincerely hope & expect that it may then be fixed to the satisfaction of both parties._  Doubtless some concessions must be made one both sides. In all disputes & quarrels both parties usually take some wrong steps; so that it is only by mutual concessions that a true reconcilement can be effected.”_ 
“Brothers- we wish you to understand us clearly on this head; for we mean that all our proceedings should be marked with candour. We therefore repeat & say explicitly, that some concessions will be necessary on your part as well as on ours, in order to establish a just & permanent peace.”

         Extract from Capt. Brants answer to the foregoing-           Niagara 9th July 1793.

"Brothers - We think from your speech that there is a prospect of our coming together.  We who are the Nations of the Westward, are of one mind; & if we agree with you, as there is a prospect that we shall, it will be binding & lasting."
“Brothers—our prospects are the fairer because all are minds are one. You have not before spoken to us unitedly. Formerly because you did not speak to us unitedly, what was done was not binding. Now you have an opportunity

                                                                                                        of
### page 2
2
of speaking to us together; and we now take you by the hand to lead you to the place appointed for the meeting.”_ 
A white belt of fewer rows.
Cap. Brandt- in continuation._ 
“Brothers—one thing more we have to pay. Yesterday you expressed a wish to be informed of the names of the Nations & numbers of  Chiefs assembled at Miami. But as they were daily coming in, we cannot give you exact information. You will see for yourselves in a few days. When we left it the following Nations were there—viz:—Five Nations—Wyandots—Shawanoes—Delawares—Munsees—Miamis—Ottawas—Chippewas—Potawatamis—Mingoes—Cherokes—Nautikokees._
“The principal men of all these nations were there.” 
Copy of a written message delivered by a Deputation of the Indians. 

                                                              July 30th 1793.

“To the Commissioners of the U.S.
“Brothers—The Deputies we sent to you did not fully explain our meaning: we have therefore sent others to meet you once more, that you may fully understand the great question we have to ask of you; & to which we expect an explicit answer in writing._ 
“Brothers—You are sent here by the U.S. in order to make peace with us the Confederate Indians._ 
“Brothers—You know very well that the boundary line which was run between the white people & us, at the Treaty of Fort Stanwix was the river Ohio. 
“Brothers- If you seriously design to make a firm & lasting peace you will immediately remove all of your people from our side of that river.
“Brothers—We therefore ask you, are you fully authorized by the U.S. to continue & firmly fix on the Ohio river as the boundary line between your people & ours.”?” 
Done in General Council at the foot of the Miamis Rapids, 27th,, July 1703, In behalf of ourselves & the whole Confederacy, & agreed to in full Council._ 
(Signed/- Wyandots___Bear 
Delawares___Turtle
Shawanoes___Snake
Miamis___
Mingoes___Snipe
Potawatamis___Fish 
Ottawas_____
Counoys___
Chippewas_____
Minsees_____

                                                                           July
### page 3
3

July 31/st At the close of the afternoon the Commissioner delivered the following
answer to the Deputation, viz:

             The Speech of the Commissioners of the U.S. to the Deputies of the Con=

federated Indian Nations assembled at the rapids of the Miami river-

"Brothers- you yesterday addressed us, mentioning a former deputation who

met us at Niagara. At that meeting you said we did not come to a right
understanding: that your Deputies did not fully explain your meaning to us
nor we ours to them: that you desired we might rightly understand each
other: & therefore thought it best that what you had to say should be put into
writing.. Then handing us a paper you Said."Here is the meaning of our hearts."
"Brothers. That Paper is directed to the Commissioners of the U. S. & speaks to them
these words , viz: [Here is repeated their written address as transcribed in the preceeding pages]
"Brothers, The Deputies here present- We have now repeated the words contained
in the paper which you delivered to us, & those words have been interpreted to you
We presume the interpretation agrees with your idea of the contents of the paper.
It is expressed to be signed by the Wyandots, Delawares, Shawanoes, Miamis, Min=
goes, Pattawatamies, Ottawas, Connoys, Chippewas & Munsees in behalf of
themselves & the whole Confederacy & agreed to in ^a full Council.~

"Brothers- We are a little surprized at the Suggestion that in the conference at Niagara,

we did not come to a right understanding; & that your Deputies did not fully ex=
plain your meaning. Those Deputies appeared  to be men of good understanding
and when we saw them they were perfectly sober. In short  we never saw men, in
public council more attentive or behave with more propriety. We could not there=
fore suppose they could mistake your meaning or ours. Certainly we were Suffi=
ciently explicit for in plan terms we declared, "that in order to establish a just &
permanent peace, some concessions would be necessary on your part as well as on
### page 4
4
waited within Province, fixty day beyond the time appointed for opening the treat. ~ "Brother - We have now expressed our opinion  of the proper mode off fueling the difference - as between you & the U.S. - But as your Nation have desired answers to certain questions previous to our meeting, & we are dispored to act with fraukuefs & sincerity, we will give an explicit answer to the great question you have now proposed to us. But before we do this, we think it necessary to look back to foure former transactions; & we desire you patiently to hear us. ~
"Brothers - We do know very well, that at the treat of Fort Stanwix, 25 years age, the river Ohio was agreed on as the boundary line between you & the white people of the British Colonies. And we all know that about fewer years after that boundary was fixed, as quarrel broke out between your father, the King of G.B & the people of those Colonies, which are now the U.S. - This quarrel warended by the treat of peace made with the King about ten years ago, by which the Great Lake & the waters which unite them were by him declared to be the boundaries of the U.S. ~ 
"Brothers - Peace having been they made between the King of G. B & the U.S., if remained to make peace between them & the Indian Nation who had taken part with the King. For this purpose Commissioners were appointed, who lent mepages to all there Indian Nations, inviting them to come & make peace. The first treat was held about 9 years ago at Fort Stanwix, with the Six Nations; which has stood firm & inviolated to this day. The next treaty was made about ninety days after, at Fort mcMutosle with the Half King and the Wyandots baps. Pike & the other chiefs in behalf of the Wyandot, Elsware, Ottawa & Chippewa Nations. Afterwards treaties were made with divers Indian Nations, South of the Ohio River; & the next treaty ws made with Kakiapilathy, here present & other Shawanoe chiefs, in behalf of the Shawanoe nation, as the mouth of the Great miami which runes into the Ohio. - 
"Brother, - The commissioners who conducted there treaties in behalf of the U.S. sent the papers containing them to the great council of the States, who, supposing them satisfactory to the nations treated with, procceded to dispose of large tracts of the lands thereby ceded; & a great number of people removed from other parts of the U.S. & settled upon them - Also many families of your antient father, the French came over the great water and settled upon a part of some lands. 
"Brothers - After some time it appeared that a number of people in your nations were dissatisfied with the treaties of Fort McIntosh & Miami. Therefore the Great Council of the U.S. appointed Gov Hlair their commissioner with full powers for the purpose of removing all causes of controversy, regulating trade & settling boundaries between the Indian Nations in the Northern department of the U.S. - He accordingly sent messages inviting all the Nations concerned to enact him a Council Fire which he kindles at the falls of Muskingum. While he was waiting for them, some mischief happened at the place, & the fires was put out. So he kindled a Council fire at For Harwar, where near six hundred Indians of different Nations, attiuded the Six Nations then renewed & confirmed the treaty of Fort Stanwix; & the Wyandots & Deawares renewed & confirmed the treaty of For McIntosh. Some Ottawas, Chippawas, Patta=
### page 5
5
Watamies & Sacs were also parties to the treaty of Fort Harmar.-
"Brothers - All these treaties we have here with us.  We have also the Speeches of many Chiefs who attended them & who voluntarily declared their Satisfaction with the terms of those treaties.
"Brothers - After making all these treaties, & after hearing the Chiefs freely express their satisfaction with them, the U. S. expected to enjoy peace, & quietly to hold the lands ceded by them.  Accordingly large tracts have been sold & settled, as before mentioned.  And now Brothers, we answer explicitly, that for the reasons here stated to you, it is impossible to make the river Ohio the boundary between your people & the people of the U. S.-
"Brothers - You are men of understanding, & if you consider the customs of white people, the great expences which attend their settling in a new Country, the nature of their improvements in building houses & barns, & clearing & fencing their lands, how valuable the lands are thus rendered, & then how dear they are to them; you will see that is is now impracticable to remove our people from the northern side of the Ohio.  Your Brothers, the English, know the nature of white people, & they know, that under the circumstances which we have mentioned, the U. S. cannot make the Ohio the boundary between you & us.
"Brothers - You seem to consider all the lands in dispute on your side of the Ohio, as claimed by the U. S., but suffer us to remind you, that a large tract was sold by the Wyandot & Delaware nations to the State of Pennsylvania.  This tract is East of a line drawn from the mouth of Beaver Creek at the Ohio, due north to Lake Erie.  This line is the western boundary of Pennsylvania, as claimed under the charger given by the King of England to your antient friend William Penn.- Of this sale made by the Wyandot & Delaware Nations to the State of Pennsylvania we have never heard any complaint.
"Brothers - We are on this occasion obliged to make a long speech.  We again desire you to hear us patiently, the business is of the highest importance; & a great many words are necessary fully to explain it; for we desire you may perfectly understand us:  And there is no danger of your forgetting what we say, because we will give you our speech in writing.
"Brothers - We have explicitly declared to you, that we cannot now make the Ohio river the boundary between us.  This agrees with our speech to your Deputies at Niagara, "that "in order to establish a just & permanent peace, some concessions would be necessary on your "part as well as on ours."-
"Brothers - The concessions which we think necessary on your part are, that you yield up &  finally relinquish to the U. S. some of the Lands on your side of the river Ohio.  The U. S. wish to have confirmed all the lands ceded to them by the Treaty of Fort Harmar; & also a small tract of land at the Rapids of the Ohio, claimed by Gen. Clarke for the use of himself & his warriors:  And in consideration there of, the U. S. would give such a large sum in money or goods as was never given at one time, for any quantity of Indian lands, since the white people first set their feet on this Island.  And because those lands did every year furnish you with Skins & furs, with which you bought clothing & other necessaries, the U. S. will now furnish the like constant supplies; & therefore besides the great sum to be delivered at once, they will

                                                                                                                                every
### page 6
6
every year deliver you a large quantity of such goods as are best suited to the wants of yourselves, your women & Children.-
"Brothers - If all the lands before mentioned cannot be yielded up to the U. S., then we shall desire to treat & agree with you on a new boundary line; & for the quantity of land you relinquish to us within that new boundary line, we shall stipulate a generous compensation; not only for a largesum to be paid at once, but for a yearly rent for the benefit of yourselves & your Children forever.
"Brothers - Here you see one concession that we are willing to make on the part of the U. S. now listen to another, of a claim which has probably more disturbed your minds than any other whatever.
"Brothers - The Commissioners of the U. S. have formerly set up a claim to your whole Country Southward of the Great Lakes, as the property of the U. S.: grounding this claim on the treaty of peace with your father the King of G. B., who declared as we have before mentioned, the middle of those Lakes & the waters which unite them to be the boundaries of the U. S.-
"Brothers - We are determined that our whole conduct shall be marked with openness & sincerity.  We there fore frankly tell you, that we think those Commissioners put an erroneous construction on that part of our treaty with the King.  As he had not purchased the Country of you, of course he could not give it away.  He only relinquished to the U. S. his claim to it.  That claim was founded on a right acquired by treaty with other white nations, to exclude them from purchasing or settling in any part of your Country:  & it is this right which the King granted to the U. S. - Before that grant the King alone had a right to purchase of the Indian Nations any of the lands between the Great Lakes, the Ohio & the Mississipi, excepting the part within the charter boundary of Pennsylvania; & the King by the treaty of peace, having granted this right to the U. S., they alone have now the right of purchasing.  So that now, neither the King nor any of his people, have any right to interfere with the U. S. in respect to any pat of those lands.  All your Brothers the English, know this to be true: & it agrees with the declaration of Lord Dorchester to your Deputies two years ago at Quebec.-
"Brothers - We now concede this great point. We by the express authority of the President of the U. S. acknowledge the property or right of Soil, of the great Country above described to be in the Indian Nations, so long as they desire to occupy the same.  We only claim particular tracts in it, as before mentioned & the general right granted by the King, as above stated, & which is well known to the English and Americans, & called the right of pre-emption, or the right or purchasing of the Indian Nations, disposed to sell their lands, to the exclusion of of all other white people whatever.-
"Brothers - We have now opened our hearts to you.  We are happy in having an opportunity of doing it: tho' we should have been more happy to have done it in the full Council of your Nations.  We expect soon to have this satisfaction;  & that your next deputation will take us by the hand & lead us to the Treaty when we meet & converse with each other freely we may more easily remove any difficulties which may come in the way of peace."

                      At Capt. Elliots at the mouth of Detroit river- 31st July 1793.
                        (signed)                    B. L.           /
                                                         B. R.          /   Commissioners
                                                         T. P.           /     of the U. S.
                                                            ---------------------------------------------

August 1st 1793.
In Council, present as Yesterday-
The Wyandot Chief, Sa"-wagh-da-wunk, arose & spoke-        Simon Girtie interpreted.

                  "Brothers - We are all Brothers you see here now.-
                                                                                                                       Brothers,
### page 7
7
"Brothers - It is now three years since you have desired to speak with us.  We heard you yesterday understand you well - perfectly well - We have a few words to say to you. -
"Brothers - You mentioned the treaties of Fort Stanwix, Beaver Creek & other places.  Those treaties were not compleat.  There were but a few Chiefs who treated with you.  You have not bought our lands:  they belong to us, you tried to draw off some of us.-
"Brothers - Many years ago, we all know, that the Ohio was made the boundary.  It was settled by Sir William Johnson.  This side is ours.  We look upon it as our property.
"Brothers - You mentioned Gen. Washington:  He & you know you have houses & people on our land.  You say you cannot move them off, and we cannot give up our land.
"Brothers - We are sorry we cannot come to an agreement.  The time has been fixed long ago.
"Brothers - We dont say much.  There has been much mischief on both sides.  We came here upon peace, & thought you did the same.  We shall talk to our Head Warriors.  You may return whence you came, & tell Washington"-

                      The Council here breaking up, Capt. Elliot went to the Shawanoe Chief, Kakiapalathy, & told him the last part of the speech was wrong.  That Chief came back & said it was wrong.  Girtie said, he has interpreted truly what the Wyandot Chief spoke.  An explanation too place & Girtie added as follows-

"Brothers - Instead of going home, we wish you to remain here for an answer from us.  We have your speech in our hearts & shall consult our head warriors."-

       The Commissioners then said, that they would wait to hear again from their Council at the Rapids, but desired their answer might be given without delay.-

August 16th 1793.  At the mouth of Detroit River.

    In the afternoon of this day, the Commissioner received by the hands of two Wyandot runners, from the Indian Council at the rapids of the Miami, the following answer to their speech of 31st July.
                     

"To the Commissioners of the United States.
"Brothers - We have recd. your speech dated the 31st of last month, & it has been interpreted to all the different Nations:  We have been long in sending you an answer, because of the great importance of the Subject: But we now answer it fully;  having given it all the consideration in our power.-
"Brothers - You tell us that after you had made peace with the King, our father, about ten years ago, - "it remained to make peace between the U. S. & the Indian Nations who had taken part with the King.  For this purpose Commissioners were appointed, who sent messages to all those Indian Nations, inviting them to come & make peace" - And after reciting the periods at which you say treaties were held at Fort Stanwix, Fort McIntosh & Miami, all which treaties according to your own acknowledgement were for the sole purpose of making peace, you then say - "Brothers - The Commissioners who conducted these treaties in behalf of the U. S., sent the papers containing them to the General Council of the States, who supposing them satisfactory to the Nations treated with, proceeded to dispose of the lands thereby ceded."-
"Brothers - This is telling us plainly what we always understood to be the case, & it agrees with the declarations of those few, who attended these treaties, viz, that they went to meet your Commissioners to

                                                              make
### page 8
make peace, but thro' fear were obliged to sign any papers that was laid before them; & it has since appeared, that deeds of cession were signed by them instead of Treaties of peace. -
"Brothers - You then say, "After some time it appeared that a number of people in your Nations, were dissatisfied with the treaties of Fort McIntosh & Miami:  therefore the Council of the U. S. appointed Gov. St. Clair, their Commissioner, with full powers, for the purpose of removing all causes of controversy relating to trade & settling boundaries, between the Indian Nations in the Northern Departm: of the U. S.  He accordingly sent messages inviting all the Nations concerned, to meet him at a Coucil fire he kindled at the Falls of Muskingum.  While he was waiting for them some mischief happened at that place & the fire was put out.  So he kindled a Council fire at Fort Marmar, where near Six hundred Indians of different Nations attended.  The Six Nations then renewed & confirmed the Treaty of Fort Stanwix; & the Wyandots & Delawares renewed & confirmed the Treat of Fort McIntosh.  Some Ottawas, Chippewas, Pattawatamies & Sacs were also parties to the treaty of Fort Harmar.-
"Now Brothers, these are your words; & it is necessary for us to make a short reply to them.-
"Brothers - A General Council of all the Indian Confederacy was held, as you well know in the fall of the year 1788 at this place:  And that General Council was invited by your Commission Gov. St. Clair, to meet him for the purpose of holding a treaty with regard to the lands mentioned by you to have been ceded by the treaties of Fort Stanwix & Fort McIntosh.-
"Brothers - We are in possession of the Speeches & letters that passed on that occasion between those deputed by us, the confederated Indians & Gov. St. Clair, the Commissioner of the U. S.  These papers prove that your said Commissioner, in the beginning of the year 1789, after having been informed by the General Council of the preceeding fall, that no bargain or Sale of any part of these Indian lands, would be considered as valid or binding, unless agreed to by a General Council, nevertheless, persisted in collecting together a few Chiefs of two or three nations only, & with them held a Treaty for the Cession of an immense Country, in which they were no more interested, than as a branch of the General Confederacy, & who were in no manner authorized to make any grant or cession whatever.-
"Brothers - How then was it possible for you to expect to enjoy peace & quietly to hold these lands, when your Commissioner was informed long before he held the treaty of Fort Harmer, that the consent of a General Council was absolutely necessary to convey any part of these lands to the U. S.?   The part of these lands which the U. S. now wish us to relinquish, & which you say you are settled, have been sold by the U. S. since that time.-
"Brothers - You say, "The U. S. wish to have confirmed all the lands ceded to them by the Treaty of Fort Harmer, & also a small tract at the Rapids of the Ohio, claimed by General Clark, for the use of himself & his Warriors:  And in consideration thereof, the U. S. would give such a large sum in money or goods as was never given at one time for any quantity of Indian lands, since the white people first set their feet on this Island.  And because these lands did every year furnish you with skins & furs, with which you bought clothing & other necessaries; the U. S. will now furnish the like constant supplies.  And therefore, besides the great sum to be delivered at once, they will every year deliver you a large quantity of such goods as are best fitted to the wants of yourselves, your women and children."-
"Brothers - Money, to us, is of no value, & to most of us unknown:  And as no consideration whatever can induce us to sell the lands on which we get sustenance for our women & children; we hope we may be allowed to point out a mode by which your settlers may be easily removed & peace thereby obtained.-
"Brothers - We know that these settlers are poor, or they would never have ventured to live in

                                                                                                                                       a
### page 9
9
a Country which has been in continual trouble ever since they crossed the Ohio.  Divide therefore this large sum of money which you have offered to us, among these people: Give to each also, a proportion of what you say you would give us annually over & above this very large sum of money, and we are persuaded they would most readily accept of it in lieu of the lands you sold to them.  If you add also the great sums you must expend in raising & paying Armies, with a view to force us to yield you our Country, you will certainly have more than sufficient for the purposes of repaying these Settlers for all their labour & their improvements.
"Brothers - You have talked to us about concessions:  It appears strange that you should expect any from us, who have only been defending our just rights against your invasions.  We want peace.  Restore to us our Country & we shall be enemies no longer.
"Brothers - You make one concession to us by offering us your money, & another, by having agreed to do us justice, after having long & injuriously witheld it:  We mean in the acknowledgement you have now made, that the King of England never did, now ever had a right to give you our Country by the treaty of peace:  And you want to make this act of common justice a great part of your concessions; and seem to expect that because you have at last acknowledged our Independance, we should for such a favor surrender to you our Country.
"Brothers - You have talked also a great deal about pre-emption & your exclusive right to purchase Indian lands, as ceded to you by the King at the treaty of peace.
"Brothers - We never made any agreement with the King, nor with any other Nation, that we would give to either the exclusive right of purchasing our lands: And we declare to you that we consider ourselves free to make any bargain or cession of lands, whenever & to whomever we please.  If the white people as you say, made a treaty, that none of them but the King should purchase of us, & that he has given that right to the U. S., it is an affair which concerns you & him, & not us.  We have never parted with such a power.
"Brothers - At our General Council held at the Glaize last fall, we agreed to meet Commissioners from the U. S. for the purpose of restoring peace; provided they consented to acknowledge & confirm our boundary line to be the Ohio:  And we determined not to meet you until you gave us satisfaction on that point;- that is the reason we have never met.   We desire you to consider, Brothers, that our only demand, is the peaceable possession of a small part of our once great Country.  Look back & view the lands from whence we have been driven to this spot.  We can retreat no further; because the Country behind hardly affords food for its present inhabitants.  And we have therefore resolved to leave our bones in this small space to which we are now confined.
"Brothers - We shall be persuaded that you mean to do us justice, if you agree that the Ohio shall remain the boundary line between us:  If you will not consent thereto, our meeting will be altogether unnecessary:   This is the great point which we hoped would have been explained before you left your homes; as our message last fall was principally directed to obtain that information."-
Done in General Council, at the foot of the Miamis Rapids, the 13th day of August 1793-
Nations [underline]
Wyandots-                                 Chippewas-                      Mohickens-
Seven Nations of Canada          Serrikas of the Glaize        Messasaques-   
Delawares-                                Pattawatamies-                  Creeks-
Shawanoes-                              Counoys-                           Cherokees-
Miamis-                                     Munsces-
Ottawas-                                   Nantikokus-
### page 10
10
To the foregoing answer of the Indian Nations, the Commissioners immediately made the following reply, I delivered it to the two Wyandot runners who brought the answer.-

    "To the Chiefs & Warriors of the Indian Nations assembled at the foot of the Mismis Rapids."
    " Brothers, we have just recd your answer, dated the 13th inst. to our speech of the 31st of last month, which we delivered to your Deputies at this place.  You say it was interpreted to all your Nations; & we presume it was fully understood.  We therein explicitly declared to you, that it was now impossible to make the river Ohio the boundary between your lands & the lands of the U. S.  Your answer amounts to a declaration that you will agree to no other boundary than the Ohio.  The negociation is therefore at an end.  We sincerely regret that peace [underline] is not the result, but knowing the upright & liberal views of the U. S., which as far as you gave us an opportunity, we have explained to you, we trust that impartial judges will not attribute the continuance of the war to them."
    Done at Capt. Elliots, at the mouth of Detroit river, the 16 day of August 1793.
                       (signed)              Ben: Lincoln           /
                                                  Ben. Randolph       /    Commissioners
                                                  Tim Pickering         /    of the U. S.