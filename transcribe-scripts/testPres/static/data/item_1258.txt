
## 1258: Charles Metcalf appointment books [vol. 2], 1893-1895
### page 1
Engagements
### page 2
Copyright, 1874, by Cambridgeport Diary Co.
The
Standard
Diary
Astrological Sign Wheel--image
Trade
1894
Mark.
For the Trade.
### page 3
January, 1894.
Monday Jan 1
1:30 a.m. Nellie
S.A. Dr. M. till 8 or 30?
MON. 1. Office all 
day
Weather warm &
Fine roads good
TUES. 2.
Office
Recd. Metcalf Calender
from Anna
Eve SA 1.
Wed. 3.
Office
Cooler a.m
Wi & Mary start
East p.m.
Letter from home
Drawing from G.S. Jr.
### page 4
January, 1894.
THURS. 4. Office
Wrote letter to
Jos. Rich Eve.
FRI. 6.
Office
Eve Prof. Dyche
Lecture
SAT. 6.
To Haskell
Place House Early
Office cold
Clear Letter
APPOINTMENTS.
from Wi.
letter from
John
### page 5
January, 1894
Sun. 7.
Letter home
"  John
" Wi no ch. a.m.
ypsce & ch. Eve.
Mon. 8.
Office
fine weather
eve acct till 11.30
Tues 9.
Office
clear cold
Wed. 10.
Office
Letter from 
home
### page 6
January, 1894  
Thurs. 11.  L. from Wi from
Office A.M. 
P.M with Mr. 
Russell
to Harbison Farm
Fri.12.
Office
Sat.13.
Office
Appointments.
### page 7
January, 1894.
Sun.14.
Letter home
Cloudy not cold
P.M. Temperance meeting
Cong. Ch.  Eve N.P.S.C.E. &
Baptist Ch. Y.M.C.A.
Mon.15.
Office
2 letters from Wi with
each a letter enclosed
from John which I sent
on to Marion
Tues.16.
Office till
10.33 train to Norwood  Meet Surber Furber? &
go to Arnold farm
over night "
Wed.17.
Meet train at Norwood a.m.
Lawrence 10.02

office
Miss Manter hard

cold in office
Short time in a.m. only
### page 8
January, 1894.
THURS. 18  Eve Dapperton & Co.
office Miss Manter
short-time in P.M. only -
Weather still fine  warm
FRI. 19.
office
Miss Manter sick
not in office at all
Snow sleet rain
till 10.30 P.M. or so
SAT. 20
Cloudy all day
Eve clear office
Miss Manter down
in the P.M.
APPOINTMENTS.
Wrote to Wilder 
at Philadelphia
### page 9
JANUARY, 1894.
SUN. 21. Church a.m. 
Letter home 
Y.P.S.C.E. grow cold P.M. 
SB at Yewell P.M.
MON. 22.
office Miss 
Manter there a.m. & 
p.m.
8 [degrees] above this morning
TUES 23.
office 7 a.m.
frozen rain then snow
4:30 p.m. zero
6.00 pm zero -5
10.00 p.m. zero -10 & 
clearing off
WED. 24.
Morn. 14 below 0 [degrees]
office 
2 below 0 [degrees] at 
noon. 0 at 6 p.m.
House burnt down 
on [?] St in Early Eve
### page 10
JANUARY, 1894,
THURS. 25
Office zero in 
morning about 
15 above noon - 10 above 
at 10 p.m. Eve to S.S. Con
SB & DOC
FRI. 26. Rev. from DA&Co 
office Mr R 
to K.C. Eve 
Oritorical Contest
K.S.U. Letter from 
Home & from Wi
SAT. 27. 
office another letter 
from Home. cutting [?] 5 or 6 in ice on river
Letter from Jos Rich
Letter from Harl  
with letters from John & Eth.
A man killed here 
last night & thrown 
in River
Coroners verdict 
Suicide
### page 11
JANUARY, 1894
SUN. 28. Letter home 
Y.P.S.C.E.  snow in 
a.m. 
cloudy
MON. 29. office
Morning to Wi's
Carmean house 
fine day
TUES. 30. office
Fine Day Morning 
to Wis Ala. St. house
Eve. to Prof Blakes 1"
Lecture on Electricity
WED. 31.
office
Eve S.A. Phonagraph.
### page 12
FEBRUARY, 1894
THURS. 1.
Wi Home last night
office, Fine day
not thaw cutting ice 
on river - Night & Day
FRI. 2.
office 
less cold & some
what cloudy
SAT. 3.
office 
Eve Court-house  
Kent Club Dr. Cordley
Appointments.
### page 13
FEBRUARY, 1894
SUN. 4.
Letter home

&   " to Marion

with  "s from Ethel 
Ch. Eve.
MON. 5.
office 
Eve Darkey scrap 
in Lawrence 
one killed 1 wounded  
woman hurt
TUES. 6.
8.18 to K.C. Lv 
K.C. 10.15 change 
at Roper 4.15 Sedan 
7.45
WED. 7.
Team to Andrews
Craaig & Graham
Kilneen & Sadler
Cloudy sprinkle 
once in a while a 
very little
### page 14
February, 1894.
Thurs.8.
Sedana
Matthews Sadler
Team to McSale's
& siotase Sedana
over night
Mansion House
Fri.9.
Sedan to K.C.
K.C. to Lawrence
Lawrence 10.52 P.M.
High wind light snow
Sat.10.
office
Grow cold
11 P.M. S.A. Girl
Appointments
with Dr All Nt.
Come in HCK
J.E. Dodge & Eli
Wilson Scrap
### page 15
February, 1894.
Sun.11. Letter home
Ch. a.m.
Y.Paps Eve
Snow all Day
Cherokee Express
RR Rockdale
Mon.12.
Office
Snow 18 in. Deep
10 [degrees] above in morn
Tues.13.
Office
Snow storm was all over
Kansas Deepest for
Years  Mr. R. started
for Denver
Wed.14.
office  Letter from
Harl. with letter from
John Letter from 
Father to Wilder
Eve Sat 1.00
### page 16
February, 1894.
Thurs.15. Office
About zero this morn
Cold Day
Eve Wind in S
Fri.16. office
South wind all
day
Thaw
Sat.17.
Office
Thaw
Sleighing gone
Eve Kent Club
Appointments
Ed. Russell
Early Kansas 
History
### page 17
February, 1894.
Sun.18. Letter home
Ch a.m. p.m. & YNSE
Eve turn cold
S.B. & S.Y. P.M.
Mon.19 office 15 [degrees] above
5.48 p.m. wi to Butler
Co.
Tues.20.
office
cold
Letter from John
via Harl
Wed.21.
Office
Cold   Letter from
Home
### page 18
February, 1894.
Thurs.22.
office
cloudy
Cherokee Co. Book
Fri.23.
office
Wi home from
Butler & Greenwood
Sat.24.
office Cold  Fine day
Russell & Knudson
Settle / K. give his
Appointments
note for 700 due in

  more or less

2 yrs or before 8%
for stock & tobes & has
rent of farm for 94
Eve red Bonnett
### page 19
February, 1894.
Sun.25.
Letter home
Church 1-2-3
Fine day
S.B. & S.Y.
Mon. 26.
Office
Fine day warmer
Eve clear not cold
Tues.27.
office
Fine day
a.m. Drive to Willow
Springs, back at 2 o.c.
Roads bad
Wed.28.
office
till 5.25. train
to Ottawa & Queneno
Queneno over night
### page 20
March, 1894.
Thurs.1.  With Chas. Speck &
to Fisherland.
back 1 O.C. Dinner &
train to Topeka 2.05-
6.15 Topeka all
night
Fri.2.4 a.m. up take 4.25
train to Lawrence 5.20office  springlike
Dr. & S.B. Leiby Eve
Sat.3.
office  spring
Watch repaired Marks
1.50
Appointments
### page 21
March, 1894.
Sun.4. Letter home &
letter enclosing letter
from Nette ch 10.00
Mch 3"/ to Ethel
p.m. K.S.U. with wi &
Wis all Eve Rain
Mon.5.
Office letter
from home
P.M. colder
Din & Sup at
Wi's
Tues.6.
office  fine day
Frosty morning
Wi's all Day
Wed.7.
office
Letter from home
light rain in a.m.
pm clear
Wis to grub all
Day
### page 22
March, 1894.
Thurs.8.
office
Wis to Grub
Eve Ten Nights ina bar
room
Fri.9.
office
Fine weather
Wi's all day grub
Wi to K.C. P.M.
Sat.10
office
Wi's to grub
Appointments.
### page 23
March, 1894.
Letter home
Sun. 11.
Ch a.m
Eve Wisc
unclear wi. 
Pm walk
Haskel place
Mon. 12
Office
Grub Wis
Eve temperance
mass at Rink?
Tues. 13. 
Office ?
Wi to Lake 
After milan
unclear at wis.
eve S.A. Presbyterian
Wed. 14
Office
Letter from home
Horse from Ike
unclear wis
### page 24
March, 1894.
Thurs. 15.
Office Friday
Mis all day
Fri. 16. 
Office
Letter from Father
Letter from John Niathare 
Sat. 17.
Office warm about 85
Appointments
### page 25
March, 1894.
Sun. 18.
Light rain in
night cloudy day
Church a.m. Letter home
Wis to grub and in P.M.
& eve
Mon. 19.
Office. Wis all day to grub
Cloudy
Threaten rain
Tues. 20.
cloudy office
? at wis all
day
Wed. 21.
Office
rain windy
Eve Pby Ch meaning: Presbyterian Church
PM wi to Topeka
### page 26
March, 1894.
Thurs. 22.
Office \
Colder Windy
5.25 to Burlington

9.45

Fri. 23. 
Team to Drakes &
Seeleys Lv. Burlington

4.40

Ottawa 8.43
Ottawa all night
Sat. 24. 
Lv. Ottawa 8.47 L.
at 10.02   office
wis dinner & sup
Eve Republican Club
Appointments.
### page 27
Cold
Easter Sunday
March, 1894.
Sun. 25. Breakfast &
Dinner at Wis. Not 
there to Supper
Wi left on 8.18 train
for Ohio
Ch. a.m. p.m & ypsce Note: Young People's Society Christian Endeavor
Mon. 26.
Office Letter from home
Wis to Grub all day
gave Mary 12.00 for
3 wks Board - to Wed.?
unclear 28" cold
Tues. 27. 
Office
Cold Windy
Grub at wis
Wed. 28. 
Office 
Cold Windy
Grub at wis
### page 28
March, 1894.
Thurs. 29.
Office
Warmer
Grub at wis
Fri. 30.
Office
Letter from John &
Harl
windy
slightly warmer
Sat. 31.
Office
Nice day
cloudy a.m.
Appointments.
### page 29
April, 1894.
Sun. 1.
Church am. &
eve at ypsce
Breakfast & Dinneer at wis.
nice day
Letter home & to Jas Rich
Mon. 2.
wi home last night - office
fine weather
Grub all day at
Wis.
mail carrier at unclear
Tues. 3. watch at marks
office warm time
Break room
Dinner Lawrence house
Supper at Wis
J. Wakeman here
Wed. 4. Office
Letter from home
Wis to Grub all
day
### page 30
mail carrier & Dad & Dr &
April, 1894.
S.B. Dr told S.B. Eve
Thurs. 5.
Team to Doak
80 Leavenworth
Jay quash Frederick
Stevenson Toyne
Break & Supper Wis
Fri. 6.
office Break & Din
at wis P.M. with wi
to lake fishing no fish
Supper at lake
s.a. meeting in a.m. unclear
Sat. 7.
warm cloudy rain
in Eve office
Pittsburgh Kan. C.W. arr
Appointments.
### page 31
April, 1894.
Sun. 8. Ch a.m.
Letter home
wis Grub all day
& Eve hickory nuts
and sugar off rain
Mon. 9. 
office cloudy
a.m. clear off  P.M
Letter from home a.m.
Tues. 10.
office
till 520 to
K.C. 925 K.C
to Toronto
Wed. 11.
Toronto 4 a.m. around
with Gilson Team 
to Virgil vig Robinson
Shreeves Smith Wildoner
& Chandler. Virgil
about 3 P.M.  book? unclear
to leases Lv V. 6.20.
Madison 7.10 over night
### page 32
April, 1894.
Thurs. 12.
Team to Wasson
Katthany & Hamilton
Foot it to Powell
Farm & Utopia
& take train to Eureka
Fri. 13.
Eureka  Ks all 
day with P.M.M.
Lv Eurek 11.52 for K.C.
rain all night
Sat. 14.
K.C. 7.20
Lv KC 8.15
Lawrence 9.42
office
Appointments.
Wis as Din & Sup
### page 33
April, 1894.
Sun. 15. 
Letter home
Church a.m. warm
wis all day
Mon. 16.
warm
office
wis all day
to Grub
Light showers
Tues. 17.
office
to dentists - 9.30 to 12.30
and 5 to 6.20  Grub at wis
wi to lake P.M.
Wed. 18. 
Office
rain in night
cloudy day
wis all day
to grub
letter from home
### page 34
April, 1894.
Thurs. 19.
Office
Shower and blows
Letter from John via Harl
Break and Dinner at wis
9.00 to Mary 10 with 1.00 2 day
ago pays for Dinner today
Fri. 20.
Office
Colder
din and sup at wis
Sat.21.
office
Grub at wis all day
Appointments.
### page 35
April, 1894.
Sun. 22. Church
am wis all day
walk to Haskel place
and Babcocks P.M. with wi
Mon. 23.
Office
wis to grub all day
Tues. 24.
office till
10.33 Ottawa Garnett

Team to welda and crack

to Garnett
break at wis
Wed. 25.
Lv Garnett  1 a.m.
Cherry 4 am Team
to Murphy & Baker
Thompson Rd?
Lv Cherry Vale 3 PM
Columbus 4.45 Shacklee
to heal
### page 36
columbus
April, 1894.
Thurs. 26. start - 6 am
back 6 p.m. oyler
Childres Adams
Foley Garr Haney
back to Columbus
over night -
Fri. 27.
To Davis Watson & 

Shellhamer  & Lv.

Columbus 11.20 for
cherokee. Cogshall house
& lot - & Donally farm?
Pittsburgh 5.35 to Sat breach
to unclear after 10 P.M.
Sat. 28.
To Dorman Farm &
back for 8.55 train to Cherute  - Cherute to K.C.
home at 10.42
Appointments.
### page 37
April, 1894.
Sun. 29. Light rain this
a.m. not to ch. am
letter home
Wilders all PM
Dinner & Sup.
Mon. 30. 
Dr Esterley P.M.
Grub at ws. all day
Rain gold crown
Tues. 1. May.
Office
Grub at ws
Wed. 2.
Break and Supper
at wis
letter from home
office
wi to K.C.
Dinner in room
### page 38
May, 1894.
Thurs. 3. office
Wi to lake P.M.
Grub at wis all
day
Fri. 4.
office
grub at wis
rainy day
Sat. 5.
office
cloudy
warm
SB tells Dr. M
about Wis
2 first
Appointments.
### page 39
May, 1894.
Sun. 6.
Fine day
Ch. a.m. and p.m.
home Miss. Man.?
Letter home and to G.W. Rich
grub at wis all
day
Mon. 7.
office 
grub at wis
Tues. 8.
office
Grub wis
Wed. 9.
office
grub wis
Letter from home



May, 1894.
Sun. 6.
Fine day
Ch. a.m. and p.m.
home wiss. man.
Letter home and to G.W. Rick
grub at wis all
day
Mon. 7.
office 
grub at wis
Tues. 8.
office
Grub wis
Wed. 9.
office
grub wis
Letter from home
### page 40
May, 1894.
Thurs. 10.
office
grub wis
good rain last-
night
Fri. 11. 
with Severance
to Arnold farm
breakfast at wis
Dinner and Sup. lunch
sent by Mary
Sat. 12.
office
wis all day to grub
Appointments.
### page 41
May, 1894.
Sun. 13.
Ch a.m.
wis all day to grub
letter home 
letter from J S Rich
Mon. 14.
office
grub wis
Mary 5.00 on board
wi to lake P.M.
Tues. 15. Miss Yaw
office
grub wis
Wed. 16.
office
wi to lake
grub wis
### page 42
May, 1894.
Thurs. 17.
Office wi left- 9.42
for Topeka and to Elk Co.
with J.R. Severance
grub wis fine shower
in night
Fri. 18.
office cold
windy
grub wis
Sat. 19. office
frost in places
last night
grub wis
wi home 9.42 a.m.
Appointments.
### page 43
May, 1894.
Sun. 20.
office
Letter home and letter from home
Cool frost
grub wis
Mon. 21.
office
Severance here to
Dinner and left for
Chicago grub wis
Mary 10.00
Tues. 22.
Office
cool grub wis
Wi to Wilson and Neosho
Co's 5.20 P.M.
Wed. 23.
Office
427 Maple st. before
Break  cloudy
Grub wis Day
eve p.m.
### page 44
May, 1894.
Thurs. 24.
Office
light rain
Grub wis pm
after sup Lindley
Fri. 25. North Lawrence
Office
grub wis 
Except supper
Sat. 26.
walk to Swain
land office
grub wis except
breakfast
Appointments.
Hat 4.50
Laundry 65
Oranges 10
WC   5
### page 45
May, 1894.
Sun. 27.
Grub wis
letter home 
Ch a.m. and eve
Mon. 28.
Office
grub wis
eve threaten rain
Tues. 29.
rain in night
Wed. 29. Decoration day
fine rain in night
cloudy muddy
office a.m.
grub wis.
### page 46
May, 1894.
Thurs. 31.
office
grub at wis
Fri. 1. June. Eve
Mrs. Anna Shaw
break at wis
office till 10.33 woman sufferage
Lawrence to Chanute
E.E. Kelly not in town
Mcclellan & J.A. Cross
Sat. 2.
a.m. ride with McClellan
walk past J.A. Cross
house Chenute
12.01 train for Lawrence
office supper at wis
Appointments.
### page 47
S.B. & S Y. 5 to 6 P.M.
June, 1894.
Sun. 3.
Hot Ch a.m.
wis all day
for grub
sprinkle in eve
Mon. 4.
Office.
Break and Dinner Wis
5.20 to KC  K.C. to
Yates Centre
Tues. 5.
Yates Centre 3 a.m. bed
Stephenson and Hogue card
& S.H.H. Y.C. to Toronto
Gilson Team to Virgil
owen farm Virgil to
Madison overnight
Wed. 6.
Madison to Eureka and
on to Severy with C.E.M
Johnson farm east &
Gainer Hawley & Douglas Severy over
night.
### page 48
June, 1894.
Thurs. 7. 
See G.R. Jones and
Lv Severy 8.15 Eureka see
Cahill & Whipple P.M. with
C.E.M to Commings
Eureka over night
Fri. 8.
a.m. To Lesley farm
and the Mill & Cahill
P.M. Bocook & more
Eve Suffrage   Shaw
Sat. 9.
Lv eureka 9 a.m.
via Emporia great
rain thunder & lightning
Supper at wis.
Appointments.
### page 49
June, 1894.
Sun 10.
Letter home
Rain
Grub wis
Ch. a.m. and eve y.p.s.c.e.
Childrens day
Mon. 11.
Office
Grub wis
Tues. 12.
office
Break and dinner at wis
5.48 to Topeka
Pop Convention
Topeka over night
Wed. 13.
Lv. Topeka 7.30 a.m.
Eskridge 11 a.m.
Clark house and 1/4 sec.
Lv Eskridge 6.P.M.
Burlingame Supper Team to 
Osage City
### page 50
June, 1894.
Thurs. 14.
Team to Youngroot Dodge
& Lyndon M&R & C
Lv 820 Osage City
8.50
Fri. 15.
Lv O.C. 3 a.m. L: 5 am
Din and Sup at wis.
office hot
rain in eve 46 meals
with sup tonight 8.76
with 33 out = 8.43 - 
1.00 = 7.43
Sat. 16.
office rain last
night & a.m.
grub at wis
Sat  3
Sun 3
Mon 1
______

               Appointments.
      7
    19 1/21
   133
      3
 1.36

7.43
600
1.43
1.32
___
2.79
### page 51
June, 1894.
Sun. 17. Ch. a.m.
letter home
grub at wis
Ch eve
Mary 6.00 on board
Mon. 18.
Break at wis
7.43 Rainy day
6.
1.43
1.56  7 meal
2.79 due Mary
Din L house
Tues. 19.
6 am to K.C.
Lv K.C. 10.20  7t. & 9.?
to pleasanton Team
to Hudson's ducking
in Sugar Creek.
Pleasanton over night
Wed. 20. 
6 am team to Markley
& M. City 10 o.c.
Colony 1 p..m
Welda Team 2.30 
Garnett Team to Schoonover Freight
to Ottawa
### page 52
June, 1894. Din L House
Thurs. 21. Ottawa 12 m.
Soldiers day great crowd
McKinley not there
tho. Lawrence
office pd wi for
bal. of board 2.75
Fri. 22.
office hot
Din. L. House
Sat. 23.
office hot
light shower am
Din L House
Appointments.
### page 53
June, 1894.
Sun. 24.
rain in night
rainy day
letter to father
to Brownville
Maine ch. a.m. & p.m.
Mon. 25.
great rain   office
Hot
Tues. 26.
office
Hot
Wed. 27.
office
Hot
Eve shower
### page 54
June, 1894.
Thurs. 28. office
Letter from Harl from 
Elyria telling of death of Frank Rich by
lock jaw.
Hot
Fri. 29.
Office wi to 
K.C. Hot
Sat. 30. 
office
Hot
Appointments.
### page 55
July, 1894.
Sun. 1.
Office    Hot  Ch. a.m.
letter 43 w Ave?
wis in Eve
Mon. 2.
10.35 to Chenute 2.50 P.M.
Team to Walker 1.28 & 8
High water 26' June
Stayed 3 days
Tues. 3.
Lv Chanute 1.45 am
ottawa 4.25
Lv  "    8.47
Lawrence 10.15
office
Wed. 4. 
4th of July
office p.m. parade
Speech - Eve. Fire works
rain in night and
rainy a.m. cloudy p.m.
### page 56
July, 1894.
Thurs. 5. office
eve bicycle turn out
Republican Club
Burton spoke
Fri. 6.
office
Eve  s a  1.
Sat. 7.
Office
Appointments
### page 57
July, 1894.
Sun. 8.
Ch. letter
to 43 W. Ave.
wis in eve
Mon. 9.
office till 10.42
Council Grove via
Emporia  Team to
Pease farm back 9 p.m
Tues. 10.
Lv C.G 10.15 Emporia
till 3.10 Madison
GO Lorett
Wed. 11. 
am to Koenig 
80 & 160 Dinner
at Madison
pm to Mam's 
& back to Madison
P.O. Lorett
over night 
paid Liv'ry
### page 58
July, 1894.
Thurs. 12. Lv Madison 11.30
Eureka 1.30  Whipple Lewis
Cahill Moore &c
Zimmerly Freeman 
Cahill wants till Oct
will try to sell
Fri. 13. a.m. Hoover
Rizer Kirk Hart
will pay 25.00 next
week p.m. team to
R. Gibson Mattingley
Eldred Cooper Parker
Eureka 10 pm
Sat. 14.
Team to Whipple Snyder
Robinson Johnson Lew
Morris Cunningham
Warner Handford
Dinner at Moores on
Cunningham farm Eureka 7 p.m.
Appointments.
Lv. Eureka 11.52
for K.C.
### page 59
July, 1894.
Sun. 15.
K.C. 7.20 a.m.
Lawrence 9.42 Letter gram
father just home from east
letter home Ch. a.m. and
y.p.s.c.e  Eve
Mon. 16.
office till 10 oc
Team to Twin Mound
Lacefield farm
Clinton Blk Smith Stewart
Traxion Engine
home 9 p.m.
Tues. 17.
Office
Wed. 18.
office
letter from
Geo Rich
R - crossed out S.B. & -  8 to 10
### page 60
July, 1894.
Thurs. 19.
office wi away
to K.C. or elsewhere
Eve rain some
Fri. 20.
office
Sat. 21.
office a.m. p.m.
to Twin Mound
Lacefield home by Belvoir?
10.30
Appointments.
### page 61
July, 1894.
Sun. 22.
Ch letter
home warm
Mon. 23.
office
Tues. 24.
office hot
car load of fish
for Lake View Club
Wed. 25.
office
Hot
Hot winds
### page 62
July, 1894.
Thurs. 26.
office Hot
Hot winds
Fri. 27.
office
Geo. T. Zimerle here
p.m. McCoy note
paid 77.75
Eve 2 B's in Dr M's office with S.B.
Sat. 28.
office sprinkle
in a.m. early
cloudy very hot
eve horison scrape
Appointments.
### page 63
July, 1894.
Sun. 29. Pby note: Presbyterian Ch in
a.m. Park in
Eve and YPSCE &
Sr Letter home
talk with horison
Mon. 30.
office letter from home
morning walk Haskel
Inst. & Bullenae curney?
Tues. 31. morning walk
& side & R.R. Bridge
& canning factory
Dorley in Drs room with
A.W. early 5.30 no rain
great show for rain?
Wed. 1. August.
office hot
Eve W and C  S.A.
from Pittsburg
### page 64
August, 1894.
Thurs. 2.
office
Hot
Fri. 3.
office cooler
Eve Sap 1.00
Morning Walk S. ontario
w. to w line of Duncans via
K.S.U & to 1st it of Pinckney
Sat. 4.
8.15 to Fairmount via
Holiday McDonald
Home 5.48 p.m.
Appointments.
### page 65
August, 1894.
Sun. 5. Cool morning
fine day
letter home
no Ch am
Park meeting Eve
s & sa & Presby. ypsce
cool morning
warm day
office morn.
walk Haskell place
Tues. 7.
walk Bismark Grove
office
Nevison again 6 to 7 pm
Wed. 8. office Hot
letters from home
p.m. wi to Osage County
### page 66
August, 1894.
Thurs. 9. walk K.S.U.
&c office
Hot
SA girls for Sat night
Supper  1 00 
Fri. 10.
walk Adams
40 a
office Hot
Sat. 11.
office hot
Eve did not got to
Sa supper
Appointments.
### page 67
August, 1894.
Sun. 12.
Hot
Letter home
Mon. 13.
office Hot 1.00 degrees
Eve Sa 1.00    winds
Tues. 14. 
office
Hot
Wed. 15.
office
Hot
### page 68
Rev. of John to C.W.
August, 1894.
Thurs. 16.
office
walk tannery
letter from home
wi to K.C.
rain pm enough
to lay dust
Fri. 17
office Hot
Eve Sa 5.00
cool night w and ce
Ex Gov Robinson
died today 3.10 am
Sat. 18.
office hot
Appointments.
### page 69
August, 1894.
Sun. 19. Room all
day funeral
of Ex Gov Robinson.
Hot - letter home
& to Geo Rich
Mon. 20.
office 
Miss Manter went home sick
Tues. 21.
office
cooler pretty 
good rain last
night
Wed. 22.
office walk 2 min
letter from
home
### page 70
August, 1894.
Thurs. 23.
office walk Shepard
Fri. 24.
office light 
rain in night
Sat. 25.
office
cold cloudy fogy
morn.
warm bright
Appointments. Day
Nevison again left about 7 p.m
Elyria Republican 
tells of Geo Rich
getting hurt
squeezed between Cider Mill
and top of barn door
### page 71
August, 1894.
warm hot 
no ch
letter home
Eial? Miller Ice? house burnt 9 pm
Mon. 27. 
office
Miss Manter back
warm
Tues. 28.
office
Eve oil house fire
S.B. & _ in Dr. M's
till 10.30 or so
Wed. 29.
office
Nevison she away
little after 6 p.m.
fellow after wis byke
wi to KC on 8.12 pm
### page 72
August, 1894.
Thurs. 30.
office warm
She to Nevison
again wi away
Fri. 31.
walk Pinkney to Miss to 
Whitman & s. & e. to K.S.U.
& to Haskell orchard
met Wm B.Brownell
with white horse
office wi here
Sat. 1. September.
shave and hair cut
walk west side
Perkins &c 
office commence to rain
Appointments.
11.30 am shower
pretty good
rain
pay day
### page 73
September, 1894.
Sun. 2.
Letter home rain
Ch a.m. Cong. preach. by
Kan. State man John
worked under
Eve Baptist Ch   Doug. Co  Bible Society
Mon. 3.
office
Eve  more rain  good rain
Tues. 4.

office   warm

Wed. 5.

office    letter from home

Eve  Ohio St Col. Ch & A.P.A. at Rink

 Rain
### page 74
September, 1894.
Thurs. 6.
good rain
in night warm day
office wi to lake p.m.
Fri. 7.
office wi to
leavenworth k.s.q. ,
school of instruction 
warm
Sat. 8.
office warm
some rain last night
cloud day
Appointments.
### page 75
September, 1894.
Sun. 9.
Ch a.m 10.00
p.m S.B. & Grace
10 a.m. to 5 p.m.
letter home
Mon. 10.
office cooler
wind in N.  letter from
Geo. Rich about his getting
squeezed
Tues. 11.
office
cool
Lacefield
Wed. 12.
office
cool
letter from home
### page 76
September, 1894.
Thurs. 13.
office
warmer
Fri. 14.
office
commence to rain
about 3 p.m.
9.30 still raining
S.B. & -  8 till 930 P.M.
Sat. 15.
Dorsey spoke about S.B. being up in Dr's room last night
office warm fine
day S.B. & - in
Appointments.
Dr's room again in
Eve walked out and watched each go down
### page 77
September, 1894.
Sun. 16.
Ch. Letter
home rain Eve
Ch Eve.
Mon. 17.
office
wi home from 
FT Leavenworth
Merriman here
Tues. 18.
office till 3.38
P.M. to K.C. with Merriman supper at Coats
sleeper to Toronto. Team to Severy via Bennetts. Streets
Severy over night
Wed. 19.
Wednesday
### page 78
September, 1894.
Thurs. 20.
Team to Snyders & 
back to Severy for 
Dinner & 1.20 train to Cherryvale Merriman &
to Oswego  Baker farms &
Lv Cherryvale 11.20
Fri. 21.
Ottawa from 4.20 to 8.56
Lawrence 10.
office
fine rain in night
Sat. 22.
office
Appointments.
### page 79
September, 1894.
Sun. 23.
Letter to Geo. Rich
and home Ch am
From Germe
wis in Eve
Picture of F.P. Metcalf 
Mon. 24.
office
Bismark Fair
opened up
Tues. 25.
a.m.Team to A Stone's and JG Hunter
Jefferson Co.
p.m. office
Wed. 26. 
office
letter from
home
### page 80
September, 1894.
Thurs. 27.
office
Eve Rink
David Overmeyer
Fri. 28.
Morn. walk over to 
Bismark Fair  back 
8.30 office
Eve S.B. and Nellie and Dr.
Sat. 29.
office
cloudy a.m.
with light rain
clear p.m.
Cooler
Appointments.
### page 81
September, 1894.
Sun. 30.
Ch am and p.m.
and y.p.s.c.e.
letter home
Mon. 1.
office
cloudy rain
Eve rain
Tues. 2.
office
fine day
Wed. 3
To 8.40 Special
to see & hear McKinly
Lv on 10.42 for Eureka
via Emporia 
Eureka 5.30
### page 82
October, 1894.
Thurs. 4.
a.m. with C.E.M.
to the Shumachers
Hodgson Hunt
Knudson. bach 3 P.M
Rizer
Fri. 5.
with C.E. Moore
out State road past Eldred
farm to NW4 18-26-12
heal to dinner. Eureka 4
p.m.
Sat. 6.
a.m. with P.M.M. 
East to 231 a farm
To Mill & Greggs house
P.M. about town
Cahill Pd. June
Appointments.
Malting Pd Sept
Jenne says pd
Andrew ? Oct 15
J.F. Moore
H.H. Chandler &c
Hart
got left on Santa Fe
Lake neo. pae? to K.C
### page 83
October, 1894.
Sun. 7. K.C. 7.20
Lawrence 9.42
Ch a.m.
Letter home
wis. in Eve
Mon. 8.
office
Miss Manter sick
Tues. 9.
7 a.m. Team to 
Abi Darnolds
with F. Martell
H. and B. to C.F. Stanleys
Wed. 10.
Team to Knudsons
and Russells places in
Jefferson Co.
start 8.30 back 3.30
Dinner at Knudsons
Letter from Home and
John via Harl
### page 84
October, 1894.
Thurs. 11.
office
Fri. 12.
office
Eve Meeting
Peoples party
Sat. 13.
office
Eve populist
party meeting
Appointments.
### page 85
October, 1894.
Sun. 14.
Church am
letter home
eve Boys Brigade
1.00
Mon. 15.
office
fine day
Tues. 16.
office
fine day
warm
Wed. 17.
office
fine day
### page 86
October, 1894.
Thurs. 18.
office
warm rainey
Fr. 19.
office
Wi to K.C. or Mo.
warm
Sat. 20.
office
warm rainey PM and
Wi to Lake View P.M.
Appointments.
### page 87
October, 18o4.
Sun. 21.
Fine Warm
Ch am CC & Eve
Letter home
Mon. 22. 
office fine warm
Wi to Ottawa
K.N.G.  Endamjrmemd? 
Wood Sr.
O.C. here
Tues. 23.
office fine
walk Adams corner
warm
front
Two teeth filled
Esterley 4.50
Wed. 24.
office walk Spencer
warm
Fine
### page 88
October, 1894.
Thurs. 25. office
walk Hickory nut
Barkers across from
Eve Pby. Ch. Temperance
Magic Lantern & rink
Reb. political
Fri. 26.
office walk
K.U. Haskell India 40a
Orchard & Tracy
Eve Opera House
Sat. 27.
office Wi back
from K.N.A. encampment at Ottawa
Eve St John at Rink
Appointments.
### page 89
October, 1894.
Sun, 28.
Cool Cloudy Rain a.m.
S.B. and S.Y 4.30 to 5.30
Ch a.m.
Letter home
Eve at Wis
Mon. 29.
office cool cloudy
rain hail
eve Opera House Tracy
Tues. 30.
office
cloudy cold
Spit snow
eve M.E. Ch Note: Methodist Episcopal  Tracy
Wed. 31.
office
Frosty morning
Warmer
Eve warm
M.E. Ch   Tracy
### page 90
November, 1894.
Thurs. 1. office
cloudy cool
Wi to Topeka
P.M.
Rain P.M. 4.30 &
Eve
Fri. 2.
Office
clear day
Wi lake P.M.
Eve Tracy
Sat. 3.
Office fine day
Eve Tracy temperance
lecture
Letter from John
Appointments.
### page 91
November, 1894.
Sun. 4.
Fine day
Letter home
Ch a.m.
Eve tracy lecture Ben Hur
Mon. 5.
office fine day
Eve Rink
Miller and Blue
Tues. 6.
Election day
office
Eve Election Ret.
Wed. 7.
Office Election Republican
Letter from 
home
Kans. Ohio. New York.
### page 92
November, 1894.
Thurs. 8.
office
fine day
letter from John
via Harl
Fri. 9.
office 
colder
Letter from home
Sat. 10.
office
cold day
Wi. to K.C.
Eve Ratification
Appointments.
after that S.B. and Dr
Puss E.F.R. & J.Q.A.N.
till 12 m
lost 400 z? or 2?
### page 93
November, 1894.
Sun. 11.
cold morn
slightly warmer P.M.
clear cold night
Letter John
Ch am and home
Mon. 12. Wis in eve
office till 3.15 PM take 
U.P to K.C. Take
6.20 Wabash to Toledo
Tues. 13.
On cars all day
Toledo 4.15
Lv   "   5.10
Home  8.30
Wed. 14.
Cold
am Geo Riches 
with Harry and to town
with Geo for coal &c 
P.M. with Geo to walk
farm saw mill
lumber  Eve E.W.M's
### page 94
November, 1894.
Thurs. 15.
Over to new store
S.H. with father
then to Geo Richs
all day pick over 
apples
Fri. 16.
Beets at hill
Sat. 17.
Carrots at
hill lot
Appointments.
### page 95
November, 1894.
Sun. 18.
Home
E.W.M. here
to dinner
Mon. 19.
a.m. carrots
p.m. carrots at
home & turnips
at Hill Lot
Cider in cellar at Geo
Rich supper
Tues. 20.
Warmer but 
hard freeze last
night carrots
Wed. 23.
Letter from Wi
warm
finish carrots
Geo Rich here
fixing door
locks &c
### page 96
November, 1894.
Thurs. 22.
Finished turnips
a.m. p.m. Father Ham? Harry?.
& ? to Geo's with tools
to Sharpen them.
Hemlock trees unclear
Fri. 23.
cloudy a.m. fair p.m.
finished the husking
at wood lot a.m.
p.m. hauled Stalks 
home Letter to unclear
Sat. 24.
a.m. load unclear
down from Peo's
P.m. wood lot]
Father Harry ..
unclear
Appointments.
### page 97
November, 1894.
Sun. 25.
Home
Mon. 26.
a.m. to Geos with a 
invitation to Thanksgiving Dinner
P.M. with father to 
Oberlin horse and
buggy
Tues. 27.
Buck wood and
walk to wood lot
to find Geo Rich
P.M. Steele
Works & Sam Hollis
Wed. 28.
a.m. To Geos & over
to Phipps & to S.S. Cozzers
then to wood lot for load wood p.m. saw wood
at home
A & A & Fran C. over from
Oberlin
### page 98
November, 1894.
Thurs. 29.
a.m. to Geos. and Wood?
for wood
Thanksgiving Dinner
here aunt Anna and Lizzie
and all the Riches here
Fri. 30.
Rain most all night
a.m. home rainy day
saw wood &c P.M. stone quarry
& Geo Richs help him
fix fence and draw load wood
Sat. 1. December
Letter to WI
Rainy day split
wood a.m. fix
fence p.m.
Eve town for R.R.
time table
Appointments.
mother still sick
a bed Ralph sick
to
### page 99
December, 1894.
Sun. 2.
Still foggy cloudy
and rainy
Home all day
Eve anti saloon 
at Opera house
Mon. 3.
a.m. mollases bbl. &
privey &c  p.m. to Geo
Rich and around by 4th st.
Bridge to Phipps farm
help Geo husk and unload at Geos. then home for sup. and
Tues. 4.
start for Lawrence
12.44 a.m. Chicago
9.20 Eth: 80 Elm St
Edith Will Johnson
Bible institute
Kindergarden L U.C. 6 pm
Wed. 5.
K.C. 11.30 am
Bum stock do
horse md. ket?
Eve K.C.K. S.A. W & C
1.00 LW KC 9.20
Lawrence 10,42
### page 100
December, 1894.
Thurs. 6.
office
warm windy
letter to Father
Fri. 7.
office warm
Letter from father
a.m.
Telegram 2.30 mother
died 11 a.m. today
Wis to Supper 
Sat. 8.
office wired E.W.M. "when
is funeral had either or both
of us better come" ans.
Funeral Tuesday
Father says perhaps
better not come"
Appointments
### page 101
December, 1894.
Sun. 9. Letter home
Ch a.m.
to Wis in eve
light rain eve
Mon. 10.
office
letter from J.S. Rich
with D? for 25.00
cloudy day
eve light rain
Tues. 11.
office all day
K.C. 6.59 8.00 T.W.
Lv KC 9.20
Letter to Harry
Wed. 12.
Eureka 4.30 am
with C.E.M. to look 
at applications all
day fine day North
### page 102
December, 1894.
Thurs. 13.
with C.E. Moore to Schlotterbeeks  Bernards to Dinner
Karraker (old man) and DavidSons.
Fri. 14.
with C.E Moores to 
Reiters S.S. Cathington
Wileys and Severy
over night
Sat. 15.
Rain
Severy to Eureka
Eureka till 4.10
North on Freight to
Appointments.
Shipley Emporia
### page 103
Letter home
December, 1894.
Sun. 16. Lv Emporia 1 a.m.
Sleep through Lawrence
to Desoto walk back
16 mi. Wi came up and 
told me of Ralphs death
Ch. in eve
Mon. 17.
office
fine day
Tues. 18.
Office
6.49 p.m. to K.C. 9.10
to Thayer
Wed. 19.
Thayer 3.15 a.m.
Team to Heodesha 
Ford Brown Dean
Holbert
Sutherland
Lv 11.31on Mo. Pa.
### page 104
December 1894
Thurs. 20.
K.C. 7.15 a.m.
Lawrence 9.42
office
Letter from Father and Eth.
Fri. 21. Fine day
office till
5.10 to Ottawa
To Quenemo over night
Sat. 22.
Saddle horse to Shrader farm
2 mi E. Cloud &c. walk
to Pomona, ride to Ottawa
home 4.30 office till 7.15
Appointments.
### page 105
December, 1894.
Sun. 23. Ch a.m. and p.m.
Letter home
hot cold
Christmas Services
Mon. 24.
office Wi quail
hunting. Eve to Ch. S.S.
Christmas Entertainment
Tues. 25.
office a.m.
Christmas 
Eve to wis
Wed. 26.
office cooler 
Eve Opera House 
Oberlin Glee Club
letter from 
Father & Eth
### page 106
December, 1894.
Thurs. 27. 2 degrees above
Miss M. not here
Wi to K.C. P.M.
Letter to Harry with 
10.00 for block
Fri. 28.
To LeCompton 
9H2 to see Lawrence
land back 3.00
cold day
Sat. 29. 
office moderating 
Standard Dictionary came
Appointments.
Letter from John
Anna Harl
### page 107
December, 1894.
Sun. 30.
Ch a.m. and p.m.
Letter home
Mon. 31.
office
wi after
quail
### page 108
Memoranda.
Tuesday
Jan 1 1895
office all
Day
### page 109
Team 1016 O.P.B
Ken 1019 J. B
1006 RI Scott
upside down
S.H. Hognelangt
John S. Lanack renter of Foster land
1/3 crop  1/3 hay
60 or 65 a in cult.
O E. Thompson & Sons Ypsilanti Mich. price root cutter Circular
### page 110
Red leather back cover