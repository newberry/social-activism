
## 40: Mary Hartwell Catherwood ocean travel diary, 1891
### page 1
COMPLIMENTS
NIAGARA
FIRE
INSURANCE Co
NEW YORK
### page 2
Niagara Fire Insurance Co. of New York
[printed page decoration, image of Niagara Falls probably]
PETER NOTMAN, President.
THOS. F. GOODRICH, Vice-President.
GEO. C. HOWE, Secretary.
C. H. Post, Ass't Sec'y.
WEST POLLOCK, Sec'y City Dep't.
WESTERN DEPARTMENT, CHICAGO, ILLS.
I. S. BLACKWELDER, Manager. GEO. A. HOLLOWAY, Ass't Manager.
### page 3
1891 [Iaw?] Sure H.C.C.
Niagra Fire Insurance Co.,
of New York. 
Losses Paid,........$15,000,000.00.
? Sea,
First few days 
people stiff about 
their ? to deck 
chairs, softening
off & perfect complaisance.
The rain has ? running but drumming along side of ship in break made by the pour/prow.
### page 4
Lunches,
At Carlisle   .30
Sunday at [Leavington?] .24
Monday at Warwick .24
Tuesday at Stratford-on-Avon .48
### page 5
The astonishing aversion most passengers have from unclearseasickness, the joke they make unclear in others.
The sea is deep black towards evening, As it breaks away from the ship, between foam and water,, the pale green like the spirit of water.
### page 6
The swell of the sea, is unclear becoming unclear pale blue; and the decks almost standing straight; + [white] caps like flecks of foam on the little waves. 
One soaks salt. You can taste it in your nails, your hand. The air is scarcely ever too cold: it blows one
### page 7
full of life. 
The sharp line of the sea against the horizon. Except in the gale, when/where hillocks, villages, banks, all sorts of unclear rise.
unclear see a whale blow, like puff or jet of white smoke from pistol. Then his great sides heaved an instant.
### page 8
The ships far off like pale ghosts, or near, distinct and beautiful: Every sail + mast informed with light.
Old Sutternan sees birds flying, asks sailor what they are; he says "Mother Carey's chickens," "What?" "Mother Carey's chickens." Old man
### page 9
goes back + tells his wife the sailor says they are prairie chickens.
We reach Queens-town in night about 2 O'clock, unclear Lots of them sit up to see the coast. Many passengers fit off for lighter. The coast like a headland brown valley than
### page 10
green.
The large young blonde man, wearing cap. His trick of smiling, the under eyelids being heavy, making a peculiarly sweet shining look and smile,
### page 11
Lights on the Irish coast, the unclear, the Bull and the Calf.
The churning sound of the machinery - rapid. The day and night noise of water unclear along the sides of the boat.
### page 12
Liverpool,
The first unclear we see in the old world: a crimson blot among clouds in south west, over the harbor.
Imperial hotel, Bed with canopy open at top, + curtains. Small swinging Toilet glass. The slow life.
Divine comfort
### page 13
is at 10.30 evening, soup, fish, roast +c, + fruit. We get up from table about midnight.
Nothing remarkable, except, slowness in handling baggage.
### page 14
Liverpool to Glasgow, July, 15, 1891,
Wonderful effect of dark stony walls in unclear. Such unclear; such dappling unclear, such old stone houses, Enormous straw unclear at stations.
Interlining hedges, unclear in far hills
Where little streams, the unclear seems to
### page 15
unclear almost to the unclear. unclear, cattle, such farms, like heavenly visions of exhibition and gardens. 
Cumberland hills, sharp unclear  dots on the great smiling shoulders.
### page 16
Far Scotlands?
Young [Caster leading horse] + ? cart in [barnyard]. He lifts his [arm] with 2 graceful waves as train [from past]
The [engines] on the Caledonian R.R. has a helmet policeman-hat shaped thing on the place when an American carbell? ? be.
### page 17
1 man + 3 women [working] in fields, 
The [swelling, views] Scotch hills, with [clouds trailing] over.
### page 18
Afr.
The lovely evening sea. Twilight here lasts until after 10 o'clock at night. 
The "low green" by the beach where a band plays, and ? think all, ? is out walking or playing here, until ? up and ? ?, and ?
### page 19
it [swarming]. 
From [O'Shanty] ? in Stitch.St.
Meet bagpiper piping.
This afternoon the party drove to Burns birthplace + [Alloway] Road. Such a mark as the inside of that church! The rocky walls + ? bunch of term, the lamb
### page 20
inside second inclosure, the old ?, the pool at ? front plane ? where Burns was christened. [Hard finds] a bunch of ?.
The [bones] of ?, the old ? of Devon.
### page 21
In Ayr? they walk much to the middle of the street.
Carlisle.
We took a cab in haste thinking we have only 45 minutes, drive to cathedral, see finest old stained glass windows in England, dating from Richard II; [crushed]
### page 22
arches which have stood so ever since a fire 500 years ago. Then to [Rickerly's], up a narrow [pond] lane. St. Cuthbert's lane. Little shop into which [? to ?]. It has old sterling spoons, forks, old [?] and china &c. We make hasty purchases.
The British lunch [basket] is an institution
### page 23
institution: a ? looking hot sausage, a chicken and sandwich, and ?. You can [buy?] a large lunch for 35[c] and leave basket in train and it will be ? ? ?.
[In these] railway carriages are racks overhead for small parcels, and sometimes cords to hold men's hats by brim.
### page 24
[Having run sometimes] with terrific speed.
The British baby cab looks like a little casket. Almost springless, and [?]. Bare legged babies, often bare armed.
Leamington.
Harel & I go to Jephson Gardens, very beautiful with trees, flowers, pond of lilies +c.
### page 25
?Antwerp
Messrs. Von der Beck + [Marsily],
Red Star Line,
[Kaverner]strasse,
Antwerp.
### page 26
[pencil drawing of possibly a palm tree, maybe done while moving]
### page 27
File:Pencil drawing of a tree (?) on a specifically delineated area?
### page 28
File:Quick pencil drawing of a bunch of trees in a regular pattern, like an avenue. alternatively very avant-garde portraiture
### page 29
Pencil drawing of something organic-looking. maybe a shoreline w/ water reflection of trees? Or a hairbrush/ comb? Perhaps a knife with rope wrapped about the handle? Any ideas?
### page 30
Mathematical calculations that are difficult to transcribe in non-formatted text - maybe money because most have decimal points
### page 31
Wednesday         18d.
Thursday Jul. 29
[on strand, new place] = 11 p.
Friday                  1[Shilling]
Saturday             ---1.6
[France]
Sunday (1) --- $1.0[3]
### page 32
Lunches
Glasgow train = 25,d
[In] Glasgow = .18
St [Carlyle] = (30=)
[Leamington] 5.
[Warwick] = 2.
Stratford 2.3
Oxford .18
1 London Strand .19
2 " [Kensington] - 3.
3 " Strand - 1.
4 " Chelsea - 1.
5 " P. Robinson's 2.6
6
### page 33
[unclear pencil marks at side of page]
Sov. 4.84
Half sov 2.42
Crown 5 shill. 1.21
Half crown .60
Florin .48
Shilling .24
Sixpence .12
Fourpence .08
Threepence .06
Penny .02
Half penny .01
Farthing [0/2]
### page 34
[more sums and fractions upside down in pencil]
NIAGARA
Fire Insurance Co.,
OF NEW YORK.
---
ASSETS.
United States Bonds......$720,050 00
Corporation Bonds and Stocks... 647,640.00
Loans on Bond and Mortgage.....  46,050 00
Loans on Collateral .......... 110,912 50
Real Estate, unincumbered.......577,566.63
Other admitted Assets.........286,199.18
Interest due and accrued and Rents...18,527.97
Cash in Bank and Office.......215,534.57

                            $2,622,480.85

LIABILITIES
Cash Capital..........$500,000.00
Re-Insurance Reserve................1,420,778.55
Reserve for all other Liabilities.....266,190.87
Net Surplus................453,511.43

                            $2,622,480.85

Cash Assets, January 1st, 1880, $1,351,777.10
"           "            "         "   , 1881, 1,577,486.15

 "          "           "          "  , 1882,  1,735,563.20
  "         "          "           "  , 1883,  1,780,490.35
  "         "           "          "  , 1884,  1,874,034.97
  "         "           "           " , 1885,  1,881,597.39
  "         "             "         "  ,1886,  2,080,950.14
 "         "            "          '"  ,1887,  2,260,479.86
 "          "           "            " ,1888,  2,237,491.50
 "         "            "           "  ,1889,  2,360,135.37
  "        "             "          "  ,1890,  2,490,654.02
   "       "             "          "  ,1891,  2,622,480.85

(more handwritten sums on the bottom)
### page 35
[back cover]