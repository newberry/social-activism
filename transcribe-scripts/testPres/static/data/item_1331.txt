
## 1331: Richard Irving Dodge journal: Powder River Expedition #1, Oct. 31-Dec. 1, 1876
### page 1
Omaha Blfs Neb
Oct 31, 1876
On the 29th of Oct & when returning from a trip to Fort Fetterman, I met on the cars Col Royale of the 3d. Cavy. just from the front. He informed me that I would, in all probability be detailed to Comd. the Infantry in Crooks Winter expedition. Genl Merritt & others gave me the same information. I am executor of an estate but have not yet had the will proved. It was very necessary for others that the will be proved at once. It was barely possible that I could get through this
### page 2
some anxiety - the result - at 5 pm I recd. a telegram as follows -
Fort Laramie Oct 31, 1876 - 
Col R I Dodge
Omaha Bks -
If you wish to command the Infantry Batallions, in the Expedition, fifteen companies there will not be time to take the leave refused - (requested?) Can you go or not - answer - 
By Comd of Genl Crook
Sgd Nickerson ADC
A plain offer from which it is impossible for me to back down with credit -
### page 3
business in time to attend to my military duties. I telegraphed to Genl Crook the Dept Comdr. (now at Fort Laramie) as follows:
Omaha Neb.
Oct 31, 1876.
Genl Geo. Crook
Comd. Dept.
Fort Laramie
Business very important to others requires my presence in New York - If I am to go with expedition please give me leave for ten days immediately - If I dont go with expedition leave is not necessary at present. I waited in Omaha with some anxiety - the result -
### page 4
Omaha Bks Neb
Novr 1, 1876.
Went to town 11 am - & directly to Hdqrs. Order for me recd. at Hdqrs. I will get it tomorro - Goodale was going west to join his Co. I stopped him & take him up with me - Did a tremendous lot of work - saw Manduson & got a paper or petition declining the executorship of Ma P's estate - in favor of Julia - Bought lots of things - Boots, arctics?, gloves, Chamois shirts, drawers &c. Made out pay a/cs for Novr. & Decr. & left them with Omaha National Bank for collection Gave Byron Need the
### page 5
I therefore immediately sent telegram as follows.
Omaha Bks Neb
Oct 31, 1876
Capt A H Nickerson
A.D.C.
Telegram received - I accept the command -
signed R I Dodge Lt Col 2d. Inf
Paid visits until 9 pm then wrote to Father enclosing check for $100 for Sis Molly - to Cousin Kate Chambers enclosing $300 check for an investment in real estate & also to Tooty, giving her the news to date -

               Omaha Bks Neb
### page 6
Omaha Bks Neb
Nov 2d 76
Yesterday paid Comy Bils. My stable man - Avery Sheely?. The day before paid Joe & Laura. - Owe nothing Went to Town -- ready to go off but Bob W. has heard nothing of Pollock - & advised me to stay - Bought several things needed - Ordered a pair of Mex? leggins - & pd $9? for a skin (Llama) Wrote to Father & Mother & to Tooty at night & was so busy I could not visit the Ladies
### page 7
Policy of insurance on my furniture with directions to renew after 27th Novr & draw on Bank for amount. A terrible day. Snow slush etc. etc. came home wet through  Stuck my feet in cold water & put on dry clothing -
Took a nap after dinner -
At night wrote to Tooty enclosing the paper referred to above to the Putnams giving list of persons to whom I want my book sent with Auction  Complevity? to Potter (I.A.) & to Earl unclear expected to start tomorro but the cos are not ready. Will go the day after tomorro ---

                                Omaha Bks Neb
### page 8
Medicine Bow
Novr 4th
Arrived 12 - midnight - all in bed except Dr. Onsley --
Had pleasant enough journey on cars, but were behind hand all the time. Found very pleasant set of Californians on the cars. Got a good room at Trabings & went to bed 12.30 am
### page 9
Omaha Bks
Novr 3d 76
Up early - gave Joe chks for $50 - dated Nov. 30 & Dec 31 - Drew all my money out of Bank. Made pay accs for Nov. & Dec. & left them in Omaha Nat Bank to be drawn when due.
Bade good by to Bob to Kirk Luddington etc. & started well fixed for Med. Bow. Train late - Goodale with me - Got transp & sleeping births - from Kirk Start with bad cold
### page 10
Gave orders to start 11 am - could not load stuff on wagons bullied agent & got more. After great trouble got off at 12 m - some drunken men of course. Wagons badly loaded. However arrived at Little Med Bow 4.30 pm & camped - Got a wall tent from Carpenter & some mess supplies from unclear. Dr Hoff & wife, Dr Onsley & Goodale messed with me - Soldier Cork pretty good. Very cold, blustery & disagreeable day - with some snow. Ride the horses I brought down from Coates for the hunt - No stove, cold night. Slept tolerably however Distance 9 miles - no wood or grass
### page 11
Nov. 5th
Camp on Lit Med bow
Slept unclear- Offrs reported before breakfast. Find here Col C. G & I 23d Infy. Dr. Hoff Lt Pease 4th Infy & 2 Hospl Stewards all wanting transfer. - Qr Mr unclear informs me that he has only one wagon to Compy. -  Several row. Snow on ground - blowing like the D. l. Every prospect of a villainous trip & not half transfered - enough. Assumed Comd. appt Goodale adjt & Qr mr
### page 12
Distance 20 miles                        
near cabin -
Box Elder Canon
Novr. 7, 1876
Broke Camp 8 am. Slept well - no wind - but very cold towards morning. Lovely day. Comd started well & by 11.30 reached the last crossing of the little Med Bow, 10 miles. Left there at 12, & at 3.30 made camp on Box Elder half a mile below cabin. Road fine No snow in Canon - Men are however very soft & there were many stragglers - Will not try to go in in two days as intended. Bagged a fine antelope today & had part for supper as did all the Offs - Good Camp. Fair grass wood & water good & abundant
### page 13
2d Crossing
Little Med Bow
Novr 6 76 -
Pretty good breakfast & got off at 8 am. Rode ahead with Goodale & Dr Onsley & tried to get an antelope. Saw plenty at distance & got a long shot. 400 yds at a herd, but missed. Got 3 sage grouse with rifle. Saw several hundreds - & might have bagged many. Day cold & windy but clear. Marching hard on green troops & many struggle. Went with camp 2.30 pm, all in by 3.30. Weather moderated - pleasant evening - Distance 18 miles - No wood or grass
near cabin -
### page 14
always being able to put them on easily even when wet - Here they have gone back on me - I get them on with the greatest difficulty & they bind (without hurting) all day -- At Ft Fetterman on the Ct Martial, I could not get them on at all & had to sit on the Court in slippers -- All my leather boots & shoes are in the same fix - So I shall I fear be reduced to cloth? - The camp of last night was fairly good but contracted - wagons & men too close for comfort & the noises of the mules kept me awake -- Got off in good time a little before 8 oclk. Dr Hoff &
### page 15
Near Dug out
Box Elder Canon -
Novr 8, 1876
Nights warm considering altitude which must be over 8000 feet above level of sea. Slept tolerably but waked up very often & as often wished for morning. I think one needs less sleep in these very high altitudes. Another curious result of the rarified atmosphere is the swelling of my feet. I noticed this when I was here before. I am wearing an old pair of riding boots made for me in Feby /63 & which I have worn ever since to Arizona through the Black Hills & on all my hunts & rides since - never finding them uncomfortable but always being able to put them
### page 16
12 mile spring
Novr 9th 1876
Had a most luxurious bath last evening, a rare luxury when camping out & rigged myself with clean clothing from head to foot - The night was very cold towards am. - & I needed all the covers I had. Got off at 7.30 am to make as we supposed a long march. Was not long at all - I arrived at camping place at 11.30 & the comd came in in good order at about 1 pm.
Met Dewers & wife, O'Brien & wife & 2d Lt & wife same Compy. All going to join Co at Ft Sanders - also Ferris going to Med Bow on a police? & mission? Dr leaving for unclear who gave
### page 17
wife left us to make the trip to Ft Fetterman today. Weather perfectly beautiful & so warm that I fear it is a "weather breeder". Determined to march only to end of Canon as that is the last camping place unless I make more than 23 miles. I reached the Camping place at 11 am - The Comd an hour later. Officers & men pretty stiff & sore. Have a very good camp - plenty of wood & water - grass as good as can be found at this season - scenery fine very wild & picturesque - We leave the Box Elder tomorro & go on the divide between it & La Prele Distance today 10 miles --
### page 18
Ft Fetterman
Novr 16th 1876
Broke Camp at 7.30 am reached this point at 11 am
Command arrived an hour or two later. Reported at once to Genl Crook & was given my Comd - Met Townsend & Campbell. Am in doubt about the organization of my Comd. The  presence of a Field Offr & the necessity of allowing him his Comd. forces on me the necessity of playing Brig Genl -- I prefer to have direct Comd but cant help myself gave all the Infy to Townsend & all the Arty to Campbell
### page 19
with us -- Dewers says that we are to have many comforts that other expns. did not have - Am glad of it. Wrote to Tooty & Dad last night & sent the letters by the mail man whom we met today -- Had a very pleasant evening the weather permitting enjoyment of tents without fire - We have come down 2000 feet at least today I am to be a Brig. Genl on the expn. with several Batallions -- No game - Dr Hoff sent us some bread - most exceptable. Sent in my worst sick men by wagon Splendid day tho warm. Excellent marching - Distance 15 miles - good water wood & grass ---
### page 20
Fort Fetterman
Novr 11, 1876
Remained in camp all day to sign papers & get the comd properly set up. Very hard matter to get the maj Infy to do anything energetically. However there is no use in being troubled. Went up to Camp about 4 pm. Saw the Genl attended to business. Dined at the mess, played billiards & came home with Goodale about 12 pm - Very bad political news from the States - looks like a row --
### page 21
Have got started nicely. Went up in afternoon & saw the Genl Had a long talk with him after dinner. He is very liberal in his allowances & says laughingly that he wishes he had taken my advice last year as to his Indian Campaigns. Dined at mess. Saw lots of Offs some old acquaintances but most new -- played Billiards in evening & came to camp late with Campbell & arty - all feeling very well --
### page 22
& this Comd will be ready to march by tomorro noon - not that I expect or want to go then - The sun set clear & it is now 10 pm as cold as Christmas.  A days sun will clear off the snow - & we may expect to get away on Tuesday - Failed to get my tent today - & consequently am sitting out Goodale & the Dr  - No stove in my present tent - Coates promises to have one fixed by 10 am tomorro - Distributed the horses - Clark is doing very well as aagen - Wrote long letter to Tooty tonight - Goodale just finishing the 37th page of a letter to his wife & the Dr fast asleep
### page 23
Ft Fetterman
Novr 12, 76
Went up to post pretty early this morning - through a real plains snow storm. I expected to cross the river today but reported to the Genl that a move in the snow would result in sickness -- He delayed the march & we do not now know when we will get off - got boots socks Comy stores & some articles from store -- all are very polite & good natured but the post qrmr is an awfully slow cuss -- too handsome to be of much account -- Nearly all our wants are supplied today
### page 24
-chair -- Went up & reported all right to Genl -- invited to lunch -- Had pleasant time - paid Comy bill - Wash bill -- 2 drivers at mess - Settled with Suttler - got pair of buffalo shoes -- Got orders to leave tomorro - River half frozen & crossing execrable -- Returned to Camp 2 pm - Issued order of march - & got all ready for tomorrow - Rumor in camp that McKenzie is not to march tomorro - glad of it - Coates visited me at night - Wrote letters to Tooty Dad & Kat Chambliss -- Changed my under clothing & got ready for work ---
### page 25
Fort Fetterman
Novr 13, 76
Heavy fog & mist this am but the sun finally came out bright & clear. The cold last night was intense - 13° below zero at 8 am - Sent circular to Compy Comdrs to report wants all reported all ready -- Coates gave me a splendid tent & for the first time on this march I am comfortable in my own tent -- got a tressle for my bed, 4 camp stools, 2 tables - Coates gave me a leather backed
### page 26
wagon half loaded to the river with half the compy - These men mounted on it were carried across, unloaded the wagon which was then sent back empty for the other property & men - The Platte River is here only about 50 yards wide, but it is very swift, & generally too deep to ford. There are however several good fords near the post, but as the river was full of floating ice & the banks lined with ice several inches thick, I anticipated great difficulty in crossing  - Fury's (qr master) Wagon Camp was fortunately on the other side & the constant passage of wagons & teams had kept one force
### page 27
Sage Creek
10 miles from Fetterman
Nov 14, 76
Had a splendid nights rest in my new bed - which I have gotten after much tribulation. It is only a pair of low tressels & 3 boards but they keep me off the cold ground & make no end of difference in my comfort. The stove too is first rate. Reveille was only too early this morning - got up however & roused the lazy ones -- Dr Ousley got his ankle sprained last night & is quite a cripple.
The river is my terror. Made arrangements so that no soldier would have to wade, but this made my start necessarily very late. Sent each company
### page 28
same ford as McKenzie & were also ahead. Behind us came Egan & his Company of grays, the Hdqr guard -
The Pawnees under Maj. North came after - It was a beautiful & exhilarating sight. The long line of Cavalry Artillery Infantry Indians pack mules & wagons. The ground was covered with snow but the day was perfectly lovely. Too warm indeed for good marching - The snow was in many places too deep & wet for the Infantry to keep in - while the road was muddy & slippery from the passage of so many animals & wagons -

 However the Doboys got along
### page 29
quite open. To my very great gratification I found but little trouble beyond the delay & had every thing over by 10.30 am. Went up to Hdqrs - saw the General who gave me Carte blanche as to Camping - posted my letters - bade good by to Coates & returned to my Comd. Just at 11.30 all being ready I gave orders for the march.

 McKenzie had crossed at a ford some two miles above & having no fear of wetting the feet of his cavalry men he had got a good start of me
 The pack train crossed at
### page 30
the battalions. There are about 100 Pawnee Indians under Maj North about 60 Arrapahoes under Lieut -
about 100 Sioux under -
Our train consists of 300 wagons - 8 ambulances & 300 pack mules - We have 30 days subsistence & forage for nearly that time -
Each Company has 200 rounds of ammunition per man & the supply train carries at least 300 more - Egan with his Compy of 2d Cavy is Genl Crooks Hd qr Guard -
We will find 30 days rations & forage at Fort Reno in case we get short - Genl Crook came into Camp just after I got my tent
### page 31
excellently slow but sure & at 3.10 arrived at our camping place on sage creek. It is only a creek in a rainy season - being now a series of water holes containing a scant supply of poor water. McKenzie went to the head - & I took the holes lowest down - The Comd is now extended for three miles along the valley - & makes a brave show - I have never seen so large & well fitted a Comd in Indian warfare -  I do not think I have enumerated our forces heretofore - McKenzie has 13 companies of Cavalry - divided into three battalions. I have 4 Comps of Artillery & 11 Comps of Infy. divided into
### page 32
Camp on South Cheyenne
Novr. 15th, 1876
Broke Camp at 7.15 am. Soon passed the supply train but everything else being already ahead beat us into camp - The day was blustery & felt like snow, but was not uncomfortable - Road good & the snow nearly all evaporated by the warm wind of last night. 
Troops marched excellently, & got into camp 2.30 - The whole Comd is camped on this stream which is a miserable branch that can be stepped over any where
The pack mules have the camp I designed for my
### page 33
pitched, but soon left for his own camp about a quarter of a mile above me. He is in first rate spirits, & has a right to be - I am camped at what is called the 12 mile camp on Sage Creek. The distance is really but little over 8 miles from Ft Fetterman but all distances on the frontier are long when estimated by contractors who are paid by the 100 miles. When it comes to be measured the 85 miles from Fetterman to Reno will likely dwindle to 70 - Delightful day. Poor camp. We brought wood, as this creek offers nothing but poor water - no grass. Have done a splendid days work & we start under good auspices -
### page 34
Went up to Genl Crooks Camp in evening, hoping to see McKenzie whom I have not yet met on the campaign. He had been there but was gone - played 5 games of whist against Genl Crook. I having Fury as partner - he Clark - Beat them 3 of the 5. Very dark & threatening rain - gave orders for very early start tomorro - hoping to beat some of these people into camp -
All getting on splendidly - quite warm tonight but looks threatening.
### page 35
Comd, & I was forced to take a sandy & rather compacted position on the south side of the River. The road today has been monotonous in the extreme except that when we got on the high divide between the waters of the Platte & Cheyenne, we had a magnificent view of the Laramie Range of mountains the same that we came through in coming from Medicine Bow. 
Water at Browns Springs 3 miles from here. This is a good camp tho the grass is poor - plenty of wood & water - Distance about 18 miles - Called 22 -
### page 36
most luxurious surroundings 
considering the necessity for short allowance that I have ever seen taken to the field by a Genl Officer -- There is no doubt of his courage, energy & will - but I am loath to say I begin to believe he is a humbug - who hopes to make reputation by assuming qualities foreign to him. One thing is most certain. He is the very worst mannered man I have ever seen in his position Though his ill manners seem the result rather of ignorance than of deliberate will - I believe him to be warm hearted - but his estimate of a man will I think be discovered to be formed not on what that man can or will do for the service but what
### page 37
Camp on Wind Creek
Novr 16,1876
Reveille at 5.15 - broke camp 6.30

Moved to this early effort because Genl Crook gives his Indians & pack mules the very best camps if they are ahead. Early as I was however McKenzie got ahead on the road with his wagons. I (from the peculiarly mean position assigned me last night) having to make over a mile of deep sand before I struck the road. Fury came up & regulated us by putting Hdqr trains ahead - Genl Crook passes for a sybarite - who utterly unclear anything like luxury & even comfort - Yet he has the most luxurious surroundings
### page 38
& pack mules - These two last are his hobbies & he rides them all the time - all the Hd qr animals, the pack mules - the mules of the supply train are above us - while his Indians wash the entrails of the beaver in the stream from which his troops have to drink below --- The Cavy & Infy are nobodies - The Indians & pack mules have all the good places - He scarcely treats McKenzie & I decently but he will spend hours chatting pleasantly with an Indian or a dirty scout -- I don't blame Davenport of the Herald one bit. He stated what he saw & is cordially hated for it. I cant state what I see except in this private journal
### page 39
he can or will do for Crook. It speaks badly for mankind when I am obliged to admit that that kind are the successful men --
The day has been lovely. The slight shower of last night laid the dust & made marching good. Though McKenzie got his wagons first on the road, I was a long way ahead of him with my troops & he was forced into all kinds of bad ground with his command. I got to the Camping place an hour ahead of my Cmd - Went to Genl C. to see where I should camp & was turned off to hunt for myself - all the choice spots being appropriated by him - his Indians
### page 40
to call on him for any thing I want, & he would help me if he could - The news now is that Hayes is elected -- good news to all of us who wish to hold our position as army officers - Had several calls after my return from the Cavy Camp - & spent a pleasant evening - McKenzie & I agreed not to struggle for the road. He is to have it one day, I the next - 
Distance today 18 miles - Road bad - fair camp - No wood - but good water & some grass - Several streams today. Middle Cheyenne 8 miles. Humphryville 12 miles North Cheyenne 17 miles, Wind Creek 18 miles.
### page 41
My troops arrived in camp at 2 pm. The road is abominable up & down all the time with very steep pitches making it hard for men & animals. Maj Stanton arrived tonight with an immense mail - but nothing for me. Not a single letter from any of my loved ones - & I am sorely disappointed. It makes one feel all alone in the world to see every one around him devouring letters from their loved & their friends - when no letter comes to him -
Went over at night to see McKenzie - He was most gentlemanly & agreeable & I enjoyed my visit - He gave me two orderlies - & asked me
### page 42
as I have seen. My Ku Klux Cap is very comfortable except that the eyes are not protected & the little hard pellets of snow striking the eye balls were painful & tended to keep a man down cast - My men bore it bravely, though as the storm struck us after we broke camp many of them were without overcoats & scarcely any had face protection -- They plodded along wind dead ahead blowing hard. Cold as Christmas, without anything but their own stout hearts to protect them - & on a little let up of the storm some glorious fellow struck up "we are marching on" - at least a hundred voices joined in at once & though I was riding on the
### page 43
Camp at Buffalo Springs
Dry fork of Powder River
Novr 17, 1876
Reveille at 5 am & got Comd off at 6.10. It looked threatening & a little rain fell while we were getting our breakfast, but as it was my morning for starting early I pushed things - Before my wagons had started it commenced to snow I called for my wraps & furs & had almost to unload the wagons before they were found - By this time it was snowing furiously & when I at last mounted my horse it was with the greatest difficulty that I could make him face the gale -- For about an hour we had as lively a snow storm
### page 44
than this never fell to the lot of a Commander & if Crook dont do something with it he is a very unlucky, or incapable man - after about two hours march the storm passed & with a flurry or two - left in entirety though the weather turned so cold that our extra snow wraps were very comfortable -
The day finally cleared & after 12 m was as pleasant as could be desired - I reached camp 1 pm - The comd got in at 2.10 - after an 8 hour march. Called on stanton paymaster at Genl C.s Hdqrs - He is very busy making up
### page 45
side of the Column have no more voice than a crow I moreover have the dignity of acting Brigadier General to support - I could hardly refrain from striking in.
It is a glorious life this soldiering - However great the trials discomforts or suffering there is pluck endurance & patience to counterbalance all - We are going on a campaign fraught with not only the natural dangers from the enemy but with a thousand rumoured dangers from the elements - yet not a man flinches - not one but would rather suffer all than turn back - A better Comd
### page 46
a body of 8 Indians supposed to be hostiles were seen watching the column from a distance - 15 arrapahoes were sent after them, but I have not yet heard the result - We are now near the home of the enemy & may look out for them. The Comd is in excellent spirits - Every man sanguine off a successful issue - 21 miles today -
### page 47
pay rolls - so I did not disturb him except for a moment - McKenzie came over today but after & we had a long talk.
Four snake Indians found us tonight with information that the hostiles are moving to the other side of the Big Horn Mts - so we are likely to have a Mountain rather than a plains Campaign - to my very great satisfaction. 

 20 miles today - very little water - otherwise a good camp
### page 48
but very crooked. When we stopped to lunch we started a rabbit which took refuge in a hollow log. I twisted him out in North Carolina style with a forked stick - very much to the surprise of the officers with me who had never seen it done before, arrived at the river soon after 10 am. It is about 1/4 the size of the North Platte at Ft Fetterman, but instead of the clear sparkling stream I expected to find it is a mushy, discolored pea soup kind of a stream, almost as filthy as the Missouri itself. The water is slightly alkaline but better than that we have had lately. Many of McKenzies team were stuck in the ford & he came near losing some animals. Profiting by his experience
### page 49
Cantonment Reno;
Powder River
Nov. 18, 1876,
This was McKenzies day to be ahead so I have reveille at 6 oclk got out of camp at 7.10, after a very delightful nights rest. I am a good deal troubled each morning by my old Cough, the remains of the pneumonia of many years ago, but it is not so bad as many times in N.Y. As soon as I get the mucus from my lungs I am all right & feel excellently well. The road today was down the dry fork of the Powder River mostly along the bottom sometimes along the very bed. It is fairly good
### page 50
A singular accident happened to my horse just after we got in. The orderly (Cook) had taken the saddle off, & the horse laid down & commenced rolling, all at once he began to groan & appeared in great pain. On examination it was found that he had run a small stick in his eye, splitting the eye ball completely & entirely destroying it -
Distance today 14 miles. Went to post at night - saw Pollock & all the Offs. P. is the most conceited ass that ever existed. He thinks he can give advice to the Almighty & talks to Genl Crook & every body else as if he tolerated them - He is only a fair officer - scarcely above the average as a worker in
### page 51
all my wagons got over safely. Camped in bottom near the post. The Cantonment is garrisoned by 4 Comps of Infantry - Comd. by Capt Pollock 9th Infy. The troops are now in tents but have almost completed a full set of unclear log huts, which will keep them comfortably sheltered, though necessarily crowded & rather dirty - The Cantonment is about 3 miles up the River from Old Fort Reno. It is a horrible place to be confined at all winter, & I had rather take my chances of an Indian bullet, of freezing or starvation for six weeks, than to have to stay here till spring.
### page 52
[Cover]