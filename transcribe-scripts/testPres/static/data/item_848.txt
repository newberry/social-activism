
## 848: Alexander Culbertson letter to George H. Roberts, 1875
### page 1
Fort Belknap M. T
Sept 4" /75
Gen. George H Roberts
My Dear Son
I have the pleasure of rec your kind and much esteemed Letter - but owing to my absence did not rec until a few days since - and I need not tell you how overjoyed I was to hear from you and your dear family - The expressions of Kindness contained in your Letter more and more increased my anxiety to be once more and have that pleasure of being in your midst - which now at most could not be a very long time for notwithstanding I as yet do not to any great extent feel the infirmities of old age I am still aware? of being on the
### page 2
down hill of Life - another to Man I was so much pleased to hear of your fine Country Seat - and only not being there to help Dear Julia to make Guarden? - we have no Guardens? here - we plant plenty but the Grass Hoppers Reap I suppose you hear a great deal about - Indians - and their deprecations but most of rumors? of that kind is as a general falsehoods upon the Indians - they are here perfectly peaceable and quiet - they have greater reasons for complaint than we have
Well George I am still tied here with the two boys - who are both doing as well as I could expect - but not very anxious I should leave? them - this with other minor matters makes my Visit indefinant - but wont be long so -- Fanny is gon East some where I have been trying to get to Visit you
### page 3
but she as yet made no reply Siter Magge I suppose will soon be a young Lady - she must write to me
I have been expecting a Letter from my Dear Julia - I hope however she is still in good health
I hope you and Julia will wright often for I am so anxious to hear from you
Give a Kiss to all? with Kind and affectionate Remembrance to you and with Gods Blessing
Your Affectionate
Father
Alex Culbertson