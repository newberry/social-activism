
## 1138: Christina Olson World’s Columbian Exposition work papers, 1893
### page 1
We are, the undersigned Pierre Antonius & co. - first-party and Augusta Olsen - second party, agreed as follows

(1st) The second party Augusta Olsen promises to send her daughter Miss Christina Olsen to dance & help the actors in the performances which will be shown in the said Theater in midway plaisance and also agreed to come at 9 o'clock A.M. to join our actors in the performances which will be shown in the times appointed P.M & A.M. by the first party, but the first party agreed to set the second party free from service after nine o'clock in the evening. Upon this agreement the second party is obligated from thenceforth to begin in her practicing & rehearsing the imitation of the Turkish dancers & customs which are requested of her & to try her best imitate it as soon as possible that she may please the stage manager & promises to be obedient to do what she is requested for.
(2nd) The first party promises to give the second party a salary of $20 for the first week; then when the second party will be able to appear on the stage just to imitate the Turkish dance or part of it, the first party agreed to give $25 twenty-five dollars per week and promises to raise the salary as soon as her dance improves. 
Now after trying her for the first week, if she proved successful or has a hope of success in future she will continue in the work & her salary will be raised.
### page 2
as mentioned before; & if she proved unsuccessful then the first party is free to leave her of the work after giving her the salary of what she has worked with it's in the days past.
(3rd) If the first party approves the work of the second party & wishes to continue the contract to the close of the World's fair the second party is obliged to obey according to the conditions above mentioned.
For the acceptance of this agreement two copies are written & signed by both of us. 
Mrs Augusta Olsen
### page 3
AGREEMENT:
We, the undersigned, Pierre Antonius & Co. a party of the first part and Augusta Olsen a party of the second part, agreed according to the following conditions:
(1rst) The party of the second part agrees to send her daughter, Miss Christina Olsen, the dancer & actress, to help the actors in the performances which will be shown in the Turkish Theatre Midway Plaisance, world's fair; as also agreed to send the said Miss at 9 o'Clock AM to join our actors in the dances & performances which will be shown in the time appointed. AM, PM, by the party of the first part; but the party of the first part agrees to set the second party of the second part free from service after nine o'clock in the evening.

Upon this agreement the party of the second part became responsible from thenceforth to send her daughter to the said Theatre to begin her rehearsing & practising the imitation of the Turkish dances & customs which are requested of her & also to try her best in imitating it as soon as possible, that she may please the stage manager & promises to be obedient to do what she is requested for. 
(2nd) The party of the first part agrees to give the party of the second part a salary of $20 twenty dollars a week in return
### page 4
As the service of her daughter in Turkish Theater, Then when the said Miss Christina Oslon will be able to appear on the stage just to indicate? the Turkish dance or a kind of it in a recomendable manner - The party of the first part agreed to give the party of the second part $25 twenty five dollars per week + unclear promised to raise the salary as soon as the dance improves.
Now after trying the said miss Christina Olscon for the first week + proved successful in this work, she may continue in this work + her salary would be raised as mentioned before + if she proved the contrary then the  party of the first part will be free to leave her of the work after giving her the wages of the days worked.
(3rd) If the party of the first part approved the work of the the said miss Christina Olscon + wish to continue the contract with the party of the second part the last is unclear to obey according to the conditions above mentioned even to the close of the World's Fair.
My own hands, unclear signs witness this agreement of this the 27th of June 1893 unclear
### page 5
To whom it may concern we certify that miss Christina Olson has been engaged as a dancer with us in the Turkish Theater, mid way plaisance, World's Fair for a term of five months + has proved to be very clever + diligent in her service in the said theater.
She has studied the Turkish + unclear dance + practiced it in the said theater + proved after all to attain a recommendable degree of dance worthy of approval by all the Turks.
So wherever she may go or work we will believe that she will show such a faithfulness in service as a dancer + will dance the real Turkish dance as it is danced in Turkey This recommendation
### page 6
is written + signed by the managers of the Turkish Theaters this day of the 3rd of November 1893
Signatures
### page 7
Christina Olsen