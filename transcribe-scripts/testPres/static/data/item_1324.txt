
## 1324: Noticias de la nueva California [in Spanish], vol. 1 [part 1], 1792
### page 1
BLANK PAGE
### page 2
Noticias dela Nueva California
Parte 1a
California 
noticias + unclear
### page 3
1
Jesus Maria y Josse
Recopilacion de noticias dela antigua, california del tiempo qe administraron aquellas Miciones, los Micioneros dela regular obserbancia de nuestro serafico Padre San Francisco, del Apostolico Caolxio de San Fernando, de Mexico, y delas nuebas que los dichos Micioneros fundaron en los nuebos establecimientos de San Diego y Monterrey

      Escritas pr el menor (por mas indigno) dedichos Micioneros, que haviendo estado, en la antigua California desde qe entro de cargo de dicho Colegio, hasta su entrega alos reverendos Padres dela sagrada Religion de nuestro unclear Padre Santo Domingo, y uqe despues con otros Micioneros, del mismo Colexio de San Fernando subio alas nuebas Miciones de Monterrey. no tieniendo otro fin de este material trabaxo, que tomo los ratos qe el Ministerio, Apostolico me permiten qe eldejar anotado,
### page 4
todo lo qe ha contecido, y fuere sucediendo en el tiempo que Dios me preste la vida y salud, para travajar en esta Nueva Viña del Señor pa qe quando el chronista de nuestros Colegios piden al de San Fernando, las nocticias de las Apostolicas Labores las tengan recopiladas en un tomo, o mas si hubiere qe notar, dejando ala avilidad del choronista el ponerlas en el estilo qe puedan salir a luz; Y asu prudencia, y religiocidad, el dejar pa el secreto del Archibo, las que solo se escruben para lo qe pueda combenir, para tapar laboca delos emulos del ministerio Apostolico, qe nunca faltan en las nuevas comverciones por si hablaren algun dia delos hechos de los Micioneros, se tengan dmano todos los acaecimientos, segun y como pasaron en la California, asi antigua, como nueva todo lo qual contodo cinceridad, y verfaf referira enesta recopilacion.

  Divididas en quatro partes: En la primera podra lo acaecido, en la antigua California, los cinco años que corrio a cargo del Colegio. En la segunda dire las expediciones qe se hivieron para la cut word
### page 5
2
quisa de Monterrey, y la fundacion delas cinco, primeras Miciones. En la tercera elestad de ellas a ultimos de Diciembre de mil cetercientos setenta, y tres, segun el informe qe se remitio desu Exd y en la quarta y ultima, parte se anotara quanto fuere sucediendo digno de nota.
Parte 1a
Delas noticias dela antigua California Capitulo 1°. Como entro la California a cargo del Colegio de San Fernando
Por la expatriacion delos reverendos Padres Jesuitas, que por orden de su magcitad el Señor Carlos tercero (que Dios guarde) se hizo en esta Nueva España el dia veinte y cinco de Junio de mil setecientos sesenta y ciete, siendo Virrey, el exmo. Señor Marquez, de Crois, se acordo su Exd. de encomendar al Apostolico Colegio de San Fernando las Miciones dela California y se vio precisado el Reverendo padre Guardian, Frai Tosso Garcia a admitirlas con el permiso por la falta de reales qe leugo se fuese a España atraer religiosos y qe entre, tanto venia, se quedaron en las Cut word miciones?
### page 6
cut word, dela Sierra Tonda, solo cinco Religiosos, uno para cada Micion, y los demas fuesen, para completar el numero dedoce.

  En atencion a esto determino el V. discretorio elque se escriviese a la Sierra, combidando ala Tornada a los Micioneros yque si animaban fuesen en derechura a in coprporarse con los demas qe luego saldrian, del Colegio: Como no setenia Certidunbre, si se comletaria el numero de los cinco, para que no hubiese atraso, se determino saliesen, del Colegio nuebe, y que llegando a Queretaro o Guadalajara, si iban a incorporarse, los cinco se bolbiesen dos, pa la Sierra. Los nombrados por el V. Discretorio que havian de salir del Colegio fueron los siguientes: El Reverendo Padre Predicador Frai Junipero Serra Doctor, y Cathedratico de Prima de Sagrado Theologia, comisario del Santo Oficio y Precidente delas Miciones dela Santa Provinica de Mayorca.

El Padre Frai Francisco Palsu Hijo dedicha Provincia, y Micionero dedicho Colegio. 
El Padre Fray Juan Moran dela Provincia de la concepcion, y Micionero.
### page 7
3
El Padre Frai Antonio Martinez dela Provincia de Burgos.
El Padre Frai Juan Ignacio Gaston hijo de dicha Provinica.
El padre Frai Fernando Parnon, hijo dela Provincia de Estremadura.
El Padre Frai Juan Sancho dela Forne dela Provincia de Mayorca.
El Padre Frai Francisco Gomez, dela Provincia dela Concepcion.
Y el Padre Frai Andres Villumbrales, dela dicha Provincia.

  Salimos todos los dichos de nuestro Colegio, el diez y seis, de Julio dedicho año de sesenta, y siete, corriendo el gasto detodo lo nesesario, de cuenta del Rey. Y llegados a Queretano, no haviendo llegado los dela sierra, ni dandonos lugar a esperarlos, seguimos los nueve dichos hasta Guadalajara, endonde tampoco los encontramos y nos fue presiso seguir hasta el Pueblo de Tepic, endonde llegamos contoda felicidad, y nos ospedamos en el Hospicio dela Santa
### page 8
cruz que es de Religiosos obserbantes de Nuestro Orden, de la Santa Provincia de Jalisco.
En este Pueblo en contramos la Tropa qe iba para la expedicion del Serro Prieto dela Provincia, de Sonora, que estaba detenida a causa, deno haverse concluido los dos paquetotes?, que se estaban de cuenta del Rey construyendo para pasar la Tropa a el Puerto de Guarimas. En cuanto llegamos a este Pueblo, vino el Tesorero dela dicha Tropa, a vicitarnos, y a decir del Reverendo, Padre Precidente, que tenia Orden del Exmo. Señor Virrey, para prober detodo lo nesesario, para la mannuntencion de los Religiosos, agradeciolo el Reverendo Padre Precidente, diciendole que comeriamos en comunidad con los Religiosos del Hospicio y el Reverendo Padre Precidente del, correria en ello: pues no hacia esta caridad: En atencion aesto, cuido de entrgar el Sindico, del Hospicio la limosna, necesaria pa que el Reverendo Padre Precidente dedicho Hospicio nos diese la comida ordinaria,



Cruz that is the Religious observers of Our Order of the Saint Province of Jalisco.
In this town we found the troop that was going to an expedition to the Serro Prieto Prieto Hill in english of the Province of Sonora, that was detained cause they had not concluded de two unclear that were being constructed by the King to pass the troop to Gurimas Port. As soon as we arrived at this port, the treasurer of said troop, came to visit us, and to tell us of the Reverend Priest President that had order from his Mr. Excellency the Viceroy to approve of all the needs of the maintenance of the clergy?, the Reverend Priest President thanked him saying that we would start
### page 9
4
de religiosos: Yassi lo hizo quedando todos, mui contentos, y agradecidos ala caridad, de Nuestro rey, que husaba con nosotros, sus menores Capellanes.
Capitulo Segundo
Micion en Tepic, y lo que sucedio en el tiempo deella
Pocos dias despues de haver llegado a Tepic, supo, el Reverendo Padre Precidente dela Micion, que se estava disponiendo una Balandrita, para salir para California, con el Governador, nombrado deella Don Gaspar de Portola con alguna Tropa de soldados Dragones, y Miguetes, consu Alteres, y un Capellan dela Tropa llamado el Bachiller, Donde Pedro Fernandez:  En vista de esto fue el Reverendo Padre Precidente a ver ael Señor Coronel, y comandante detoda la Tropa, Don Domingo Elizondo; En solicitud de conseguir qe se embarcasen tambien algunos Religiosos, y alcanso qe fuesen dos aunqe se siguiese incomodidad,
### page 10
por ser chica La Balandria; con este permiso determino que yo me embarcase con el padre, Fria Juan Ignacio Gasten;

  Dia veinte y quatro de Agosto por la tarde, nos embarcamos en Matanchel los dos reliosos con los demas expresados arriba, y la misma tarde  se hizo a la vela La Balandrita, acompañada de una lanch, en que iban todas las sillas, y equipaxe, delos soldados, con cinco de los Dragones, que se embarcaron en ella; como era tiempo de las furbunadas, no nos faltaron, buenos sustos por ser el buque tan chico, y mui selozo; principalmente la noche del veinte y ocho que estando mar adentro, sobre el cubo de corrientes selevanto, una recia tempestas como alas seis, dela tarde que duro hasta serca de media noche, con la que nos vimos a el peligro de perdernos, disponeindose todos para morir, (y ciertamente pense que nos perdieramos) en la maior afliccion




because The Balandria was small; with this permit he detemined that I was to embark with Priest Fray Juan Ignacio Gasten;

 Day 24 of August in the afternoon we embarked in Matanchel the two clierks and the others mentioned above, that same afternoon the Balandrita started their voyage joined by a small boat that carried all the chairs and bagagge of the solidiers along with five of the dragones that embark in it; Because it was time of Furbunadas, we had several scares in account of our boat being small; most important was the night of the 28th when we were in deep ocean, above several currents a big storm started around six in the afternoon and lasted almost till midnight, we were in danger of getting lost, we were getting ready to die, (I really thought we were lost)
### page 11
me dixoel señor Governador, (que ya lo havia confesado por lo que podia suseder) que hicieremos alguna promesa a algun santo, porque nos liberase de aquella orrible, tempestad; Al decir esto me unclear el Padre compañero, Fray Juan Gaston, el sacate que yo traia dela Santa Cruz del Hospicio de Tepic, y prometiendo cantarle una missa, ala Santa Cruz, y asisitr, todos a ella tire a la Mar unas ebras del sacate (que tienen y aprecian los Tepiqueños como reliquia) y ciertamente puede decir; que en cuantto cayo a la mar dicho sacate se aplaco de modo que se puso en calma, no puedo asegurarlo, de milagro, pero si qe todos los tubimos, por pordigio, y gran misericordia de Dios, y en accion de gracias luego que llegmaos al Hospicio cantamos la missa, alaque asisiteron el Señor Governador con muchos oficiales




Mister Governor told me (that he had confessed that is why it could happened) that we should promise to some Saint, to free us from this horrible storm; After saying this Priest Fray Juan Gaston reminded me of the hay I brought from the Holy Cross of the Hospice in Tepic, I promised to give a mass, to the Holy Cross and attend, saying this a threw some straws of hay into the ocean ( that the people from Tepic have and appreciate as relic) and I can surely say : that as soon as said straws of hay fell in the ocean it stopped, it calmed down, I cant say that it was a miracle, but we all saw it, unclear and of the great Mercy of God, and as a thank you after we arrived at the Hospice we gave mass to which the Governor and many other officers assisted
### page 12
de la tropa y todos los soldados que se habian embarcado; lo que se hizo en el tospicio, porque con lista de las continuas furbunadas, mando el senor gobernador, bolbiesemos a Matanchel por consebinuno era tiempo para el uiaso e, aunque lo fue para la lancha que en once dias estaba ya en el puerto escondido de la california, como ciete leguas del Real Precidio de Loreto: aunque no desembarcaron, esperando la Balandra: Como llebaba la orden; pero biendo no parecia despues de handar toda la costa interior, de la California desde dicho puerto hasta el cabo de san Lucas, no hallandola, se bolbio a matanchel. 
Con la llegada de dicha lancha del puerto escondido llego noticia de los Padres Jesuitas, que iba el gobernador de la Peninsula, y que lo acompanaban dos religiosos misioneros del colegio de San Fernando; que es lo unico que los
### page 13
6
de la lancha, dijeron aun Indio que bieron, endicho Puerto, escondido, callandole todo lo demas(que es vien se admiran en la gente de mar, y mas siendo los marieneros, los mas deellos criollos de la california) Por estas confusas noticias, exejeron los padres Misioneros Jesuitas, que se les havia admitido la renuncia que a˜õs antes havian hecho de dichas Misiones, del Exmo Señor Marquez de Cruillas, Virrey dela Nueva Espa˜ã; Pero jamas pensaron el golpe dela expatriacion; al saber qe yban Padres Micioneros de San Fernando, hicieron muchas, demostraciones de alegria, como me aseguraron assi Indios, como soldados mereciendoles, que alabasen nuestro Apostolico, Ynstituto que sirvio mucho para que los Yndios nos reciviesen bien, y no les fuese tan sencible la salidade los Padres, que los havian criado, y que no havian visto, ni conosido a otros.
A
### page 14
A los seis dias del mes de septiembre bolbimos a estar de buelta en Tepic, y encontramos, en el hospicio, ya mayor numero de micioneros, pues estaban ya alojados en el catorce colegio de la santa cruz de Queretaro, que iban para las Primerias de la Provincia de Sonora; y once obserbantes de la Provincia de Jalisco que con otro que faltaba habia de pasar tambien a las misiones, de Sonora; Y pocos dis despues, llegaron otros siete de la misma Provincia que Iba a ocupar las Miciones, de Nayarith, asi mismo encontramos los cinco de nuestro Colegio de San Fernando que venian de la Sierrra Gorda y que fueron los Padres Frai Josse Murguia, Frai Juan Ramos de Cora, Frai Juan Crespi, Frai Miguel de la campa y Frai Fermin Lazuen. Y aunque estabamos dos mas del numero pedido pero como el Ilustrisimo senor obispo de Guadalajara, nos dijo que no tenia
### page 15
clerigos, que embiar, y que ya le havia escrito a su ExA determino el precidente, no despachar a ninguno para la sierra, conciderandolos a todos necesarios, y que aun faltaban dos, para completar el numero de los misioneros Jesuitas, que havia en la California. 
Era la orden de su ExA. que todos los micioneros, y sus tropas fuese por Max, asi los de la california (que era presiso) como los que iban para sonora y que se hiciese con los dos paquebotas, que se estaban fabricando en el Rio de Santiago; Estavan estos mui atrasados, y asi havia de ser mucha la demora. Atendiendo a esto, y el ver a tantos micioneros, en alguna manera osiosos, pretendio el Reverendo Padre Precidente de nuestra, micion, de San Fernando el que se hiciese Micion en Tepie; hablolo al Reverendo Padre Precidente de la micion de Queretaro, y haciendo tratado el asunto, re
### page 16
solbieron suspenderlo juzgando no estaba dicho Pueblo en dispocicion de Micion, y que poco, o ningun fructo se sacaria, juzgando mejor, el suspenderlo, hasta tanto se desaogase algo de la Tropa, que los ___ que quedase, despues de la salida esta, podrian hacer la micion. 
A principio de octubre despues de pasao el equinoccio, Determino el comandante, de la Tropa Don Domingo Elizondo, se dispusiese la Tropa que havia de marchar para la California, con el senor Governador, pasando recado a nuestro Padre Precidente, que se embarcaria con sus Religiosos; Que tenia determinado saliesen amediados de dicho mes con la Balandrita, y una Lancha que estaba en matanchel, propia de un minero de la California, Don Manuel de Ocio; En quanto recivio, ester recauso, passo el Padre Precidente, a terse con los senores, comandante y Governador, que se havian de Embarcar
### page 17
y diciendole estre que escojia para so, y la tropa, la balandrita, y que en la lancha, yrian los micioneros, determino hir personalmente a verla, y rejistrar si habia, lugar para los catorce, Religiosos como defactofue, y estando en matanchel, llego correo de Mexico con la novedad, de que su Eja. mandaba fuera la micion de san Fernando a la Provincia de Sonora, junto con la de Queretaro, y la de Jalisco, pasase a la California, cuia orden me intimaron (porque dar, Yo de Precidente nombrado de Colegio, en aucencia del Reverendo Padre, Frai Junipero Serra) entregandome a el mismo tiempo, una carta de nuestro Padre, Guardian, en que nos decia que por haver tenido noticia, el mui Reverendo Padre Comisario General Frai Manuelde Nagera, que los misioneros de la Micion del colegio de Santa Cruz de Queretaro, por lo que de ellos se havia ejprimentado, en el hospicio de Tepic
### page 18
no havian de tener en la sonora, union y armonia (que el tan importante) con los Religiosos de Jalisco, havia pedido a asu EjA que los obserbantes pasasen a la california, y los micioneros de los dos colegios, pasasen a sonora, que siendo, de un mismo Instituto se llebaran me mejor. 
En cuanto resivi la carta, y me ente con la novedad desoache correo a Matanchel avisar del Padre Precidente quien luego se bolbio del Hospicio, sintiendo mucha la Independencia novedad el mismo efecto causo a todos nosotros como tambien a los micioneros de Queretaro, cuio precidente viendo lacrado el honor, de su micion, no remoliendole en lo mas minimo la conciencia de que Religioso alguno hubiese dado motibo para presumir habia de faltar la union, y armonia entre sus micioneros, y los de la provincia, se presento con
### page 19
con una peticion a los dos precidentes del hospicio, y de la micion de la provincia; suplicandoles, que el piee della de clarsen si havian visto, alguna cosa en alguno de los micioneros que indicare lo mas minimo de desunion, a lo que respondieron, que no havian visto lo mas minimo, antes vien havian exprimentado entodos ellos, mucha caridad, y afecto, y queno tenian el menor motibo, sospecha, faltaria la buena armonia, y union. 
Al mismo tiempo que corria esta diligencia el Reverendo Padre Precidente, del colegio de Queretaro, llego el nuestro de Matanchel, y viendo de sus micioners desconvoladosm assi por la variacion del destino como por el motivo que se havia alegado determino, que Yo con otro companero pasase a la ciudad de Guanajuato, a verme con el senor Vicitadpr, general a fin de saber si havia mudado de intencion en cuanto a nuestro, destino, y que en casso que hasi fuese
### page 20
Le satisfaciese de que los padres micioneros de Queretaro, no havian dado motivo para que los de la Provincia de Jalisco pretendiesen el no ir, con ellos a la Sonora, sino a la California; Llebando para ello las certificaciones , de los dos REverendos Padres Precidentes, observantes del Hospicio, y de la Micion, de la Provincia, sacrifiqueme adicho viaje, tomando de companero del Padre Frai Miguel de la campacos, criollo de Durango en la Nueba, Viscalla e Hijo del colegio, y tomada, la vendicion del ReverendoPadre Precidente, salimos del Hospicio el dia diez, y nuebe de Octubre, del mismo tiempo que salian para Matanchel, los Padres, obserbantes, que iban a embarcarse, para ir a la California que por no haver llegado el uno que faltava para completar el numero de doce, llebaron para suplir, un colegio del obispadode Guaxaca, Don Isidro Ibarsabal, que habia venido arrimado a la tropa, con el fin de pasar a sonora; embarcaronse de los dichos, en la
### page 21
lancha, y el senor Governador con las tropas de la Balandra, condicho capellan Don Pedro Fernandez; Y yo con mi companero camine para Guanajuato. 
Llegamos a dicha Ciudad el dia primero de noveimbre, al medio dia, yendo a parar al convento, de los Reverendos Padres Descalsos, de vuestra orden, y despues a las tres fuimos a Vicitar al senor Vicitador general y aproponerle el motivo de nuestra venida quien luego dijo que lla sabia lo que habia susedido, y que era contra su voluntad, y que no era esta la intencion de su majestad que enquanto el hubiese llegado a mexico habria ablado a su EjA y sin la menor duda, no habria mandado transportar de la sonora, a la california: Y de esta a los Padres, observantes, del destino de la sonora Pero que suspuesto havian tomando el trabajo de benir del Fipec, lotomasemos tambien en pasar hasta Mexico, que el nos darian cartas para su Exa y que en cuanto llegasemos.
### page 22
nos despacharia, Combenimos a ellos, y el dia siguiente nos entrego las cartas, y depsues de Missa, nos salimos para Mexico, a donde, llegamos, el dia nuebe de dicho mes, Y haciendo referencia a el reberendo Padre Guardian y V. Discretorio lo que havia pasado nos remitio a su "EjA?" Quien enquanto vio la cartra del senor Vicitador General, y referido lo susedido, en Tepic y la causa de la dentencion, dio luego "unclear" rebocando el que habia dado, mandado de nuebo que nosotros pasasemos a la california, y los obserbantes, a su primer destino de Sonora. entregaronme, este DCcreto, el dia once, y pasando a dar las gracias de el a su "ExA" nos dijo que lo despachasemos por correo, y descansasemos algunos dias del dilatado viaje, lo mismo, parecio del Reverendo Padre Guardian y asi se ejecuto embiando correo a Guadalajara, con dicho DCcreto, y quedamos descansando unos dias. 
En estos dias se celebro el capitulo del colejio, en el que salio de Guardian el Re
### page 23
berendo Padre Frai Juan Andres, a quien pedi, permitiese viniese otros dos Micioneros, mas, supuesto que lla no hiban clarigos y que eran nesesarios diez, y seis saserdotes, propusolo en Discretorio, y habiendo combenido, fueron nombrados los Padres Predicador, Frai DConicio Barterra, y Frai Juan de Medina Beitia, ambos de la Provincia, de Cantabria, y juntos los cuatro saliamos, del colegio para Tepic el dia seis de Diciembre, sin haver tenido entanto caino, la menor nobedad; llegamos del Hospicio, de dicho Pueblo, el ultimo dia de dicho mes, siendo recividos de los demas con extraordinarias demostraciones de Alegria. 
Capitulo 3#
Prosigue la materia del antecedente La dije en el capitulo inmediato, que se embarcaron, los Padres observantes para la california, saliendo el mismo del Hospicio, para embarcarse, que nosotros para Mexico;  Quedando, en el Hospicio las dos Miciones de los
### page 24
colegios, en donde se mantubieron sin la menor nobedad en este tiempo se concluio el "Paquebot?" nombrado San Carlos, y determino el comandante, Elizondo, el Embarcarse en el con todos los soldados Dragones, y Caballeria dejando ordenado que despues en el otro barco, o con los que vinieren de la California se embarcaria lo restante de la Tropa, y los Padres, Micioneros; Salio dicho comandante, con dicho Barco por la pasqua, de noche buena, ya los ocho dias bolbio de arribada a San Blas, por cuio motibo determinoise por Tierra con todos los Dragones dejando el de San Carlos, para que se embarcase lo restante de la Tropa para Quaimas, y que fuesen tambien los micioneros que cupiesen de los que iban para Sonora, que eran los del colegio de Queretaro. Aeste tiempo llego a San Blas, el Baquebot nombrado nuestra senora de Loreto, que venia de la california embiado del senor Governador Don Gaspar, de Portola que habia llegado del cabo de
### page 25No transcription.
### page 26No transcription.
### page 27
dispuso, el viaje, embiando el Reverendo Padre Precidente, a dos de los Religiosos a San Blas por delante, para que resibieren las cargas, y dispusiesen, lo nesesario para el Barco, aunque esto no fue nesesario, porque corrio todo de cuenta del Rey, y a cargo de Don Manuel, De Ribero, comisario de San Blas, quien cuido; de poner todo el Rancho nesesario para el viaje, nombrando para conductor que cuidase de todo a Don Antonio Taroca, comandante, de Artilleria, para que nos companase, junto, con un Alferez, y ocho soldados. Luego, que nos dieron el haviso salimos de Tepic que fue el dia trece de Marzo, y el catorce, a medio dia, estabamos, ya todos en San Blas; En donde haiamos antlado al Paquebot, de San Carlos que venia de arrivada, despues de quarenta dis de navegacion, y al Reverendo Padre Precidente de la Micionde Queretaro, con otros cinco Religiosos, de su Micion, refiriendos los trabajos, que habian padecido los quaretna dias, de navegacion, sin hacer viaje, que
### page 28No transcription.
### page 29No transcription.
### page 30No transcription.
### page 31No transcription.
### page 32No transcription.
### page 33No transcription.
### page 34No transcription.
### page 35No transcription.
### page 36No transcription.
### page 37No transcription.
### page 38No transcription.
### page 39No transcription.
### page 40No transcription.
### page 41No transcription.
### page 42No transcription.
### page 43No transcription.
### page 44No transcription.
### page 45No transcription.
### page 46No transcription.
### page 47No transcription.
### page 48No transcription.
### page 49No transcription.
### page 50No transcription.
### page 51No transcription.
### page 52
Indios, hastsa ponerlos en todos santos. Assi se executo, por el mes de Septiembre, havecindandose en todos santos, los dos pueblos de dolores, y san Luis, que eran como ochocientas Almas; A la de Santiago, se mudaron los pocos que havian, en todos santos, y a san Josse del cabo, paso de San Javier, una Pancheria de quarenta, y cuatro almas, con que quedaron bien completas estas, tres miciones, del sur. Y en el paraje de San Luiz, se mudo la Familia de Felipe Romero, soldado reformado con todos sus hijos dnadole pocecion de la tierra dejando para aquella Iglecia, todos los Hornamentos nesesarios, para que dijesen Missa, quando pudiese ir, el Padre Micionero de San Javier, dejando encagado, que quando hubiese dos micioneros, fuese el uno una vez del mes a decir, Missa; todos los demas Hornamentos vasos sagrados, y utencilios de Iglecia, y sacristia, les dio el destino, para las nuebas miciones, de Monterrey, de lo que hablare mas adelante. 
Capitulo 8#
### page 53
26
Viaje del Reberendo Padre Precidente, al Real de Santa Anna, llamado del senor Vicitador General y de lo que entre los dos se determino a serca de las miciones. 
DEsde que puso el senor vicitador los pies en la Pen-insula, fue mui amenudo escriviendo al Reverendo Padre Precidente sobre las dispociciones, de las Miciones de la Pen-insula, y del encargo que trahia de su Magestad, de las expediciones de Monterrey, a cuias Cartas, respondia el Reberendo, Padre Precidente, pero deceando su unclear. tratar, boca a boca, estos importantes asuntos, le escrivio tomase el travajo de pasar al Real de Santa Anna, para resolver, entre los dos lo que se jusgase por mas combeniente del vien, de la Pen-insula, y propagacion de nuetra, santa fe. En vista de la carta de su unclear passo a el Real de Santa Anna, el Reverndo Padre Precidente llegando a el el ultimp dia, de octubre de dicho ano de mil setecientos sesenta
### page 54
y ocho; comunicaronse entresi, y resolbieron algunos puntos, asi por lo que tocaba a las miciones, antiguas como a las nuebas. 
Resolvieron que sinodos habian de dar para los micioneros, que fue para las antiguas, y que tenian las miciones con que pasar, se les diese, por los dos micioneros, quatrocientos pesos anuales, para las otras que estvan mas pobres aquatrocientos, y cinquenta para las mas necesitadas, quinientos pesos; y para la de Santa Maria que era novisima, y para las demas que fueren fundadndo, asetecientos pesos, amas de que sedarian todos los Hornamentos, Basos, sagrados, y utencilios, necessarios de Iglecia, y Sacristia, Y que mas, de esto, sedaria un mil pesos para que se gastasen en lo que se jusgase, necesario para su fundacion. 
Propusele su Illma. la horden quetenia de la corte, y en cargo de su "ExA?" de despachar una expedicion por mar para la conquista, y Poblacion, de los Puertos de San Diego, y Monterrey para cuio efecto estaba aguardando los dos
### page 55
27 
Paquebotes, de su Magestad nombrados SanCarlos y San Antonio alias el Principe, que venian cargados de Vienes, y de todo lo necesario para la empresa; Assimismo declaro como tenia resuleto, el Despachar tambien por tierra, otra expedicion que fuese Caminando desde la micion de Santa Maria, frontera de la Gentilidad hasta San Diego, y que Poblando San Diego, siguiese su viaje hasta Monterrey y para eso le parecia combeniente el que fuese Micioneros, por Mar, y tierra para que se juntasen las tres miciones la una en el puerto de San diego, la otra entre esta y Monterrey, y que si fuera pocible se fundase, otra mas halla de Santa Maria camino de San Diego, pero que para esto eran nesesarios, mas Micioneros. 
Oida esta propuesta por el Reverendo Padre, precidente, se ofrecio gustoso air en Persona, con una de las expediciones, y que tamcut word
### page 56No transcription.
### page 57No transcription.
### page 58No transcription.
### page 59No transcription.
### page 60No transcription.
### page 61No transcription.
### page 62No transcription.
### page 63No transcription.
### page 64No transcription.
### page 65No transcription.
### page 66No transcription.
### page 67No transcription.
### page 68No transcription.
### page 69No transcription.
### page 70No transcription.
### page 71No transcription.
### page 72No transcription.
### page 73No transcription.
### page 74No transcription.
### page 75No transcription.
### page 76No transcription.
### page 77No transcription.
### page 78No transcription.
### page 79No transcription.
### page 80No transcription.
### page 81No transcription.
### page 82No transcription.
### page 83No transcription.
### page 84No transcription.
### page 85No transcription.
### page 86No transcription.
### page 87No transcription.
### page 88No transcription.
### page 89No transcription.
### page 90No transcription.
### page 91No transcription.
### page 92No transcription.
### page 93No transcription.
### page 94No transcription.
### page 95No transcription.
### page 96No transcription.
### page 97No transcription.
### page 98No transcription.
### page 99No transcription.
### page 100No transcription.
### page 101No transcription.
### page 102No transcription.
### page 103No transcription.
### page 104No transcription.
### page 105No transcription.
### page 106No transcription.
### page 107No transcription.
### page 108No transcription.
### page 109No transcription.
### page 110No transcription.
### page 111No transcription.
### page 112No transcription.
### page 113No transcription.
### page 114No transcription.
### page 115No transcription.
### page 116No transcription.
### page 117No transcription.
### page 118No transcription.
### page 119No transcription.
### page 120No transcription.
### page 121No transcription.
### page 122No transcription.
### page 123No transcription.
### page 124No transcription.
### page 125No transcription.
### page 126No transcription.
### page 127No transcription.
### page 128No transcription.
### page 129No transcription.
### page 130No transcription.
### page 131No transcription.
### page 132No transcription.
### page 133No transcription.
### page 134No transcription.
### page 135No transcription.
### page 136No transcription.
### page 137No transcription.
### page 138No transcription.
### page 139No transcription.
### page 140No transcription.
### page 141No transcription.
### page 142No transcription.
### page 143No transcription.
### page 144No transcription.
### page 145No transcription.
### page 146No transcription.
### page 147No transcription.
### page 148No transcription.
### page 149No transcription.
### page 150No transcription.
### page 151No transcription.
### page 152No transcription.
### page 153No transcription.
### page 154No transcription.
### page 155No transcription.
### page 156No transcription.
### page 157No transcription.
### page 158No transcription.
### page 159No transcription.
### page 160No transcription.
### page 161No transcription.
### page 162No transcription.
### page 163No transcription.
### page 164No transcription.
### page 165No transcription.
### page 166No transcription.
### page 167No transcription.
### page 168No transcription.
### page 169No transcription.
### page 170No transcription.
### page 171No transcription.
### page 172No transcription.
### page 173No transcription.
### page 174No transcription.
### page 175No transcription.
### page 176No transcription.
### page 177No transcription.
### page 178No transcription.
### page 179No transcription.
### page 180No transcription.
### page 181No transcription.
### page 182No transcription.
### page 183No transcription.
### page 184No transcription.
### page 185No transcription.
### page 186No transcription.
### page 187No transcription.
### page 188No transcription.
### page 189No transcription.
### page 190No transcription.
### page 191No transcription.
### page 192No transcription.
### page 193No transcription.
### page 194No transcription.
### page 195No transcription.
### page 196No transcription.
### page 197No transcription.
### page 198No transcription.
### page 199No transcription.
### page 200No transcription.
### page 201No transcription.
### page 202No transcription.
### page 203No transcription.
### page 204No transcription.
### page 205No transcription.
### page 206No transcription.
### page 207No transcription.
### page 208No transcription.
### page 209No transcription.
### page 210No transcription.
### page 211No transcription.
### page 212No transcription.
### page 213No transcription.
### page 214No transcription.
### page 215No transcription.
### page 216No transcription.
### page 217No transcription.
### page 218No transcription.
### page 219No transcription.
### page 220No transcription.
### page 221No transcription.
### page 222No transcription.
### page 223No transcription.
### page 224No transcription.
### page 225No transcription.
### page 226No transcription.
### page 227No transcription.
### page 228No transcription.
### page 229No transcription.
### page 230No transcription.
### page 231No transcription.
### page 232No transcription.
### page 233No transcription.
### page 234No transcription.
### page 235No transcription.
### page 236No transcription.
### page 237No transcription.
### page 238No transcription.
### page 239No transcription.
### page 240No transcription.
### page 241No transcription.
### page 242No transcription.
### page 243No transcription.
### page 244No transcription.
### page 245No transcription.
### page 246No transcription.
### page 247No transcription.
### page 248No transcription.
### page 249No transcription.
### page 250No transcription.
### page 251No transcription.
### page 252No transcription.
### page 253No transcription.
### page 254No transcription.
### page 255No transcription.
### page 256No transcription.
### page 257No transcription.
### page 258No transcription.
### page 259No transcription.
### page 260No transcription.
### page 261No transcription.
### page 262No transcription.
### page 263No transcription.
### page 264No transcription.
### page 265No transcription.
### page 266No transcription.
### page 267No transcription.
### page 268No transcription.
### page 269No transcription.
### page 270No transcription.
### page 271No transcription.
### page 272No transcription.
### page 273No transcription.
### page 274No transcription.
### page 275No transcription.
### page 276No transcription.
### page 277No transcription.
### page 278No transcription.
### page 279No transcription.
### page 280No transcription.
### page 281No transcription.
### page 282No transcription.
### page 283No transcription.
### page 284No transcription.
### page 285No transcription.
### page 286No transcription.
### page 287No transcription.
### page 288No transcription.
### page 289No transcription.
### page 290No transcription.
### page 291No transcription.
### page 292No transcription.
### page 293No transcription.
### page 294No transcription.
### page 295No transcription.
### page 296No transcription.
### page 297No transcription.
### page 298No transcription.
### page 299No transcription.
### page 300No transcription.
### page 301No transcription.
### page 302No transcription.
### page 303No transcription.
### page 304No transcription.
### page 305No transcription.
### page 306No transcription.
### page 307No transcription.
### page 308No transcription.
### page 309No transcription.
### page 310No transcription.
### page 311No transcription.
### page 312No transcription.
### page 313No transcription.
### page 314No transcription.
### page 315No transcription.
### page 316No transcription.
### page 317No transcription.
### page 318No transcription.
### page 319No transcription.
### page 320No transcription.
### page 321No transcription.
### page 322No transcription.
### page 323No transcription.
### page 324No transcription.
### page 325No transcription.
### page 326No transcription.
### page 327No transcription.
### page 328No transcription.
### page 329No transcription.