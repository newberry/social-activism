
## 1288: Sigismundo Taraval journal accounting Indian uprisings in Baja, California [in Spanish], 1734-1737
### page 1
Front cover - no visible text
### page 2
sintieron assi [Zabian] como los otros, q todos eran de
los suios la huida de sus mujeres, ahora porque
querian matarlas, ahora porq habian de contar
q.to habian platicado, mas como ya habian perdido
el miedo a las armas y d los soldados
como en esto se demuestra con evidencia, siendo
solo ocho, o nueve se atrevieron ir de dia
Illegible erasure á sacarlas de donde estauan.
237. Mas quiso Dios castigarles, que
fuese oido, y librar las mujeres q les hubiera 
costado la vida si lograra su intento. Fue pues
con los suios una mañana oieronle los Nros. aunq
el puso p.a no ser oido todas las diligencias q podia
con la plena noticia q tenia de todo, Impusieronse
los Nros. todos alerta, y aun en
armas, porq juzgavauan q venian los enemigos
todos á darles assalto. Luego q conocio ser
oido escondiose con los suios, y uiendose al descubierto
poco á poco se fue retirando. No lo pudo hazer
de suerte q los q los q estauan alerta de 
centinela, y de uijia no lo viessen. Luego
### page 3
236. Mudóse, salió de la Mission
con no se q pretexto, y fuése a los suios. Hablo
con ellos con los Aripes y con los demas. Dixo
Les qto. se haria, habia y se hablava en Santiago
Informoles de las pocas fuerzas q habia en Los
Dolores y en Loreto, y de la inutilidad ineptitud,
y miedo de los q estavan en Santiago.
Como se podian assaltar, como coger, y como
acauarlos. Despues de haberles dicho qto. [pensava]
conu.te q. D su intento se boluio a la MIssion
á contar míl mentiras y a ser espia
de los Apostatas. Oieronle en ella una ú
otra proposicion, q permittio D.s p.a que se
descubriera. Temiendo el q de ellas se podiese
congeturar todos sus designios huióse con
otros una noche siguieronle por la mañana,
mas de ceremonía. Llevaronse sus
mujeres, mas estas regalando por lo q les
oieron, q los havian de matar, los dejaron
descuidar, y se bolvieron huiendo a la Mission,
y contaron quanto passava. Mucho
### page 4
descubrio todas sus tramas confeso publicamte sus
delitos, y confesose merecedor de qualq.r castigo
Substanciole el Comand.te la causa, hechole la
la sentencia, pusieronle luego en la capilla, y
el dia 8 de Agosto lo mando passar por las armas
Dixo estando en la capilla lo q les habiar de
embiar a decir su p.te a los suios despues
de su muerte, q todo fue una platica de
consejos, y desengaños. Al salir al patibulo
hizo otra, y con no poco zelo a los q estavan
presos. Murio por ultimo con muestras, y no 
vulgares señales de verdadero catholico, arrepentido
y predestinado. Admirose en el claram.te
lo q con tanta verdad, como discrecion dixo
un sabio q nunca empuña Dios la espada
de su Justicia si no es con la mano diestra
de su misericordia.
### page 5
q lo divisaron algunos de los Yndios auxiliares
fueron p.a el; viendo q ya le iban a alcanzar
y q estavan a tiro de flecha hizo frente
a los Nros. tiraron de ambas p.tes varias flechas,
y como assestavan principalmente
a el quedo herido, prosiguio el debate hirieronlo
segunda vez, continuó con todo y
quedó herido terzera. Viendose con tres
heridas, q todos le tiravan á el, que 
venia ó acudia mas gente de los Nros hincose
de rodillas, y pidio confession. Mientras
executava esto y los Nros se suspendieron
huieronse los otros, q a no hazer esto todos
ó casi todos hubieran caido. Assi el que con 
su delito los habia puesto en peligro con su
arrepentim.to los saco de el, y con su castigo
despues los saco tambien del delito.
238 luego q lo vieron tirar las
armas, hincarse de rodillas, y pedir confession,
fueron los Nros p.a el, y lo levantaron
asseguraron y llevaron al real. Allí
### page 6No transcription.
### page 7No transcription.
### page 8No transcription.
### page 9No transcription.
### page 10No transcription.
### page 11No transcription.
### page 12No transcription.
### page 13No transcription.
### page 14No transcription.
### page 15No transcription.
### page 16No transcription.
### page 17No transcription.
### page 18No transcription.
### page 19No transcription.
### page 20No transcription.
### page 21No transcription.
### page 22No transcription.
### page 23No transcription.
### page 24No transcription.
### page 25No transcription.
### page 26No transcription.
### page 27No transcription.
### page 28No transcription.
### page 29No transcription.
### page 30No transcription.
### page 31No transcription.
### page 32No transcription.
### page 33No transcription.
### page 34No transcription.
### page 35No transcription.
### page 36No transcription.
### page 37No transcription.
### page 38No transcription.
### page 39No transcription.
### page 40No transcription.
### page 41No transcription.
### page 42No transcription.
### page 43No transcription.
### page 44No transcription.
### page 45No transcription.
### page 46No transcription.
### page 47No transcription.
### page 48No transcription.
### page 49No transcription.
### page 50No transcription.
### page 51No transcription.
### page 52No transcription.
### page 53No transcription.
### page 54No transcription.
### page 55No transcription.
### page 56No transcription.
### page 57No transcription.
### page 58No transcription.
### page 59No transcription.
### page 60No transcription.
### page 61No transcription.
### page 62No transcription.
### page 63No transcription.
### page 64No transcription.
### page 65No transcription.
### page 66No transcription.
### page 67No transcription.
### page 68No transcription.
### page 69No transcription.
### page 70No transcription.
### page 71No transcription.
### page 72No transcription.
### page 73No transcription.
### page 74No transcription.
### page 75No transcription.
### page 76No transcription.
### page 77No transcription.
### page 78No transcription.
### page 79No transcription.
### page 80No transcription.
### page 81No transcription.
### page 82No transcription.
### page 83No transcription.
### page 84No transcription.
### page 85No transcription.
### page 86No transcription.
### page 87No transcription.
### page 88No transcription.
### page 89No transcription.
### page 90No transcription.
### page 91No transcription.
### page 92No transcription.
### page 93No transcription.
### page 94No transcription.
### page 95No transcription.
### page 96No transcription.
### page 97No transcription.
### page 98No transcription.
### page 99No transcription.
### page 100No transcription.
### page 101No transcription.
### page 102No transcription.