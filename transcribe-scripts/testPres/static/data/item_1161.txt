
## 1161: Anna Sophie Raster diary of dream analysis, circa 1930
### page 1
Anna Hercz
ELMAN'S
Book Store
2300 W. Van Buren St.
Name  Arthur R. Hercz
Subject Chemistry 1
Class 250  5+6    Date Feb. 
Instructor Wilson
Dr. Neymann
Note Book No 64
### page 2
Dreams. Anna Hercz.
1st night, July17.
Walking with Mrs. Waller and some other person away from the Sanatarium along a country-road with street-railway.  Mrs. Waller urging me to take the street-car, but I insisted on walking which led her to suspect that I wished to avoid people, especially the police, fearing arrest.  (On the night before this dream I had seen a movie full of detectives and pursuit).  
Being somewhere in a house, (not familiar in waking) packing for a trip abroad, where I was being sent for "nerves".  My aunt (who made her home with us for eight years and is now in Germany, very sick after a cancer operation)
### page 3
was with me and, vaguely, my daughter.  My brother (who died four years ago) came in from Milwaukee, inquired about the trip and was told by my aunt that I was being sent away for "Wechselfieber".  (This term I had heard from my mother who used to tell me that as a child she had had this complaint).  It seemed to be understood by all of us that my brother, himself a nervous type, had better not be told about my nerves.  (On the day preceding this dream I had received long letters from my brother's daughter, now traveling abroad, and from my aunt in Germany).
Merely a vision of an old furnace (in the basement of
### page 4
the house where I was born and lived for 30 years), with the points of a large pair of open scissors showing beneath the ash door.
2nd night, July 18.
Taking treatment at the Sanatarium.  Being put with another patient on a small sliding platform and rolled along while hot and cold air was blown at us.  I was told to hold on to the other patient, a very thin woman, who finally vanished entirely, leaving me with her empty négligée in my arm.
Later at the Sanatarium.  A meal being served in the basement to some other patients and myself.  An attendant brought
### page 5
me one of my own pearl-handled knives, quite blackened and broken and apologized for its appearance.  (Upon my return home from the Sanatarium I had found all my silver blackened or stained).
Later at the Sanatarium.  Mrs. Johns, another patient, and I wished to pack our things to go home, but were told by Mr. Ayers that everything had been locked away and could not be taken out at this hour.  Our things seemed to be in a small store-room behind a door camouflaged by a book-case.  (I had recently read about such a door).  Finally Mr. Ayers gave Mr. Johns and myself permission to go in.  It was pitch-
### page 6
dark and we could not find our things.  Mr. Johns rather familiarly put his arm around my shoulder.  (Mrs. Johns had often spoke about his manners with disapproval to me).  It seems that we all finally left the Sanatarium somehow.
### page 7
July 29th
Night-mares every hour, waking up at 2, 3, 4 a.m.  All more or less confused about Sanitarium patients, riding on trains, being insane myself.  The last impression deepest, causing very serious depression on day following. (Intestinal disturbance).
August 1st
Restless sleep, thumping in head.  Riding in a car from West to East down-town with some-one (brother or husband, vague).  Getting off by myself because I thought destination was reached.  Found myself far from loop with high office-buildings stretching endlessly
### page 8
towards East.  Faced necessity of walking the distance and worried about anxiety of my companion when he should discover that I was not on car (He had been on rear platform).  Saw the car I had left passing, but it was just a black locomotive.  Watched for other cars; they were also all single locomotives.  Woke with great noise in right ear. Nervous, hot-water bottle.  Later dreamt about some neighbor coming in for house-cleaning; face very familiar, but could not place it and became very uneasy trying to do so, fearing my faculties were leaving me.  Woke up restless.
### page 9
August 2.
Dreamt about Sanatarium Dr. Neymann and Sana. staff (doctors & nurses, but not the identical ones of Parkway) and myself taking troop of children, Boy Scouts (Mrs. Waller's nephew) on a hike, to a ravine, lakeshore.  Dr. N. angry with them, tried to soothe him by smoothing his hair, like Bubi's.  Like Bubi, he said: don't do that, impatiently.  Later at Sana. top floor, very cold.  Janitor said he did not know how to handle furnace, I told him my brother would show him when he came.  My
### page 10
brother came, laughed at small open furnace.  Later I was at large, open studio (some artist Oscar Gross? Hoffmann? Hassmann?) host.  Brother came in light-gray suit, quite sporty, took cigaret from host's pocket immediately and lit up.
August 3.
Being at some sewing-club or class, making fruits and vegetables of felt.  One member showed me her work: one piece a cherry (vague) and another a carrot, green with yellow top in which she put pins to show it's use as pin cushion
### page 11
August 4th
Being on ocean-liner and then somewhere in a European restaurant with O.O. McIntyre and his family: wife and two children.  Drinking iced drinks.
August 16th.
Being at Dr. Neymann's office, or rather, being through with treatment and starting home.  Dr. Neymann offering to drive me (as he had done in reality the last time) and walking with him to the auto we passed a street (like Stratford or Hawthorne) with gardens and pigeons strutting in one of them.
### page 12
One pigeon was yellow as a canary and I remarked upon it, but Dr. said that that was not unusual.  Also passed some people seated at a table, like in a beer-garden. (Vaguely, Dr. Luckhardt seemed to be one of them.)  When we reached auto, it was an old-fashioned buggy and Dr. N. asked me to put up the top, which worked something like a dentists' chair, going up in jerks by pressure on a foot-rest.  While doing this, I woke.
Previous to this dream, I dreamt, vaguely, of a quarrel with my husband about some thing some-one had said about
### page 13
him or myself.  All this I barely remembered on waking.
17th
(Dreamt this immediately after Mrs. Waller visited us and we had put her out).  
Going from my house with Mrs. Waller, with the object of taking her away, to a hotel where some friends (Notz's and Kranz's) lived.  Found them playing cards in their room, asked to join them.  The mother was putting the little girl (Mrs. Charles Hines?) to bed in a room across the hall and I
### page 14
helped her. Mrs. Waller stayed with the men in the other room and there put on the gown I had loaned her. 
Later returned somewhere and watched people passing in the street, particularly a family of ten pairs of little twins, all almost the same size, walking in a row. (Had seen a pair of cute twins the afternoon previous).
22nd August
Somebody piercing or lancing me in the spine (at waist) and waking with a very slight pain there. - Being with some children (son & nephew) in a garden,
### page 15
then finding them in a very dirty little house (pig-sty?) eating "corn-husks" as they said, - a sort of gray, dry, net (like lufa-sponge) the shape of an ear of corn, tasting like breakfast-food. 
Later, driving about with my mother on a Sunday, when all stores were closed, to shop a gift for my little niece or nephew.
### page 16
24th
Night-mare. (Had been nervous during day thinking about possibly becoming insane).
Being in some kind of asylum, going repeatedly down cellar to a class, seeing insane people about me, grasping their sleeves, suddenly gone empty, hearing insane laughter (forgot the rest immediately on waking). 
Awoke with rushing & singing noise in ear, very warm sudden heavy covering, head fallen from pillow; also some heart-palpitation! -
### page 17
26th
Disagreeable dream of cooking in kitchen (had been working there at baking all day) and taking a liver to fry (chicken-liver or a piece of liver), dropped it on floor where it seemed to turn into something alive, with mouth and closed eyes, moving and suffering. Felt suffering, very night-marish, - awoke feeling nervous, with head-noise. Back to sleep, dreamt of visiting, with my mother, some-one in a very pleasant, cottage-like hospital: Mrs. Meixner.  On first floor were offices of "Hansa" or something
### page 18
similar.  Were told we could not see her now because some one else, Mrs. Buhl? was with her.  Waited down-stairs where there was a pleasant dining-room.  Young boy-waiter asked to be of service, or brought glass of water, or something, - all vague.  Mother wanted to go to ladies toilet and asked me to go with her, - awoke.
31st
Being somewhere (in a garden) with my mother and, vaguely, old Mrs. Fleischmann.  Mother agitated & fearful at the thought of her approaching death, saying
### page 19
she was now 76 and had, at best, not much longer to live.  I soothed her with a long piece of philosophy.  Later, in a room, Ella Piper arrived from Powers Lake and was given breakfast by me, especially very hot coffee, served by Mrs. Aermel (who also had figured somewhere in a dream of that night as sitting at a table sewing for me).  Also my niece Susi appeared somewhere from N. Y., briefly.  Then my daughter came home with the young man she was going to marry, not Mr. B., but vaguely, Grant Strathern.  (I had written to Mrs. "  "  that afternoon.  I was dissatisfied and deeply hurt by
### page 20
Greta's behaviour, especially by her saying that she was going away to be married in the West and would stay away.  Later she came to me with a number of gifts she had brought home, made by herself, some very intricate metal-work (this partly due to place-cards seen at party & things from India at Eitels).
Then M. Arnold (friend who died last year with cancer) came home too and embraced me, calling attention to the fact that she was again able to wear a tight stiff corset, but when my hand touched the open laced space at the back she screamed and said I had hurt her, that
### page 21
the dr. had told her not to touch that spot, and she cried.  I felt remorseful & frightened, thinking that now, through my fault, she might die sooner. -
Somewhere in the dream I was looking out of a window, all this seemed to happen in a sort of boarding-house or small hotel, and looked into a large, airy                expressing-place, where [wagons were comin - crossed out] loads were coming in and big boxes & trunks were being handled.
Then, it seemed, I was preparing a meal in a room all littered up, my old desk, opened, standing in a corner, a large pot of soup boiling near
### page 22
Jung essay: "Krebs'-dream.
the floor probably on a small heater.  I used the desk as a kitchen table & prepared what seemed like a calf's heart (had been cleaning out squabs the day previous).  In each valve of the heart I found a queer substance), 2 different ones, a compact mass each.  I called to my mother in the door-way to come & see.  One of the pieces turned into what looked like a lobster-claw, whitish, and the other into a small flat fish.  Both began to move.  Mother came and closed the desk-lid on them, which seemed to squeeze the fish.  When I quickly opened the lid, there was a
### page 23
smaller fish hatched out. 
Then mother cried, "Your soup is boiling over and as I sprang to remove it, there was a sudden deluge of water from the ceiling over my desk in the corner, like a shower- bath. We looked up and say a leak in the ceiling, tried to think of a way to stop it,-- awoke.
Sept. 1st
Very vagues dreams. One of them in which my father seemed to be sick in bed somewhere and quite irritable. Mother & my brothers also somewhere about. I was writing picture-post-cards.
### page 24
Took packages of them from a dresser.  One package turned into a parcel of oil-paintings, or card-board, still wet, I handed them to the patient in bed for criticism (vaguely, he seemed to have my husband's authority on paintings).  Another parcel of cards, large ones, had pictures such as I lately saw in the movies with rivers and lakes of moving water.
Later was driving in a park with parents and brother, car (or carriage) slipped from a pier or steep road into a pond or swamp, but I neither saw nor felt the water, there seemed to be no danger, we were not
### page 25
submerged and soon extricated.  Later, sat in some restaurant near by, ordering food - - vague.
Also dreamt about a friend (Mrs. Meixner) showing me a box, which she opened and which was empty, that she had brought from abroad as a gift for some-one.  It was exactly like a little chest (only somewhat larger) that was given me when I was about thirteen (also brought from abroad) and in which I first kept intimate letters of my girl friends and later the little souvenir trinkets of my little son who died.
### page 26
Analysis:
Something to do with my grief and remorse at my mother's premature death.  Also something to do with a "cancer".  Uncertainty about my daughter's future.
What are boxes and trunks (especially open ones) a symbol for?
Soup boiling on small stove probably something to do with the "furnace" symbol.
What are small living and squirming things a symbol for? 
Water-leak probably something to do with sex-symbol. -
### page 27
Sept.20.
On awakening, the dream had already become very vague, it seemed to be all about being in a rather large, nice house with my mother (Christmas-time) and somewhere a poor family to be looked after.  Then my guardian Mr. Pietsch, arriving from somewhere with a son, to persuade us to do something we did not wish to do (it seemed to be to go somewhere with him, or for me to marry his son, or something about money).  He brought a gift (as a bribe), a sort of chandelier of
### page 28
Venetian glass, many small yellow bulbs or ornaments on it.  Then, trying to attach it to the ceiling (it may have been my mother who tried to do this, or myself) it fell and crashed to pieces.  Mother and I seemed glad of it. - Finally consented to go out with the Pietsches, but stopped in hall way to consider necessity of taking care of furnace-fire first.  Mr. Pietsch said he would do that and went to look after it. -
Then vaguely, we were with the poor family to whom we were supposed to give Christmas presents, - all very vague
### page 29
Sept. 25.
Many fleeting, confused dreams, all forgotten.  Vaguely remembered: Starting somewhere on trip in auto-bus or auto with various people (forgotten), one of them Nelda Meiners, (Possibly Greta, too.)  My mother also there, Leaving a trunk (packed) behind, right on curb of street.  Nelda and I protesting to my mother about leaving it so unguarded and asking her to take out valuables, at least.  Where upon Mother opened trunk and took out my little  pearl pin with Greta's picture. - Something
### page 30
more, vague, about showing each other clothes, newly bought. -
Sept. 26.
At dentist's, having tooth extracted: one large, hollow lower molar on right side.  No pain.
My friend Mattie Arnold packing books, manuscripts, etc. for a trip.
### page 31
November 30. [date underlined]
Visiting Marie Arnold dying at Hospital with E. Eitel & Clothilde Hotz. Mattie saying: "well, one of us must be the first to go. I wonder which one will be next?" Also telling of Lily Brand being ill like herself. - - Looking at a new car of Eitels; inside something like the old-fashioned hansome cab, done in maroon and lavender. Silly conversation, Emil saying: I tell Emma to get a lavender suit to match the car." And I replying: "then you ought to get yourself a maroon one, to make it perfect."
### page 32
December 7. [date underlined]
Very vague about Dr. N. being at our house in library(totally strange surroundings) instructing me from a small book, which he claimed was a most wonderful treatise. I had work to do (or to go out, or having company coming) and realized it was getting late (about 11[superscript]30 a.m. by clock) and was inwardly impatient, but said nothing.
Later went to County Hospital with Walt to see Tante Hansen. Entering and mounting stairs, a nurse plugged my Aurophone so that it would not be ruined in passing through a certain electric Zone. (This due to something similar I had just read). Found Mrs. H. in bed in
### page 33
[Begin underline] Mrs. H. had died on Dec. 1, a week before [End underline] 
a very pleasant room with 2-3 other patients. Her bed at large window with curtains overlooking tree in garden and nice street. She much better and very well satisfied. Said she had cancer (of throat?) and would be sick there a long time. A doctor and nurse came and started to shave her face about the eyes. Her complexion during this process was that of a negro. She did not seem to like it, but submitted. On leaving we asked if she were contented there and she said yes.
### page 34
December 12. [ Date underlined]
Something vague about being in a kitchen and somebody (vaguely my brother Edwin) was playing with a small box of kitchen matches. I warned him, but he did not heed and then the box caught fire. He threw it into the sink where it flared up into a tall yellow flame (like a taper) that touched and even slightly blackened the ceiling, so that I feared the fire would spread, but it died down. Also, vaguely, little spurts of fire (perhaps matches carelessly thrown) here and there, before above happened.
### page 35
[Begin underline] January 11 [superscript and underlined] th. [End underline]
Being at some resort (hotel) with mother. Mrs. Neymann also there and Dr. Neymann. Mrs. N. telling me that her lungs were much better, one entirely cured, and sang a few bars to prove it. Dr. N. sitting in chair listening. I complimented Mrs. N. on her lovely voice. She said she was going to California for a complete convalescence. Later mother and I walking in rather crowded street of resort-town, seeing (or looking for) an [lined out] a shop to buy Dr. N. something for his aquarium. Much more to this dream, but all vague & forgotten.
### page 36
[Begin underline]January 25[superscript and underlined]th[End underline]
A bit night-marish after a bad day. First dreamt something about cooking in the old home kitchen and going to fetch something in the old barn. Nasty feeling of being followed up a stair-case (spiral) by a cat or two (one, I believe, white). Wet spots on stair-case, somewhat disgusting. Having in my hand a package of sliced bacon and tearing off pieces to drop down to cat to keep her from following me. The rest vague. Woke with disagreeable feeling.
Later dreamt of house-cleaning in large house (reminiscent of Emma's flat with white walls, etc.) Greta washing pictures for me. Telling her I was glad to have some-one else do it after so many years. - Feeling sick (as in daytime) and Dr. N. came
### page 37
in to see me. Told him (as I had told husband on night before dream) that I ought to be in an institution. (Or perhaps he told me.
February 14.[date underlined]
Short dream of passing Dewes house with my son and noting that it had been torn down and replaced by a modern Clubhouse building. Looked into lobby and admired the modern plan of straight, hight stairs, in three sections (from pictures I had looked at before retiring). My son and I were troubled to think of the disappointment to my husband when he should find his house destroyed.
### page 38
February 15[superscript underlined]th [whole date underlined]
Woke early disturbed by present worries. Fell asleep again later and had night-marish dream of being lost in strange streets. Had been in house with Miss A. Michaelis (our old seamstress who suffered terribly before her death  with some brain or nerve trouble) and some other person whom I do not remember. The other person (a woman relative?) said that she was going out and I had better go down and get Miss A. her lunch, it being far past time. I went to kitchen, but wandered out to street instead (sense of having lost my mind throughout dream) and was lost in strange surroundings. Went a few blocks, then tried to return and entered a hotel to make inquiries. In a beauty or
barber-shop  at entrance a male attendant (unfamiliar) answered my questions, seemed concerned about my condition, thought I might be an amnesia victim. I asked him if he had seen anything about my disappearance in papers; told him my name. He said
### page 39
no. I wandered out again. In vestibule with glass doors and marble floor met many ladies and children, beautifully dressed, coming in. One tiny child in white misbehaved and left puddles of water on floor through which we others passed with some disgust. - Out in street, I was confronted with the same bewilderment, strangeness, sense of having lost my way and mind. Also realized it was very late & Miss A. was still waiting for her lunch. Awoke with disagreeable sense this night-mare always leaves. Head-ache too.
### page 40
Indian lecture March 27[superscript underlined]th [entire date underlined]
Just before awakening (with head-noises and in cramped position) dreamt vividly of being with my two brothers somewhere (bright, light place) immediately following my mother's death. A coffin or perhaps treasure-chest (of mother's) was standing near, covered with a bright red cloth embroidered with gold disks* [there is a line drawn from the phrase "gold disks" up to the phrase "Indian lecture" at top of page]. There was some talk about "who would be next" and an atmosphere of peace & contentment, as of pleasant memories of mother (vaguely reminiscent of girlhood scene with brothers & Will H. whistling at piano, of which I had just spoken to my husband).
### page 41
Tell Dr. - not reluctant about talking because afraid of him, - he a very sympathetic listener, - it ought to be a release, an escape, but is just the opposite.
### page 42
Did not quite cover the subject: intellectual interest & vast pity for humanity (Upton Sinclair). [the preceding words constitute the entire contents of this page and are crossed out]
### page 43
January 28 [superscript and underlined]th after Greta & father had been to Dr Neymann with me the night before
________________________________________________________________________________________________
Greta had told Dr. about her affairs and he approved her attitude. He told me he did not want me to make "odious comparisons" between her life and mine. From what he said later (his surprise because I said I believed her life to be unhappier than mine had been) I gathered that he meant by "odious comparisons" jealousy on my part. Since he had never thought of this before and I knew that Greta had (from remarks he made at the time of her affair with Bob) I knew that he had gathered it from Greta. Much upset because of this feeling of contempt (subconscious) on Greta's part; inferiority feeling.---------------------
Jealousy on my part because of Dr.'s approval of Greta's conduct and disapproval of mine. Why
### page 44
did Dr. Neymann say that Greta was not a good influence for me?
### page 45
2nd[underlined] Contin. of Jan. 17 [superscript underlined]th
superior to them, but I never had feeling that I was not[underlined] so. Being in Sana. first gave me that idea. Else why should I be there? Nobody explained. --- Being called abnormal and disagreeable by Dr. has only intensified dissatisfaction & disgust with myself & made me self-conscious. Depressed by father, because he is constant reminder of life wasted on him. Self-reproach for having made him unhappy[last three words crossed out] kept him married to person he did not care for. ---  About doing what is expected of me. Not for lack of self-confidence but because I wanted others to be comfortable X Most important thing in my mental life: this putting myself in another's place.
rather a feeling of strength to be able to do so.[line drawn under "comfortable" to "X" and on to "rather"]
### page 46
Continued January 17.[underlined]
The fact that I subconsc. did not like my mother stirs me only to more self-reproach, as I may inadvertently have hurt her by this mental attitude. (Ask about this "putting myself in another's place") Not as pronounced now as it used to be. What does he mean by "normal"? I have known many people intimately and no 2 exactly alike in emotions, views, re-actions. What is the standard? -- What I gathered from Dr.'s philosophy is this: nothing is very important in life, or worth thinking and feeling about, but just go on living & doing things from day to day, which is just what I have been and am doing. --- He said he put me in Sana. so that I should see nuts and feel
### page 47
January 17. [date underlined]
Felt  badly after visit to Doctor on night before, when he told me that I had never been normal. Chief cause for mental distress: 
self-reproach, because if [crossed out] (although this is no new [underlined] thought for me) I must have unintentionally done much to make those dependent upon me unhappy. Also depression because it is too late to reprieve anything and not worth changing for the few remaining years of my life. Dr. merely corroborated my own opinion of myself (enforced by statement of abnormality) thereby adding to inferiority feeling.
Dr. told me I had never liked my mother. The inference is that my self-reproach at her death was due to this sub-conscious feeling. But self-reproach was merely for not being with her. Also I had it at Franz's death & am always having it in regard to my marriage. (turn back)
### page 48
NOV. 30. HAD ADAPTED MYSELF TO MY HUSBAND IN AGE, CANNOT AFFORD TO BE YOUNG AND I AM WITH MY CHILDREN.
TELL DR. THAT, WHILE I AM NEGATIVELY HAPPY, I AM NOT SATISFIED FUNDAMENTALLY. I AM APPRECIATIVE AND
GRATEFUL FOR WHAT I HAVE; RESTORED HEALTH, COMPARATIVE HEALTH OF MY FAMILY, NO IMMINENT LOSS OF GRIEF,
SHELTER, FOOD, FRIENDS. THIS DOES NOT SATISFY, DOES NOT COMPLETELY ABSORB MY INTEREST; I MUST APPLY
MYSELF TO SOME WORK, TRY TO BE SUCCESSFUL AT IT. ( LIKE DR. N. HIMSELF, WHO WOULD BE HAPPY WITH HIS
PROFESSION). AM NOW DOING THE SORT OF FUTILE THINGS I HAVE BEEN FORCED TO DO ALL MY LIFE AND VAINLY 
STRUGGLED AGAINST. DO NOT LIKE BEING SUBMERGED BY THEM AGAIN. IT IS NOT CONSTRUCTIVE WORK WHICH
ALONE GIVES SATISFACTION. SPEAKING OF "KITCHEN-JOB" WAS ONLY BECAUSE OF IMMEDIATE NECESSITY.
### page 49
DR. N. 
TUESDAY. NOV 25.
PROTEST BECAUSE HE SAID HE WOULD NOT HAVE STOOD FOR MY STAYING AT HOME, ETC. TELL THAT I 
SIMPLY WAS NOT ASKED OUT.
Tell about this Sat. eve. & preliminary conversation (crossed out)
SAY THAT ANAL. IS NOT HELPING FEELINGS. HE COULD NOT SAY ANYTHING. 1/2 AS DISAGREEABLE ABOUT MYSELF
AS I HAVE KNOWN ALL MY LIFE. AND REASONING THAT I MUST NOT TRY TO HAVE ALLES ODER NICHTS ( ALL OR NOTHING)
DOES NOT HELP EMOTIONS. ELSE IT HAD DONE SO LONG AGO, SINCE I HAVE TOLD MYSELF ALL THIS AND ACTED
ACCORDINGLY. BUT HOW DOES IT HELP?  AM ASKING THIS NOT FOR ARGUMENT, BUT BECAUSE I WANT TO BE
HELPED.
WAS PERFECTLY SATISFIED WITH WHAT I HAD FROM MY HUSBAND A FEW MONTHS AGO, NOW AM DISSATISFIED
WILL NOT REGAIN JOYFUL ATTITUDE. WANT TO LEAVE FATHER FOR HIS SAKE. REALLY BELIEVE MYSELF SANE ON
THIS ONE SUBJECT. NOW QUITE MYSELF WITH FATHER. TOO HARD FOR HIM.
LIFE'S ONE REDEMPTION FROM FUTILITY WHEN ALL ELSE IS SAID. (crossed out)
### page 50
ASK DR. N.
WHEN I SAID "THAT LEAVES 2 OPENINGS FOR THE PSYCHO-ANALYST", I EXPRESSED MY THOUGHT BADLY. 
WHAT I MEAN IS THAT THE DR. WOULD CONCLUDE THE DREAM (EMPTY SLEEVE) INDICATED A RESISTANCE
TO PSYCHO-A. UNLESS HE WERE TOLD CERTAIN DEFINITE FACTS OF THE PAST LIFE AND CONSCIOUS
THOUGHT OF PATIENT. IN OTHER WORDS THE DR. CANNOT POSSIBLY DRAW THE MORE IMPORTANT
CONCLUSIONS WITHOUT KNOWING SOME OF THE PATIENT'S CONSCIOUS THOUGHTS AND MEMORIES.
QUESTION: WHY, IF THIS IS SO, DOES DR. SAY ( AS HE DID ) THAT HE IS NOT INTERESTED IN THE
CONSCIOUS THOUGHTS, ACTIONS, AND MEMORIES OF PATIENT BECAUSE THEY ARE NOT HELPFUL?
### page 51
IMMUNITY TO ALL THE ILLS OF EARTH.
A CRYSTAL WALL BETWEEN THE WORLD AND ME.
A HOPE. AN OPALESCE HAZE TO GUARD [____] MY THOUGHTS
### page 52
blank -- back cover