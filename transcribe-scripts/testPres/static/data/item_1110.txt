
## 1110: Edward A. Barnes diary, 1865
### page 1
January, Sunday, 1, 1865.
A beautiful day ... quite a contrast compared with the weather of last New Year.
MONDAY 2
Set aside as a general holiday in this city.
All went to the Washington States Park in the evening except Della & Myself.
### page 2
January, TUESDAY, 3, 1865.
Recieved My "Magazine" containing "The Sailors Path"
WEDNESDAY,4.
Nothing happened worthy of notice
### page 3
January, THURSDAY, 5, 1865.
Weather reminds me of Spring. 
Wrote to Cousin Delia 
Received a letter from Aunt Kitty
FRIDAY, 6
There is nothing today that I can set down.
### page 4
January, SATURDAY, 7, 1865.
Nothing to-day to write down.
SUNDAY, 8.
Ditto
### page 5
January, MONDAY, 9 1865.
A happy day, we recieved our long expected box.
I wrote to Alister Rett
TUESDAY, 10
A cold March day. Expected to recieve the Magazine containing my first story but was disappointed.
### page 6
January, WEDNESDAY, 11, 1865
Rather cold. 
Nothing more to write down.
THURSDAY, 12.
A beautiful day, very much like Spring.
### page 7
January, FRIDAY, 13, 1865.
Nothing in particular to note down.

SATURDAY, 14.
Feel as if I must write down something, but have nothing of interest to write.
### page 8
January, SUNDAY, 15, 1865.
received a call from Mr & Mrs Ward.
Promised to write to thier son John, H, Ward.

MONDAY 16
Cold & stormy towards evening.
Army corrospondence working for Mr Whitaker
### page 9
January, TUESDAY, 17, 1865.
Clear, but very cold. 
My 'Magazine' did not come.
WEDNESDAY, 18.
Still very cold.
Received a letter from Cousin Delia
### page 10
January, THURSDAY, 19, 1865.
The weather more moderate. Nothing further to write down
FRIDAY, 20
A beautiful day, warm & pleasent & much like Spring.
### page 11
January, SATURDAY, 21, 1865.

Another beautiful day

Wrote to Grandfather & John Ward
SUNDAY, 22.
Had a snow storm, which for a time threatened to be heavy for a while.
### page 12
January, MONDAY, 23, 1865.
Very cold & stormy towards night.
Received a good letter from Aunt Kitty.
TUESDAY 24
Very cold, My birthday
Wrote to Cousin Delia
### page 13
January, WEDNESDAY, 25, 1865
Very cold indeed, cold feet & fingers seemed to be all the rage.
But through the kind mercies of God I & those who are dear to me are not suffering for lifes comforts
THURSDAY, 26.
Not much change in the weather,
Seldom saw the windows so thickly coated with frost as they are tonight.
### page 14
January, FRIDAY, 27, 1865
The weather milder
Finished copying my story, the "Brides Peril" after working upon it for several weeks
'
SATURDAY, 28
Much warmer than it was yesterday.
This has a been a week of remarkable severity in weather matters
### page 15
January, SUNDAY, 29, 1865
A beautiful day.
Father & mother went to church in the evening
MONDAY 30
Commenced a letter to Aunt Kitty, but did not finish it.

Hope I shall not be disappointed tomorrow. in seeing my story in the magazine
### page 16
January, TUESDAY 31, 1865
Warm & seems as if we were going to have a thaw.
Father wrote me a letter to send to Mr Dow. 
Finished my letter to Aunt Kitty
February, WEDNESDAY, 1
Received the "Waverly" containing my story the Wayward Con.
& also a letter from Cousin Andrew. Lillie quite unwell
### page 17
February, THURSDAY,2, 1865
Very wet, for it has rained all day.
Wrote to cousin Mariette.
Copied my letter to Mr. Dow. There is nothing more that I think of to note down.
FRIDAY, 3
Wet & rainy.
Wrote to Miss Effie A. Clifton
### page 18
February, SATURDAY, 4, 1865.
Clear & pleasent towards evening.
Did not write a word. Except in my Diary all day
Felt quite unwell with a cold.
SUNDAY, 5.
Another beautiful Sabbath
Father & mother went to Church in the evening.
### page 19
February, MONDAY, 6, 1865.
Done w/ my story but not sense it, Can think of nothing to more to write down.
TUESDAY, 7.
Father mailed my Story 'Brides Peril'
I wrote to cousin Andrew
The above story & poem I sent to the "waverly magazine'
### page 20
February, WEDNESDAY, 8, 1865.
A slight fall of snow last night & very cold.
Can think of nothing more worthy to note except that father sent up 50 weight of buckwheat flour.
THURSDAY, 9.
Father & Mother went to a festival at Union Parke
### page 21
February, FRIDAY 10, 1865
A nice day, but nothing more to note down
SATURDAY 11
Nothing happened today that is worthy of notice.
Tonight the wind is blowing very hard & it snows too.
### page 22
February, SUNDAY 12, 1865
A pleasant day but rather cold.
Father & Mother went to Church
MONDAY 13
Very pleasant
Mother went down to the city shopping.
Wrote to Cousin Emeline.
### page 23
February, TUESDAY, 14, 1865.
Cloudy + looks like a storm
Wrote to Mrs Crane
WEDNESDAY 15
The appearences of a storm I spoke of yesterday proved correct today, for it snowed all day
### page 24
February, THURSDAY, 16, 1865
Wrote to Mrs. Warner.
Received a Valentine.
The weather warm & pleasent
FRIDAY, 17
Received a beautiful letter from Father as a Valentine & which I intend to keep for years to come.
Kit has been gone all day, we know not why.
### page 25
February, SATURDAY, 18, 1865
Can think of nothing that has happened today that is worthy of notice.
SUNDAY 19
A present day.
Father & Mother went to Church in the evening.
Read twenty chapters in the Bible.
### page 26
February, MONDAY 20, 1865
Received the "Marshalltown Ca Times" from Cousin Andy. Had a call from Mrs Hubbard who left us two tracts. Did not write what intended Saturday
TUESDAY, 21.
Commenced copying my story 'Abel Smith a Ward'
Answered fathers letter
Father almost sick with a sore foot.
### page 27
February, WEDNESDAY, 22, 1865.
A beautiful day & one long to be remembered.
Received our second box from Aunt Kitty, which contained many things that we value very highly
Receieved a from Cousin Andrew & Delia
THURSDAY 23
Felt quite unwell all day. With a very hard cold. Answered Cousin Delia's letter 
I received last night
### page 28
February, FRIDAY, 24, 1865.
Sick all day.
Did not feel able to write.
My father got me some medicine in the evening that helped much
SATURDAY 25
A wet, rainy day
Wrote to cousin Andrew & received a letter from Cousin Emeline
### page 29
February, SUNDAY 26, 1865
A cold raw day.
As usual Father & Mother went to Church in the evening.
Lillie gave me a ribbon to put in my Bible for a mark
MONDAY, 27
Stormy towards evening. Wrote on my story & also wrote to Cousin Mary
### page 30
February, TUESDAY, 28, 1865
Nothing happened that is worthy of note.
Except that I wrote on my Story.
March, WEDNESDAY, 1.
Wrote to Cousin Emeline & also Cousin William
### page 31
March, THURSDAY, 2, 1865.
Wrote on my story & wrote to Aunt Kitty also.
Had a can of oysters for supper.
FRIDAY 3
A cold windy day.
Did not write any. Miss Lizzie Flayhaven brought me a book to read.
### page 32
March, SATURDAY, 4, 1865
A beautiful day, but rather cold.
SUNDAY 5
A bautiful Sabbath
Father & mother did not intend Church in the evening as usual.
### page 33
March, MONDAY 6, 1865
Finished reading the Old Testament.
Wrote on my story. Received a letter from John R. Ward
& Cousin Mariette
TUESDAY, 7.
Commenced reading the New Testament.
Wrote to John Ward & Cousin Maryette.
Had a favorable news from Mrs Dow + received a letter from Any, My poem accepted.
### page 34
March, WEDNESDAY, 8, 1865
A bleak stormy day. Wrote on my story.
Answered Amy's letter.
THURSDAY, 9.
A very cold day indeed.
Wrote to Lydia Percival. Father brought home some beautiful cards.
### page 35
March, FRIDAY 10 1865
Milder - but still very cold.
Lillie mailed my letter to cousin Lydia
Received a letter from Mrs Crane.
SATURDAY, 11.
Answered Mrs Crane's letter & received a long beautiful one from Mrs Warner
### page 36
March, SUNDAY 12 1865
Mild & pleasent
Father bought me the "Wide World"
MONDAY 13
A cold wet stormy day & evening
Answered Mrs Warner's letter
### page 37
March, Tuesday, 14, 1865
Very and stormy all day.
Wrote on my story.
Amy returned from Coldwater and brought me some reading.
Wednesday, 15.
Still very wet and raining.
Wrote on my story.
### page 38
March, Thursday, 16, 1865.
Wet and stormy in the morning but pleasent toward evening.
Wrote on my story.
Friday, 17.
A windy March day.
Father left G.S. Whitaker.
### page 39
March, Saturday, 18, 1865.
Very much like Spring. Mild and pleasent.
Received a letter from Cousin Emeline.
Sunday, 19.
Father and Mother went to Church in the Evening.
### page 40
March, Monday, 20, 1865.
Warm and pleasent.
Father at home nearly all day.
Had a thunder shower in the Evening.
Tuesday, 21.
Wet and rainy.
Wrote on my store and answered Cousin Emeline's letter.
Mr. Waverly did not come.
### page 41
March, Wednesday, 22, 1865.
A beautiful day but rather windy.
Received a letter from Cousin Andrew which contained Cousin Lucy's picture.
Magazine did not come.
Thursday, 23.
Pleasent but cold and windy.
Did did see my poem in print as I expected but hope I shall next week.
### page 42
March, Friday, 24, 1865.
Still pleasent but cold.
Father brought me a beautiful "Soldier's Hymns Book" from the Young Men Christians Association Rooms.
Saturday, 25.
Pleasent but cold.
Father commenced working for Mr. Stanley. He brought me five little books from the Young Men's Christian Association Rooms.
### page 43
March, Sunday, 26, 1865.
A pleasent day.
Father and Mother did not attend Church by took a walk in the evening.
Monday, 27.
Received a letter from Cousin Mary and wrote on my story.
### page 44
March, Tuesday, 28, 1865.
Warm and pleasent.
Answered Cousin Mary's letter.
Wednesday, 29.
Pleasent in the morning but raining in the afternoon.
Wrote to Cousin Andrew and recieved a letter form Cousin Delia.
### page 45
March, Thursday, 30, 1865.
Cold and windy.
Felt bad and discouraged all day.
Friday, 31.
A beautiful day.
Went out and took the air.
Wrote to Aunt Kitty.
### page 46
April, Saturday, 1, 1865.
A charming day.
Felt sad and discouraged.
Sunday, 2.
A lovely Sabbath.
### page 47
April, Monday, 3, 1865.
Received a letter from Aunt Kitty and wrote to Grandfather.
A great time down in the city celebrating the "The Fall of Richmond."
Tuesday, 4.
Warm and pleasent.
### page 48
April, Wednesday, 5, 1865.
Pleasent but windy.
Thursday, 6.
Cold and windy.
### page 49
April, Friday, 7, 1865.
Still very cold and windy.
Received a letter from Cousin Lydia.
Saturday, 8.
A beautiful day but somewhat cold.
Had a sore knee.
### page 50
April, Sunday, 9, 1865.
Almost sick all day. Father went up to Camp Douglass and found that Amy had enlisted and gone to Springfield.
Monday, 10.
Lee surrendered to General Grant.
Knee better, wrote to Cousin Delia.
Father made a bonfire in the evening.
### page 51
April, Tuesday, 11, 1865.
Rainy towards evening.
Answered Cousin Lydia's letter.
Wednesday, 12.
Pleasent.
Miss Lizzie brought me down a book to read.
### page 52
April, Thursday, 13, 1865.
Still pleasent.
Friday, 14.
A beautiful day.
Went out and set on the stoop.
Mother bought me the "Wide World."
### page 53
April, Saturday, 15, 1865.
A pleasent day.
The Death of Abraham Lincoln.
Sunday, 16.
Pleasent but cold.
Father and Mother went down to the city.
### page 54
April, Monday, 17, 1865.
Pleasent but awful windy.
Tuesday, 18.
Pleasent.
Went out and sat on the stoop.
### page 55
Wednesday, April 19, 1865.
Rainy in the forenoon.
Pleasent towards night.
Thursday, 20.
Still rainy all day.
### page 56
Friday, April 21, 1865.
rained nearly all day. & cold & windy.
I wrote two pieces of poetry
Saturday, 22.
Cold & stormy.
Sent a peice of poetry entitled "Spring" to the "Wide World."
### page 57
April, Sunday, 23, 1865.
Pleasent but rather cold.
Monday, 24. 
Still Pleasent,
### page 58
April, Tuesday, 25, 1865. 
A high wind
recieved a letter from Cousin Emiline
Wednesday, 26. 
Warm & unclear
### page 59
April, THURSDAY, 27,1865.
rained nearly all day.

     Booth the Murderer killed

FRIDAY, 28.
Still damp & rainy
### page 60
April, SATURDAY, 29, 1865.
Pleasent but cool.
Mother cut my hair. 
Copied a piece of poetry. 
Sunday 30
A wet rainy Sabbath.
### page 61
May, MONDAY, 1, 1865
Pleasant but cool 
President Lincoln's remains arrived in this city. 
Sent a piece of poetry 
"Beginning to bloom."
"Waverly Magazine
Tuesday, 2. 
Pleasent but rather cold. 
Father took me unclear a good long ride.
### page 62
May, WEDNESDAY, 3, 1865.

Pleasent but rather cold unclear evening
THURSDAY, 4.
A dark day. received a letter from Cousin Lydia
### page 63
May, FRIDAY, 5, 1865. 
rather cool. Moved from Indiana. to Hubbard Street,
Saturday, 6. 
Rainy in the morning but pleasent in the afternoon.
### page 64
May, Sunday 7, 1865
A beautiful Sabbath.
Monday, 8.
Pleasent [sic] but rather cool.
### page 65
May, TUESDAY, 9, 1865.
Still pleasent sic
Wednesday, 10.
Pleasent [sic] but cloudy & rather cold. Father brought home a lot of unclear sugar & some soap.
### page 66
May, THURSDAY, 11, 1865.
A charming day. Wrote to Cousin Lydia. 
Friday, 12.
Very pleasent but cloudy. Towards eve = windy & had a little rain.
### page 67
May, SATURDAY 13, 1865.
Pleasent but cool. A rainy night. Finished reading the Bible. 
SUNDAY, 14. 
A happy day for me. Father brought me unclear "Wide World" containing my poem? "Spring." Commenced the Bible again.
### page 68
May, MONDAY 15, 1865. 
Very Warm, send another piece of poetry entitled "The Soldier's Mother" unclear "Wide World."
TUESDAY 16. 
Very warm indeed. Father gave me a nice lead? pencil.
### page 69
May, WEDNESDAY, 17, 1865. 
Still very warm. Had a delightful shower in the afternoon. Wrote to Cousin Emelin? Father brought home a large cake of unclear sugar. 
THURSDAY, 18.
Rather rainy. Commenced a short story entitled "The Stranger? at the Grave."
### page 70
MAY, FRIDAY 19, 1865.
Pleasent in the morning but damp and foggy at night. Finished the story I began yesterday. 
Saturday, 20.
Very warm. Much interested in reading John Godfry's For? Unclear.
### page 71
May, SUNDAY, 21, 1865.
Warm but a good wind. Had company to dinner. 
MONDAY, 22. 
Still warm but pleasent. Wrote some & read John Godfrey.
### page 72
May, TUESDAY, 23, 1865.
Wrote some & finished reading John Godfry. A great book that. 
Wednesday, 24. 
Pleasent but cool towards night. Mother went down town but crossed out did not do her errants.
### page 73
May, THURSDAY, 25, 1865.
Still pleasent but rather cool. Wrote Miss Maud Sterling. 
Friday, 26. 
Mother took a walk in the afternoon & brought me a book from Mrs Hoonkr's?.
### page 74
May, Saturday, 27, 1865.
A pleasent day much interested in the "The Green Mountain Boys"
Sunday, 28. 
Warm & pleasent. Father & Mother was in church & heard Doc Harris preach
### page 75
May, MONDAY, 29, 1865.
A charming day. Mother brought me a magazine. Had strawberries for supper.
Tuesday, 30.
Mother went down to the city & bought me my "magazine" containing my piece "Beginning to Bloom"
### page 76
May, Wednesday, 31, 1865.
Very warm. Finsihed copying my story. Had strawberries for supper. 
June, Thursday, 1. 
Another warm day. National Fast.
### page 77
June, Friday, 2, 1865. 
Still very warm. 
Saturday, 3. 
No change in the weather. Wrote a piece of poetry.
### page 78
June, Sunday, 4, 1865.
A warm Sabbath. Expected company but she did not come. 
Monday, 5. 
Hot & dry. Had company for supper.
### page 79
June, Tuesday, 6, 1865. 
Still very warm. Father & mother went to the Fair in the evening. 
Wednesday, 7. 
Sent two pieces of poetry "Welcome Home" & Midnight Thoughts" to the Waverly? Father brought home a $500 bill of confederate money.
### page 80
June, Thursday, 8, 1865.
Sick all day. Mother brought me some medicine that helped me. 
Friday 9. 
Rained nearly all day. The first we we have had for almost two weeks.
### page 81
June, Saturday, 10, 1865. 
A beautiful day but rather cool after the rain. Had strawberries for supper but not could not eat any. 
Sunday, 11.
Father brought home the "Wide World" which contained my "Soldier's Mother." He did not know it until I showed it to him.
### page 82
June, Monday, 12, 1865.
Sent a story entitled "The Stranger at the Grave" to the Wide World. Father & mother & the children went to the Fair. 
Tuesday, 13.
Wet & rainy. Disappointed about going to the Fair. Had a new pencil. Received a letter from Cousin Delia.
### page 83
June, Wednesdsay 14, 1865.
Wet & showery. Wrote & copied a piece of poetry. Recieve two letters one from Cousin Emeline & one from Cousin Lydia. 
Thursday, 15.
Still showery. Father took me to the Fair. Brought home my story.
### page 84
June, Friday, 16, 1865.
Very warm.
Saturday 17.
Very warm indeed?. Had a delightful shower.
### page 85
June, Sunday, 18, 1865.
Had a heavy shower. Received a letter from Cousin Maryettte? Had strawberries for supper
Monday, 19.
A charming day. Another shower. Commenced a story entitled "Out on the Lake"
### page 86
June, Tuesday, 20, 1865.
Had a shoewr in the morning. Wrote? on my story.
Wednesday, 21.
Clear & pleasent. Wrote on my story.
### page 87
June, Thursday, 22, 1865.
Wrote on my story. Had strawberries for supper.
Friday, 23. 
A warm day. Had a fress? of a Lincoln unclear.
### page 88
June, Saturday 24, 1865.
Warm & dry. Finished my story. Had a heavy storm toward night. 
Sunday, 25.
Wet & rainy all day. Della ran a sick spell.
### page 89
June, Monday, 26, 1865.
Still clowdy. Wrote to Cousin Margatte?. Father brought home a "Harper's Magazine."Lillie got me a book.
Tuesday, 27. 
Pleasent but cloudy. Wrote to Cousin Lydia. Received the magazine containing my "Welcome Home."
### page 90
June, Wednesday, 28, 1865.
Had a heavy shower in the morning. Wrote to Cousin Delia. Received a letter from Cousin Andrew. 
Thursday, 29.
Pleasent but a high wind. Wrote & copied a peice of poetry for Cousin Emeline. Received a letter from Cousin Mary.
### page 91
June, Friday, 30, 1865. 
Wrote to Cousin Emeline & sent her a piece of poetry unclear. Had a splendid shower. Had a cherry pie for supper. 
July, Saturday, 1. 
Sent a story "The Bride's Peril," a poem "The Young Orphan" & a letter to the "Waverley" for the second time?. Father brought home some soaps?.
### page 92
July, Sunday, 2, 1865. 
A charming Sabbath. Nothing occurred that is worthy of note.
Monday, 3. 
Warm & cloudy. Mrs Bonham sent me a book to read.
### page 93
July, Tuesday, 4, 1865. 
Did not go down to the city as I expected. Received my magazine containing "Midnight Thoughts" and the "Wide World" containing "The Starnger at the Grave". Received a letter from Mr. Barry. 
Wednesday, 5. 
Warm & sultry. Father brought me the "Spirit of the West?. Had company in the evening.
### page 94
July, Thursday, 6, 1865.
Very warm & dry. Copied a piece of poetry. Received a letter from Cousin Mary Goodrich. 
Friday, 7. 
Wrote & copied a piece of poetry. Much interested in reading "Dred." Rainy at night.
### page 95
July, Saturday, 8, 1865.
Wrote to Cousin Mary Goodrich & also to Mr Berry. 
Sunday, 9. 
A wet rainy Sabbath. Father had a big offer but did not accept it.
### page 96
July, Monday, 10, 1865.
Rainy. Sent "The Mother Welcome" to the "Spirit of the West" & "the Bride's Adieu" to the "Wide World." Commenced copying "Out on the Lake."
Tuesday, 11,
A beautiful day. Wrote on my story.
### page 97
July, Wednesday, 12, 1865. 
Pleasent but cool towards night. Wrote on my story. 
Thursday, 13.
Pleasent but cool. Wrote on my story.
### page 98
July, Friday, 14, 1865. 
Pleasent but clowdy and rainy towards night. Wrote on my story. 
Saturday, 15.
A wet rainy day. Wrote on my story. Received a letter from Cousin Mariette.
### page 99
July, Sunday, 16, 1865. 
A charming Sabbath. Father brought me "Harper's Weekly." Miss Lizzie brought me a book. 
Monday, 17. 
Very pleasent. Wrote on my story but is tired to? copy part of it . Much interested in "The Unclear."
### page 100
July, Tuesday, 18, 1865.
Cool & pleasent. Wrote to Cousin Andrew. Had some splendid blackberries for supper. 
Wednesday, 19. 
Rainy in the morning. Wrote to Cousin Mary.
### page 101
July, Thursday, 20, 1865.
Pleasent but cloudy. Wrote on my story. 
Friday, 21. 
Wet & rainy. Had a peach pie? for dinner.
### page 102
July, Saturday, 22, 1865. 
Clowdy. Felt sick all day. Father brought home some peaches & got me some medicine. Lillie got me a nice box?
Sunday, 23. 
Pleasent in the morning but cloudy & rainy at night. Felt better. Father brought me "Wide World".
### page 103
July, Monday, 24, 1865.
Still wet & rainy. Sent "The Mother's Welcome" to the "Wide world" & wrote to Cousin Mariette. 
Tuesday, 26. 
Pleasent but a high wind & a little rain. Wrote on my story. Received the "Wide World" containing "The Bride's Adieu".
### page 104
July, Wednesday, 26, 1865.
Rain, rain, nothing but rain. Wrote on my story. Had a cherry pie? & clams? for supper. 
Thursday, 27. 
Pleasent but cloudy. Commenced a short story "Coming Home" but think I shall alter the title. Mother got me a Dime Novel & father a book.
### page 105
July, Friday, 28, 1865. 
Pleasent but cloudy & a little rain. Wrote on my story. Father brought home a basket of peaches. 
Saturday, 29.
Clear & a splendid air? Finished my story. Received a letter from Cousin Emeline & also a paper from her with my poem "Drifters"
### page 106
July, Sunday, 30, 1865.
A beautiful Sabbath. Had company to dinner. Mrs Ward & another lady called. Della sick with the mumps. 
Monday, 31. 
Warm & pleasent. Commenced copying "Coming Home" Had the first number of the new Corning paper?
### page 107
August, Tuesday, 1, 1865. 
Warm & clear. Wrote on my story. Received a letter from cousin Lydia & one from Delia. She also unclear in a paper? containing my "Welcome Home"
Wednesday, 2. 
Very warm. Wrote on my story. Father brought home a box of peaches.
### page 108
August, Thursday, 3, 1865.
Sill warm & sultry.
Wrote on my story.
Cool & Cloudy, low unknown night.
Father went unknown look at a house.
Friday, 4.
rainy in the morning
wrote on my story,
Heard from Mr Dow.
Sent me four Dollars.
### page 109
August, Saturday, 5, 1865.
A rainy day.
Did not write much.
Sunday, 6.
Cloudy but pleasant.
Father brought me a novelette.
Had a water-melee.
### page 110
August, Monday, 7, 1865.
Warm & pleasant.
Did not write much.
An awful noise down stairs.
Tuesday, 8.
Warm & sultry.
received the magazine containing "The Bride's Peril" & the "Wide World" continaining "The Mather's Welcome".
Wrote on my story.
Lilli gave me a little book & Della also.
### page 111
August, Wednesday, 9, 1865.
A dark dreary day 
An awful storm. 
Wrote on my story.
Thursday, 10.
A beautiful day.
Wrote on my story. 
Father brought home 15 bls? of maple sugar
### page 112
August, Friday, 11, 1865. 
Cloudy yet pleasant.
Wrote on my story.
Had a watermellon
Saturday, 12.
Clear & warm
Did not write any
Father brought home a basket of peaches
### page 113
August, Sunday, 13, 1865.
A unknown Sabbath
Had another watermellon
Saw Nellie Clark
Monday, 14.
Clear & warm.
Wrote on my story
Received the letter I wrote to unknown
### page 114
August, Tuesday, 15, 1865.
Pleasant but cloudy towards night.
Wrote on my story.
Father came home to dinner.
Wednesday, 16.
Cloudy in the forenoon, but came? off clear & pleasant
Wrote on my story. 
Nellie & Carrie Clark visited us.
### page 115
August, Thursday, 17, 1865.
Clear, warm & pleasant.
Wrote on my story. 
Father at home in the afternoon.
Friday, 18.
Clear & Sultry.
Did not finish my story as I expect. 
Lillie went down to the? Clark's in the afternoon.
### page 116
August, Saturday, 19, 1865.
Pleasant in the forenoon.
Had a heavy hailstorm.
Finished my story. unknown altered it title.
Sunday, 20.
Pleasant but rainy toward night.
Had a call from Miss Ward & unknown.
### page 117
August, Monday, 21, 1865.
Cool & cloudy.
Corrected my story.
Wrote a peice of poetry
recieved a letter from Cousin Mariette.
Tuesday, 22.
Pleasant but cool & windy
Copied my poetry
### page 118
August, Wednesday, 23, 1865.
Pleasant & warm.
Father at home with a lame back
He Brought a book from a unknown soldier.
Thursday, 24.
Dry and warm.
Did not write any
### page 119
August, Friday, 25, 1865.
Still dry & warm
Wrote to Cousin Emmeline.
Saturday, 26.
Very warm,
wrote & copied a peice of poetry.
Mrs. Smith sent me a book
### page 120
August, Sunday, 27, 1865. 
Pleasant but cloudy at night.
John brought me some books.
Monday, 28.
Had a thunderstorm
sent two peices to the "Wide World" - "unknown Bride" & "Tread? Softly"
Sent a peice of poetry - "Perfect Day!" to the "Waverly".
### page 121
August, Tuesday, 29, 1865.
A very warm day.
Much interested in reading "Silver Star."
Wednesday, 30.
Still very warm
Wrote to Cousin Dilia?
### page 122
August, Thursday, 31, 1865.
Cloudy & Sultry
Sprinkled a little
Wrote to Cousin Lydia
September, Friday, 1.
Clear & sultry.
Went to a Irish dance.
### page 123
September, Saturday, 2, 1865.
Cloudy in the morning, but came off warm & sultry.
Mother almost sick with a cold.
Sunday, 3. 
No changes in the weather.
Miss Lizzie brought me a novelette
### page 124
September, Monday, 4, 1865.
Excessively warm.
Cloudy & sprinkled a little.
Had an introduction to a captain on the unknown/brigade.
Tuesday, 5.
Cloudy & rainy.
Father brought home some honey.
### page 125
September, Wednesday, 6, 1865.
Warm & Cloudy.
Mother went down to the city.
Wrote to Cousin Mariette
Thursday, 7.
A rainy day
Had a nap.
### page 126
September, FRIDAY, 8, 1865
A pleasent day. Had quite a fire in our niegborhood Father brought home som grapes.
SATURDAY, 9.
Pleasent but very sultry. Did nothing having nothing to do
### page 127
September, SUNDAY, 10, 1865.
Pleasent in the morning. riny? towards night
MONDAY, 11.
Clear & pleasent. Father brought home two mushmellows?
### page 128
September, TUESDAY, 12, 1865.
A showery day. Mother & the children went out.
WEDNESDAY, 13.
Pleasent in the forenoon, but rainy at night.
### page 129
September, THURSDAY, 14 1865.
Cloudy & very sultry. Father home early.
FRIDAY, 15.
Damp & mist but pleasent towards night Mother sick & father at home nearly all day.
### page 130
September, SATURDAY, 16, 1865.
No change in the weather.
SUNDAY, 17.
rained nearly all day. A dreary Sabbath.
### page 131
September, MONDAY, 18, 1865.
Pleasent but rather cool  
News from home 
TUESDAY, 19. 
A beautiful day. 
received the magazines containing “Perfect Day” 
Father brought me some writing paper
### page 132
September, Wednesday, 20, 1865
Clear and pleasant
Father brought home an egg plant.
Thursday, 21
rather warm.
wrote and copied a piece of poetry.
### page 133
September, Friday, 22, 1865.
cloudy and pleasant
wrote a piece of poetry.
Saturday, 23
very warm.
copied my poetry.
### page 134
September, Sunday, 24, 1865
Rainy in the forenoon, but pleasant towards night.
An awful storm during the night.
Monday, 25
clear and pleasant
Sent "Waiting" in the "Wide World".  "The Maiden's song" to the magazine
### page 135
September, Tuesday, 26, 1865
Pleasant but cloudy towards night.
Wednesday, 27
pleasant but cloudy 
felt discouraged
### page 136
September, Thursday 28, 1865
Still pleasant but cloudy
Father brought me a $20 bill confederate money
Wished it was good
Friday, 29
Cloudy and cool.
A beautiful sunset
### page 137
September, Saturday, 30, 1865
A charming day
Mother cut my hair

October, Sunday, 1
A beautiful sabbath
read the Book of Esther
Exchanged knives with Father
### page 138
October, Monday, 2, 1865
Read some and thought much
Tuesday, 3
Still clear and pleasant
Mother went down to the city unclear
### page 139
October, Wednesday, 4, 1865
Cool and pleasant
Saw the advertisement of a new literary paper to be published here.
A mistake. I saw in the 3rd.
Thursday, 5
Charming weather 
Wrote and copied a piece of poetry
### page 140
October, Friday, 6, 1865
Cloudy but pleasant
wrote a fall piece? 
Saturday, 7
Cloudy but pleasant
Mother made pumpkin pies.
### page 141
October, Sunday 8, 1865
A lovely sabbath.
Father brought home some grapes
Monday, 9
No change in the weather
### page 142
October, Tuesday, 10, 1865
Still clear and pleasant
Copied my fall piece
Had some grapes given me
Wednesday, 11
Still pleasant
Received the "Wide World" containing my poetry "waiting"
My story to appear next week
### page 143
October, Thursday, 12, 1865
Cool and cloudy
Read some and thought much
Friday, 13
Still cool and cloudy
Wrote and copied a piece of poetry
### page 144
October, Saturday, 14, 1865
Pleasant but cool
re-copied my poetry
received a letter from Cousin Sam?
Sunday, 15
Cold and windy
Father brought home an accordion and the messenger
### page 145
October, Monday 16, 1865
Pleasant
Sent "A little while" to the magazine and "Life" to the Wide World
Tuesday, 17
A windy day, and rainy towards night
received the "wide world", contains Annie's Pride
### page 146
October, Wednesday, 18, 1865
Warmer but not very pleasant
Nothing more to write down
Thursday, 19
Pleasant
Saw the eclipse
Copied a piece of poetry
Had oysters for supper
### page 147
October, Friday, 20, 1865
Cool and pleasant
Mother went down town on business for me
Mr Worden sent me a paper
Saturday, 21
Pleasant
Had quinces? guineas? for supper
### page 148
October, Sunday, 22, 1865
A beautiful sabbath
Monday, 23
Rainy but pleases towards night
Sent "The Falling Leaves" to the Wide World with a letter
### page 149
Dark & rainy
Received my magazine containing "the maiden's song."
Pleasant
Did not feel well.
### page 150
A rainy day.
Copied my poetry.
Drear & rainy.
### page 151
The Storm cleared off.
Wrote to Cousin Em.
A beautiful Sabbath.
Mother made me a book mark of leaves.
### page 152
October, Monday 30, 1865
Cloudy
Sent "Passing Away" to the Messenger
Tuesday, 31
Clouds
Heard of the death of grandfather Barnes
### page 153
November, Wednesday 1, 1865
Rainy and dark
Fixed my writing desk
Thursday, 2
Cloudy
### page 154
November, Friday, 3, 1865
A beautiful day
Had three ducks and a pie from the Fremont House for supper
Saturday, 4
Cool and stormy
### page 155
November, Sunday, 5, 1865
Pleasant
Had company for dinner
Monday, 6
Pleasant 
Had company for dinner
### page 156
November, Tuesday, 7, 1865
A lovely day
Election but did not vote
Wednesday, 8
Mild and pleasant
### page 157
November, Thursday, 9, 1865
Still mild and pleasant
Father brought home some grapes and candy
Friday, 10
Still pleasant
Father brought home the Pillsfield? Sun
### page 158
November, Saturday, 11, 1865 
Still pleasant
Mrs Ward lent me two books
Sunday, 12
A beautiful sabbath
Had a call
### page 159
November Monday, 13, 1865
pleasant
Wrote a piece of poetry
Had company
Father brought home a box of sweet potatoes
Tuesday, 14
Pleasant but cloudy
Sent "The Dying Mother? to the Messenger
received the magazine containing "A Little While"
### page 160
November, Wednesday, 15, 1865
Warm and sultry
Read some and thought until my head ached
Thursday, 16
Pleasant but cloudy
wrote and copied a piece of poetry
### page 161
November, Friday, 17, 1865
Pleasant
received a letter from cousin Lidy stated the death of Aunt Mary
Received the unclear Eagle containing my unclear
Saturday, 18
Pleasant
Wrote a piece of poetry
### page 162
November, Sunday, 19, 1865 
Pleasant
Had company 
Received the messenger containing "The Dying Mother"
Monday, 20
Clear and pleasant
Read the "Songs of Solomon"
Wrote on my long neglected story
### page 163
November, Tuesday, 21, 1865
Pleasant
Wrote on my story.
Father brought home some apples and unclear
Had a song book given me
Wednesday, 22
Pleasant
Mother quite sick finished my story
### page 164
November, Thursday, 23, 1865
Cloudy
Father brought home some apples, cranberries and a card table
He gave me a new knife
Friday, 24
A charming day
received the "Wide World" contains "Life" that I thought was lost
### page 165
November, Saturday, 25, 1865
Pleasant
Partially copied a piece of poetry
Sunday, 26
A beautiful sabbath
Father brought home some good things
### page 166
November, Monday, 27, 1865 
Pleasant
Sent "Out on the Lake" at the Messenger
Tuesday, 28
A stormy day
Snow and rain
Much interested in reading Jane Eyre
### page 167
November, Wednesday, 29, 1865
Cloudy.
Had a call 
Began a new story "The Sailor's unclear"
Thursday, 30
Cloudy
Wrote on my story
Della Sick?
### page 168
December, Friday, 1, 1865
Cloudy
A snowstorm at night
### page 169
December, SUNDAY, 3, 1865.
A gloomy Sabbath
rain & fog.
MONDAY, 4.
Damp & cloudy
Wrote? on my story.
Had company.
### page 170
December, TUESDAY, 5, 1865.
A charming day. 
Had company.
Wrote just two words
WEDNESDAY, 6.
Pleasent
Mother & Aunty went down town.
### page 171
December, THURSDAY, 7, 1865.
A charming day
Thanksgiving
FRIDAY, 8.
Very pleasent
Did not feel well
### page 172
December, SATURDAY, 9, 1865.
Stormy.
Wrote & coppied a peice of poetry.
Recieved a letter from Brigham & unclear
SUNDAY, 10.
A beautiful Sabbath
### page 173
December, MONDAY, 11, 1865.
Very mild for the seasion.
Wrote on my story.
TUESDAY, 12.
A cold bleak day.
Wrote on my story
### page 174
December, WEDNESDAY, 13, 1865.
Very cold & bleak
Father at home with a sore foot.
THURSDAY, 14.
Still extremly cold.
### page 175
December, FRIDAY, 15, 1865.
Milder to-day.
Had an attack of fever & chills
Corbills & Fleming hung.
SATURDAY, 16.
Warmer.
Had a unclear sent me.
### page 176
December, SUNDAY, 17, 1865.
Clear & pleasent.
Miss Lizzie brought me a book.
MONDAY, 18.
Had a snow storm.
Wrote on my story
### page 177
December, TUESDAY, 19, 1865
Pleasent but cool.
Recived the "Wide World" with my peice "Gone."
WEDNESDAY, 20.
A heavy snow storm. 
Wrote some
### page 178
[December, THURSDAY, 21, 1865.]
Clear & cold,
Wrote on my story.
[FRIDAY]
Clear &cold,
Wrote some
Finished
Shoulder Straps
### page 179
[December, SATURDAY, 23, 1865]
Milder
Wrote Some
[SUNDAY, 24.]
A.Cleasen & Sabbath
Mother gave me a “Novelette.” for a
Christmas present
### page 180
[December, MONDAY,25, 1865.]
Mild & pleasant
Mother Made
some candy.
[TUESDAY, 26.]
Pleasant & Mild
Wrote some
### page 181
[December., WEDNESDAY, 27, 1865.]
A charming day
Wrote some
[Thursday, 28.]
Pleasant but clowdy.
Wrote some
(Mother & the children
went away.
### page 182
[December, Friday, 29, 1865.]
Pleasent
Wrote some
Father browght to me
Some writing paper
[Saturday, 30.]
Pleasent
Copied a piece
of poetry.
Mother browght me a
beautiful picture for a
New Year’s 
Present.
### page 183
December, SUNDAY, 31, 1865.
A beautiful morning?
Pleasent all day, & the unclear line mild & pleasent.
It has been a year of union triumphs & it has brought peace to our Country.
Farewell to the blighted? hopes, the many cares, & the despondent days of 1865!
### page 184
10 [MEMORANDA]
Nov. 8th, 1864,
Commenced the
Bible the second time,
Abraham Lincoln 
re-elected to the
Presidential
Chair
Dec 28th. 1864,
Amos arrived
here. 
Lillie’s birthday
also,
10 years old,
Della had a new
pair of Shoes.
### page 185
[MEMORANDA.]
Dec 5th 1854
had a Barrel
of flour.
Oct 11th 1866
Mother papered my writing desk.
### page 186
[MEMORANDA.]
upside down 15
Jan 13th Cousin Amy
visited the Washington
Skating 
Park with Miss
Sarah Wells. Wanted
to go with them
but could not.
Imagined all
the Evening what
a good time
they were having.

& finally went

to be a Scopins?
[?] at my time 
for such things
is yet to come,
### page 187
41
Feb 7th Sent a story entitled the 'Bride's Bride?" 20 pages in length with a poem the "unclear Orphan?" with a letter to Mr Dow the whole costing 45 cents. No of weeks the above had been gone 12345678
July 1st/67 Sent with a letter a draft for $10.00 to the Wide World.
### page 188
Margin notes: 5 "Memoranda" 51
1868
Songs written for Root Cady and prices received for the same:
The Captain's Bride $1.00
'Tis Sweet 1.00
Give Grant the Votes 2.00
We shall all go home 1.00
The beauty of the seasons 1.00
The waiting Wife 2.00
Nellie - 1.00
The shoes that Nellie wore 2.00
Once more at Home 2.00
The Lord we love 1.00
There is one hear 1.00
The absent one 1.00
18,72
### page 189
71
1872 Hymn Written for Prof. PP Bliss
"Like" $3.00 the pleading unclear 3,00 Unclear pretty pretty can br
### page 190
91 MEMORANDA. Feb 22th 1865. Received a box from home containing mother’s sofa pillows & [ottimonds]. My protrate a beautiful Testiment as a gift from Aunt Kitty & which was Aunt Mary’s & also a portion of a scrapbook that my Mother made for me in the days of my child-hood. all of which I value more highly than words can express & it is a day that will live in my an rememberance. for many years to come.
### page 191
unclear Letters recieved (followed by 122 strokes) Letter written (followed by 63 strokes) 9197711200905671087120205105714200201020610067191017205106106168203000100106178872030010180110889 pretty 2 29 2144
### page 192
[December, BILLS PAYABLE.]
[Date. Name. Amount.]
Miss Barnes
M.P.
M.B.
K.L
J.B.
J.J.
H.R.
D.J.
W.B.
S.W. [RECEIVABLE.]
A.B.
M.H.
M.G.
### page 193
[MEMORANDA]
[Date. Dolls. Cts.]
M.P.
M.B
R.L
W.L.
J.B.
J.J.
H.B.
D.J.
W.B.
J.W.
A.B.
M.A.
M.G
### page 194
Miss Barnes Teacher Annie Bartlett Georgie Strong Martha Phelam Mable Blake Dora Smith Helen Robison, Kitty Lawrence Maryann Gaffrey Willie Bartlett, " Lawrence Georgie Winter Speckie Barnes Mary Arnold
### page 195
[MEMORANDA.]
[Date. Dolls. Cts.]
Attendance. 
P.P.P.P.P.
P.P.P.P.P.
P.P.P.P.P.
P.P.P.P.P.
P.P.P.P.P.
P.P.P.P.P.
P.P.P.P.H.
P.P.P.P.P.
P.P.P.P.H.
P.P.P.P.P.
P.P.P.P.H.
P.unclear.P.P.P.
P.P.
### page 196
[MEMORANDA.]
[Date. Dolls. Cts.]
unclear
M.P. 100.
M.B. 100
K.L. 100.
W.L. 100.
S.B. 100.
unclear. 100.
H.R. 100.
D.J. 90.
W.B. 88.
S.W. 100.
H.B. 100.
M.H. 100.
M.G. 100.
### page 197
[MEMORANDA] 
Reading.
.
### page 198
Geography. M.P. 100. M.B. 100. K.L. 100. M.L 75. G. B. 90. G.T. MR D.S. W.B. G.W. A.B. M.A. M.G.
### page 199
BLANK
### page 200
41446955 Dec 24th 1864 No. 26. id World 41446455 No 805 May 28 7 I am lame and cannot talk unclear Eddie  little while Eddie N.B.M. from my little
### page 201
I wish to stop
at Centre Ave