
export const prerender = true;
// export const trailingSlash = 'always';

const colors = {
	sapphire: '#4051a3',
	parkGreen: '#20543e',
	midnight: '#111a2a',
	mulberry: '#ad285a',
	tile: '#925840',
	flame: '#f16151',
	lake: '#7ec4e2',
	granite: '#eaebe7',
	lavender: '#cdb8d1',
	sand: '#eadcc9'
};
const combinations = [
	// [colors.sapphire, colors.lavender],
	// [colors.sapphire, colors.granite],
	// [colors.sapphire, colors.sand],
	// [colors.sapphire, colors.lake],
	// [colors.parkGreen, colors.granite],
	// [colors.parkGreen, colors.sand],
	// [colors.parkGreen, colors.lake],
	// [colors.midnight, colors.lavender],
	[colors.lavendar, colors.granite],
	// [colors.midnight, colors.sand],
	// [colors.midnight, colors.lake],
	// [colors.mulberry, colors.lavender],
	// [colors.mulberry, colors.granite],
	// [colors.mulberry, colors.sand],
	// [colors.mulberry, colors.lake],
	// [colors.tile, colors.granite],
	// [colors.tile, colors.sand],
	// [colors.tile, colors.lake]
];
export function load() {
	const index = Math.floor(Math.random() * combinations.length);
	// console.log("Math.floor(Math.random() * combinations.length)",index)
	// let flipper = !!Math.round(Math.random()) ? [0, 1] : [1, 0];
	const flipper = [0, 1]
	return { randocolors: [combinations[index][flipper[0]], combinations[index][flipper[1]]] }
}
	// export const randocolors = colorfkr();